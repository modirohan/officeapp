<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'web'], function () {
    Route::get('/login', ['as' => 'login', 'uses' => 'HomeController@login']);
    Route::get('/', ['as' => 'home', 'uses' => 'HomeController@login']);
    Route::get('test', ['as' => 'test', 'uses' => 'HomeController@test']);
    Route::post('/authenticate', ['as' => 'user.authenticate', 'uses' => 'HomeController@authenticate']);

    //USER FORGOT PASSWORD
    Route::get('forgot-password', ['as' => 'login.forgot-password', 'uses' => 'ForgotPasswordController@index']);
    Route::post('forgot-password', ['as' => 'forgot-password.sendResetLink', 'uses' => 'ForgotPasswordController@sendResetLink']);
    Route::get('reset-password/{token}', ['as' => 'resetPasswordLink', 'uses' => 'ResetPasswordController@resetPasswordLink']);
    Route::post('reset-user-password', ['as' => 'reset-user-password', 'uses' => 'ResetPasswordController@resetPassword']);

    Route::get('/hrm/profile', 'UsersController@index')->name('user.profile');
    Route::get('/hrm/edit-profile', 'UsersController@edit')->name('user.edit');
    Route::POST('/hrm/profile', 'UsersController@update')->name('user.update');
    Route::get('/hrm/employees', 'UsersController@showList')->name('employee.list');
    Route::get('dashboard', ['as' => 'dashboard', 'uses' => 'DashboardController@index']);
    Route::get('logout', ['as' => 'logout', 'uses' => 'HomeController@logout']);

    Route::group(['prefix' => 'hrm', 'middleware' => 'auth'], function () {
        Route::get('dashboard', 'DashboardController@hrm')->name('hrm.dashboard');
        Route::get('holidays', 'Hrm\CompanyHolidaysController@index')->name('company_holidays.index');

        //Time Logging
        Route::get('timelog', 'Hrm\TimelogsController@index')->name('timelog.index');
        Route::post('timelog', 'Hrm\TimelogsController@store')->name('timelog.store');

        // Timesheet Page
        Route::get('timesheet', 'Hrm\TimesheetController@index')->name('timesheet.index');
        Route::get('timesheet/{start_date}/{end_date}', 'Hrm\TimesheetController@getMonthlyCalender')->name('timesheet.month');

        // Leave Management
        Route::get('leaves', 'Hrm\LeavesController@index')->name('leaves.index');
        Route::get('leaves/create', 'Hrm\LeavesController@create')->name('leaves.create');
        Route::post('leaves', 'Hrm\LeavesController@store')->name('leaves.store');
    });

    Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {
        Route::get('dashboard', 'DashboardController@admin')->name('admin.dashboard');

        // Timesheet Page
        Route::get('timesheet', 'Admin\TimesheetController@index')->name('admin_timesheet.index');
        Route::get('timesheet/{start_date}/{end_date}/{user_id}', 'Admin\TimesheetController@getMonthlyCalender')->name('admin_timesheet.month');

    });
    /* End : All the Routes for the HRM Module */

    /* All the Routes for the Talent Module */
    Route::group(['prefix' => 'talent'], function () {
        Route::get('dashboard', ['as' => 'talent.dashboard', 'uses' => 'Interview\DashboardController@index']);
        // Interviews
        Route::get('interviews', ['as' => 'talent.interviews.index', 'uses' => 'Interview\InterviewController@index']);
        Route::get('interviews/create', ['as' => 'talent.interviews.create', 'uses' => 'Interview\InterviewController@create']);
        Route::post('interviews/store', ['as' => 'talent.interviews.store', 'uses' => 'Interview\InterviewController@store']);
        Route::get('interviews/edit/{id?}', ['as' => 'talent.interviews.edit', 'uses' => 'Interview\InterviewController@edit']);
        Route::post('interviews/update/{id?}', ['as' => 'talent.interviews.update', 'uses' => 'Interview\InterviewController@update']);
        Route::get('interviews/delete/{id?}', ['as' => 'talent.interviews.delete', 'uses' => 'Interview\InterviewController@delete']);
        Route::get('interviews/view/{id?}', ['as' => 'talent.interviews.view', 'uses' => 'Interview\InterviewController@view']);
        Route::post('interviews/status/{id?}', ['as' => 'talent.interviews.status.update', 'uses' => 'Interview\InterviewController@updateStatus']);

        // Candidates
        Route::get('candidates', ['as' => 'talent.candidates.index', 'uses' => 'Interview\CandidatesController@index']);
        Route::get('candidates/create', ['as' => 'talent.candidates.create', 'uses' => 'Interview\CandidatesController@create']);
        Route::post('candidates/store', ['as' => 'talent.candidates.store', 'uses' => 'Interview\CandidatesController@store']);
        Route::get('candidates/edit/{id?}', ['as' => 'talent.candidates.edit', 'uses' => 'Interview\CandidatesController@edit']);
        Route::post('candidates/update/{id?}', ['as' => 'talent.candidates.update', 'uses' => 'Interview\CandidatesController@update']);
        Route::get('candidates/delete/{id?}', ['as' => 'talent.candidates.delete', 'uses' => 'Interview\CandidatesController@delete']);
        Route::get('candidates/view/{id?}', ['as' => 'talent.candidates.view', 'uses' => 'Interview\CandidatesController@view']);
        Route::post('candidates/notes/{id?}', ['as' => 'talent.candidates.notes.store', 'uses' => 'Interview\CandidatesController@storeNotes']);
        //Story board
        Route::get('story-board', ['as' => 'talent.story_board', 'uses' => 'Interview\StoryBoardController@index']);

        // Skills
        Route::get('skills', ['as' => 'talent.skills.index', 'uses' => 'Interview\SkillsController@index']);
        Route::get('skills/create', ['as' => 'talent.skills.create', 'uses' => 'Interview\SkillsController@create']);
        Route::post('skills/store', ['as' => 'talent.skills.store', 'uses' => 'Interview\SkillsController@store']);
        Route::get('skills/edit/{id?}', ['as' => 'talent.skills.edit', 'uses' => 'Interview\SkillsController@edit']);
        Route::post('skills/update/{id?}', ['as' => 'talent.skills.update', 'uses' => 'Interview\SkillsController@update']);
        Route::post('skills/delete/{id?}', ['as' => 'talent.skills.delete', 'uses' => 'Interview\SkillsController@delete']);
        Route::get('skills/view/{id?}', ['as' => 'talent.skills.view', 'uses' => 'Interview\SkillsController@view']);

        //POSITIONS
        Route::get('positions', ['as' => 'talent.positions.index', 'uses' => 'Interview\PositionsController@index']);
        Route::get('positions/create', ['as' => 'talent.positions.create', 'uses' => 'Interview\PositionsController@create']);
        Route::post('positions/store', ['as' => 'talent.positions.store', 'uses' => 'Interview\PositionsController@store']);
        Route::get('positions/edit/{id?}', ['as' => 'talent.positions.edit', 'uses' => 'Interview\PositionsController@edit']);
        Route::post('positions/update/{id?}', ['as' => 'talent.positions.update', 'uses' => 'Interview\PositionsController@update']);
        Route::post('positions/delete/{id?}', ['as' => 'talent.positions.delete', 'uses' => 'Interview\PositionsController@destroy']);
        Route::get('positions/view/{id?}', ['as' => 'talent.positions.view', 'uses' => 'Interview\PositionsController@show']);

        // Users
        Route::get('users', ['as' => 'talent.users.index', 'uses' => 'Interview\UsersController@index']);
        Route::get('users/create', ['as' => 'talent.users.create', 'uses' => 'Interview\UsersController@create']);
        Route::post('users/store', ['as' => 'talent.users.store', 'uses' => 'Interview\UsersController@store']);
        Route::get('users/show/{id?}', ['as' => 'talent.users.show', 'uses' => 'Interview\UsersController@show']);
        Route::get('users/edit/{id?}', ['as' => 'talent.users.edit', 'uses' => 'Interview\UsersController@edit']);
        Route::post('users/{id}/update', ['as' => 'talent.users.update', 'uses' => 'Interview\UsersController@update']);
        Route::post('users/{id}/destroy', ['as' => 'talent.users.destroy', 'uses' => 'Interview\UsersController@destroy']);

        Route::get('users/activation-process/{token}', ['as' => 'user.activation-process', 'uses' => 'Interview\UsersController@activateLink']);
        Route::post('users/storeDetails', 'Interview\UsersController@storeUserDetails')->name('storeUsersDetails');

        // Questions, Examinations, Answers
        Route::get('questions', ['as' => 'talent.exam_questions', 'uses' => 'Interview\QuestionsController@index']);
        Route::get('questions/create', ['as' => 'talent.create_exam_question', 'uses' => 'Interview\QuestionsController@create']);
        Route::post('questions/store', ['as' => 'talent.store_exam_question', 'uses' => 'Interview\QuestionsController@store']);
        Route::post('questions/show/{id?}', ['as' => 'talent.show_exam_question', 'uses' => 'Interview\QuestionsController@show']);

        Route::get('exams', ['as' => 'talent.online_exams', 'uses' => 'Interview\ExamsController@index']);
        Route::get('exams/create', ['as' => 'talent.create_online_exam', 'uses' => 'Interview\ExamsController@create']);
        Route::post('exams/store', ['as' => 'talent.store_online_exam', 'uses' => 'Interview\ExamsController@store']);

        Route::get('exam/results/{id?}', ['as' => 'talent.exam_results', 'uses' => 'Interview\ExamResultsController@index']);


    });
    /* End : All the Routes for the Talent Module */
\
    /* All the Routes for the PMS Module */
    /* End : All the Routes for the PMS Module */
    /*// Interview Statuses to Candidate Statuses Mapping
    Route::get('mappings', ['as' => 'mappings.index', 'uses' => 'MappingsController@index']);
    Route::post('mappings', ['as' => 'mappings.update', 'uses' => 'MappingsController@update']);*/

    // Student Registration

    // Interview Questions related routes
// Create Student Registration Page
    Route::get('students', ['as' => 'talent.students_list', 'uses' => 'Interview\StudentsController@index']);
//    Route::get('students/create', ['as' => 'create_student', 'uses' => 'Interview\StudentsController@create']);
    Route::post('students/store', ['as' => 'store_student', 'uses' => 'Interview\StudentsController@store']);

    Route::get('talent/exams', ['as' => 'talent.exams', 'uses' => 'Interview\ExamsController@index']);
    Route::get('talent/exam/{question_id}', ['as' => 'talent.exams.question', 'uses' => 'Interview\ExamsController@createExamQuestion']);
    Route::post('talent/exam/store-answer', ['as' => 'talent.exams.store_answer', 'uses' => 'Interview\ExamsController@storeAnswer']);
    Route::get('talent/thank-you', ['as' => 'talent.exams.thanks', 'uses' => 'Interview\ExamsController@thanks']);
});
