@extends("layouts.app")
@section("content")
    <div class="container">
        <div class="row my-3">
            <div class="col-md-3">
                <div class="form-group">
                    <form action="{{route('admin_timesheet.index')}}" method="GET">
                        <select name="user_id" id="user_id" class="form-control">
                            <option value="0">Select User</option>
                            @foreach($users as $user)
                                <option value="{{$user->id}}" @if($user->id == $user_id) selected @endif>{{ $user->name }}</option>
                            @endforeach
                        </select>
                    </form>
                </div>
            </div>
        </div>
        <div class="row my-5">
            <div class="col-md-12">
                {!! $calendar->calendar() !!}
                {!! $calendar->script() !!}
            </div>
        </div>
    </div>
@endsection
@section("header_styles")
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.css"/>
    <style>
        .fc-content{
            font-size: 14px;
            margin: 2px;
            padding: 2px;
        }
        .fc-more-popover {
            z-index: 2;
            width: auto;
        }
        .fc-event{
            margin-bottom : 10px!important;
        }
    </style>

@endsection
@section("footer_scripts")
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.js"></script>
    <script>
        $(function(){
            $('#user_id').on('change', function(){
                $('form').submit();
            })
        })
    </script>
@endsection
