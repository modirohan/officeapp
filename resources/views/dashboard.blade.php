@extends('layouts.app')
@section('content')
    <div class="container my-2">
        <div class="row mt-5">
            <div class="col-md-3 text-center">
                <div class="card">
                    <div class="card-header">
                        <h2 class="mb-0">0</h2>
                    </div>
                    <div class="card-body">Total Candidates</div>
                </div>
            </div>
            <div class="col-md-3 text-center">
                <div class="card">
                    <div class="card-header">
                        <h2 class="mb-0">0</h2>
                    </div>
                    <div class="card-body">Total Candidates</div>
                </div>
            </div>
            <div class="col-md-3 text-center">
                <div class="card">
                    <div class="card-header">
                        <h2 class="mb-0">0</h2>
                    </div>
                    <div class="card-body">Total Candidates</div>
                </div>
            </div>
            <div class="col-md-3 text-center">
                <div class="card">
                    <div class="card-header">
                        <h2 class="mb-0">0</h2>
                    </div>
                    <div class="card-body">Total Candidates</div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
    <div class="">{{--bg-style--}}
        <div class="wrapper">
            <div class="row pt-5 pb-5">
                <div class="col-md-6  col-lg-3 text-center">
                    <div class="counter" data-cp-percentage="75" data-cp-color="#00bfeb">
                    </div>
                    <h4>SKILLS</h4>
                    <p>Lorem ipsum dolor sit amet, consecte elit. Condimentum porttitor cursumus.</p>
                </div>
                <div class="col-md-6 col-lg-3 text-center">
                    <div class="counter" data-cp-percentage="65" data-cp-color="#EA4C89"></div>
                    <h4>CONTENT</h4>
                    <p>Lorem ipsum dolor sit amet, consecte elit. Condimentum porttitor cursumus.</p>
                </div>
                <div class="col-md-6 col-lg-3 text-center">
                    <div class="counter" data-cp-percentage="35" data-cp-color="#FF675B"></div>

                    <h4>WEB SITES</h4>
                    <p>Lorem ipsum dolor sit amet, consecte elit. Condimentum porttitor cursumus.</p>
                </div>
                <div class="col-md-6 col-lg-3 text-center">
                    <div class="counter" data-cp-percentage="44" data-cp-color="#FF9900"></div>
                    <h4>EMPLOYEES</h4>
                    <p>Lorem ipsum dolor sit amet, consecte elit. Condimentum porttitor cursumus.</p>
                </div>
            </div>
        </div>
    </div>
    </div>
    <div class="container mt-5">
        <div class="">{{--bg-style--}}
            <div class="wrapper">
                <div class="row pt-5 pb-5">
                    <div class="col-md-6 col-lg-3 text-center">
                        <div class="counter" data-cp-percentage="75" data-cp-color="#00bfeb">
                        </div>
                        <h4>SKILLS</h4>
                        <p>Lorem ipsum dolor sit amet, consecte elit. Condimentum porttitor cursumus.</p>
                    </div>
                    <div class="col-md-6 col-lg-3 text-center">
                        <div class="counter" data-cp-percentage="65" data-cp-color="#EA4C89"></div>
                        <h4>CONTENT</h4>
                        <p>Lorem ipsum dolor sit amet, consecte elit. Condimentum porttitor cursumus.</p>
                    </div>
                    <div class="col-md-6 col-lg-3 text-center">
                        <div class="counter" data-cp-percentage="35" data-cp-color="#FF675B"></div>

                        <h4>WEB SITES</h4>
                        <p>Lorem ipsum dolor sit amet, consecte elit. Condimentum porttitor cursumus.</p>
                    </div>
                    <div class="col-md-6 col-lg-3 text-center">
                        <div class="counter" data-cp-percentage="44" data-cp-color="#FF9900"></div>
                        <h4>EMPLOYEES</h4>
                        <p>Lorem ipsum dolor sit amet, consecte elit. Condimentum porttitor cursumus.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer_scripts')
    <script>
        document.addEventListener("DOMContentLoaded", function() {

            var circleProgress = (function(selector) {
                var wrapper = document.querySelectorAll(selector);
                Array.prototype.forEach.call(wrapper, function(wrapper, i) {
                    var wrapperWidth,
                        wrapperHeight,
                        percent,
                        innerHTML,
                        context,
                        lineWidth,
                        centerX,
                        centerY,
                        radius,
                        newPercent,
                        speed,
                        from,
                        to,
                        duration,
                        start,
                        strokeStyle,
                        text;

                    var getValues = function() {
                        wrapperWidth = parseInt(window.getComputedStyle(wrapper).width);
                        wrapperHeight = wrapperWidth;
                        percent = wrapper.getAttribute('data-cp-percentage');
                        innerHTML = '<span class="percentage"><strong>' + percent + '</strong> %</span><canvas class="circleProgressCanvas" width="' + (wrapperWidth * 2) + '" height="' + wrapperHeight * 2 + '"></canvas>';
                        wrapper.innerHTML = innerHTML;
                        text = wrapper.querySelector(".percentage");
                        canvas = wrapper.querySelector(".circleProgressCanvas");
                        wrapper.style.height = canvas.style.width = canvas.style.height = wrapperWidth + "px";
                        context = canvas.getContext('2d');
                        centerX = canvas.width / 2;
                        centerY = canvas.height / 2;
                        newPercent = 0;
                        speed = 1;
                        from = 0;
                        to = percent;
                        duration = 1000;
                        lineWidth = 25;
                        radius = canvas.width / 2 - lineWidth;
                        strokeStyle = wrapper.getAttribute('data-cp-color');
                        start = new Date().getTime();
                    };

                    function animate() {
                        requestAnimationFrame(animate);
                        var time = new Date().getTime() - start;
                        if (time <= duration) {
                            var x = easeInOutQuart(time, from, to - from, duration);
                            newPercent = x;
                            text.innerHTML = Math.round(newPercent) + " %";
                            drawArc();
                        }
                    }

                    function drawArc() {
                        var circleStart = 1.5 * Math.PI;
                        var circleEnd = circleStart + (newPercent / 50) * Math.PI;
                        context.clearRect(0, 0, canvas.width, canvas.height);
                        context.beginPath();
                        context.arc(centerX, centerY, radius, circleStart, 4 * Math.PI, false);
                        context.lineWidth = lineWidth;
                        context.strokeStyle = "#ddd";
                        context.stroke();
                        context.beginPath();
                        context.arc(centerX, centerY, radius, circleStart, circleEnd, false);
                        context.lineWidth = lineWidth;
                        context.strokeStyle = strokeStyle;
                        context.stroke();

                    }
                    var update = function() {
                        getValues();
                        animate();
                    }
                    update();



                    wrapper.addEventListener("click", function() {
                        update();
                    });

                    var resizeTimer;
                    window.addEventListener("resize", function() {
                        clearTimeout(resizeTimer);
                        resizeTimer = setTimeout(function() {
                            clearTimeout(resizeTimer);
                            start = new Date().getTime();
                            update();
                        }, 250);
                    });
                });

                //
                // http://easings.net/#easeInOutQuart
                //  t: current time
                //  b: beginning value
                //  c: change in value
                //  d: duration
                //
                function easeInOutQuart(t, b, c, d) {
                    if ((t /= d / 2) < 1) return c / 2 * t * t * t * t + b;
                    return -c / 2 * ((t -= 2) * t * t * t - 2) + b;
                }

            });

            circleProgress('.counter');

            // Gibt eine Zufallszahl zwischen min (inklusive) und max (exklusive) zurück
            function getRandom(min, max) {
                return Math.random() * (max - min) + min;
            }
        });
    </script>
@endsection
