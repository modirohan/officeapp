@extends('layouts.app')

@section('content')
    <div class="container" id="Employees-List">
<h1>Employees-List</h1>
       <div class="row">
        <div class="col-md-3">
          <div class="card">
              <div class="img-box">
                  <img class="card-img-top" src="https://www.jamf.com/jamf-nation/img/default-avatars/generic-user-purple.png" alt="Card image cap">
              </div>
              <div class="card-body">
                <h6 class="card-text">Name :</h6>
                <h6 class="card-text">position :</h6>
                <h6 class="card-text">joined since info :</h6>
              </div>
           </div>
        </div>
           <div class="col-md-3">
               <div class="card">
                   <div class="img-box">
                       <img class="card-img-top" src="https://www.jamf.com/jamf-nation/img/default-avatars/generic-user-purple.png" alt="Card image cap">
                   </div>
                   <div class="card-body">
                       <h6 class="card-text">Name :</h6>
                       <h6 class="card-text">position :</h6>
                       <h6 class="card-text">joined since info :</h6>
                   </div>
               </div>
           </div>
           <div class="col-md-3">
               <div class="card">
                   <div class="img-box">
                       <img class="card-img-top" src="https://www.jamf.com/jamf-nation/img/default-avatars/generic-user-purple.png" alt="Card image cap">
                   </div>
                   <div class="card-body">
                       <h6 class="card-text">Name :</h6>
                       <h6 class="card-text">position :</h6>
                       <h6 class="card-text">joined since info :</h6>
                   </div>
               </div>
           </div>
           <div class="col-md-3">
               <div class="card">
                   <div class="img-box">
                   <img class="card-img-top" src="https://www.jamf.com/jamf-nation/img/default-avatars/generic-user-purple.png" alt="Card image cap">
                   </div>
                   <div class="card-body">
                       <h6 class="card-text">Name :</h6>
                       <h6 class="card-text">position :</h6>
                       <h6 class="card-text">joined since info :</h6>
                   </div>
               </div>
           </div>
       </div>
    </div>
@endsection

@section('header_styles')
    <link rel="stylesheet" href="{{asset('global/css/custom/employees_list.css')}}">
    @endsection

@section('footer_scripts')

    @endsection