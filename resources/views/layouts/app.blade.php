<!doctype html>
<html lang="en">
<head>
    <title>Urvam | Internal Application</title>
    <!-- Required meta tags -->
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="Content-Language" content="en"/>

    <meta name="msapplication-TileColor" content="#2d89ef">
    <meta name="theme-color" content="#4188c9">

    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">

    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/custom.css') }}">
    <link rel="stylesheet" href="{{ asset('theme/assets/css/dashboard.css') }}">
    <link rel="stylesheet" href="{{ asset('theme/assets/css/bootstrap-datepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('theme/assets/plugins/charts-c3/plugin.css') }}">
    <link rel="stylesheet" href="{{ asset('theme/assets/plugins/maps-google/plugin.css') }}">
    <link rel="stylesheet" href="//use.fontawesome.com/releases/v5.6.1/css/all.css"
          integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">
    <link href="//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-bs4.css" rel="stylesheet">
    @yield('header_styles')

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="//stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
            integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
            crossorigin="anonymous"></script>
    <script src="{{ asset('theme/assets/plugins/maps-google/plugin.js') }}"></script>
    <script src="{{ asset('theme/assets/js/bootstrap-datepicker.min.js') }}"></script>

</head>
<body>
<div class="page">
    <div class="page-main">
        @include('layouts.partials.header')
        @include('layouts.partials.flash_message')
        @yield('content')
        @include('layouts.partials.footer')
    </div>
</div>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="//stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
        crossorigin="anonymous"></script>
<script src="{{ asset('theme/assets/plugins/maps-google/plugin.js') }}"></script>
<script src="//code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>


<script src="{{asset('global/plugins/bootstrapValidator/bootstrapValidator.min.js')}}"></script>
<script src="{{asset('global/plugins/summernote/summernote-bs4.js')}}"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<script>
    $(document).ready(function () {
        $.validate();
        // Binding common event on all the elements with wysiwyg_editor class
        if ($('.wysiwyg_editor').length > 0) {
            $('.wysiwyg_editor').summernote({
                tabsize: 4,
                height: 250
            });
        }
    });
</script>
@yield('footer_scripts')
</body>
</html>