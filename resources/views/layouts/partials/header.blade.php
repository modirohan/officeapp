<div class="header py-4 bg-light-yellow">
    <div class="container">
        <div class="d-flex align-items-center">
            <a class="header-brand text-dark" href="{{ route('dashboard') }}">
                {{--Urvam - Hire Talent--}}
                <img src="http://www.urvam.com/assets/img/urvamlogo.png" class="img-fluid" alt="">
            </a>
            <div class="m-hide ml-auto">
                <ul class="nav hire-details">
                    {{--@if(auth()->user()->is_timetracking_enabled ==1)--}}
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('hrm.dashboard') }}">HRM</a>
                        </li>
                    {{--@endif--}} 
                     {{--@if(auth()->user()->is_interview_enabled ==1)--}}
                        <li class="nav-item"><a class="nav-link" href="{{ route('talent.dashboard') }}">INTERVIEW</a>
                        </li>
                    {{--@endif
                    @if(auth()->user()->is_pms_enabled ==1)--}}
                        <li class="nav-item"><a class="nav-link" href="javascript:;">PMS</a></li>
                    {{--@endif
                    @if(auth()->user()->is_admin ==1)--}}
                        <li class="nav-item"><a class="nav-link" href="{{ route('admin.dashboard') }}">ADMIN</a></li>
                    {{--@endif--}}
                </ul>
            </div>

            <div class="d-flex order-lg-2 ml-auto">
                <div class="dropdown d-none d-md-flex">
                    <a class="nav-link icon" data-toggle="dropdown">
                    </a>
                </div>
                <div class="dropdown">
                    <a href="#" class="nav-link pr-0 leading-none" data-toggle="dropdown">
                        <span class="ml-2 d-none d-lg-block">
                            <a href="{{route('user.profile')}}"
                               style="font-weight:bold;color:#fff;">{{ Auth::user()->name }}</a>
                            <small class="d-block mt-1 text-light">{{ (Auth::user()->is_admin == 1) ? 'Administrator' : '' }}</small>
                        </span>
                    </a>
                    <li class="list-inline-item">
                        <a href="{{ route('logout') }}" class="font-weight-bold"
                           style="text-decoration:none;color:#fff;">
                            Log Out
                        </a>
                    </li>
                    <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                        <a class="dropdown-item" href="#">
                            <i class="dropdown-icon fe fe-user"></i> Profile
                        </a>
                        <a class="dropdown-item" href="#">
                            <i class="dropdown-icon fe fe-settings"></i> Settings
                        </a>
                        <a class="dropdown-item" href="#">
                            <span class="float-right"><span class="badge badge-primary">6</span></span>
                            <i class="dropdown-icon fe fe-mail"></i> Inbox
                        </a>
                        <a class="dropdown-item" href="#">
                            <i class="dropdown-icon fe fe-send"></i> Message
                        </a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">
                            <i class="dropdown-icon fe fe-help-circle"></i> Need help?
                        </a>
                        <a class="dropdown-item" href="#">
                            <i class="dropdown-icon fe fe-log-out"></i> Sign out
                        </a>
                    </div>
                </div>
            </div>
            <a href="#" class="header-toggler  d-lg-none ml-3 ml-lg-0" data-toggle="collapse"
               data-target="#headerMenuCollapse">
                <span class="header-toggler-icon"></span>
            </a>
        </div>
    </div>

</div>

@if(Request::is('talent/*'))
    <div class="collapse d-lg-flex dashboard-items" id="headerMenuCollapse">{{--bg-teal-light--}}
        <div class="container-fluid">
            <div class="row align-items-center justify-content-center m-auto">
                <div class="col-lg-12 justify-content-center d-lg-flex">
                    <ul class="nav nav-tabs border-0 text-dark">
                        <li class="nav-item">
                            <a href="{{ route('talent.dashboard') }}" class="nav-link">
                                {{--<i class="fa fa-tachometer"></i> --}}
                                <i class="fa fa-tachometer" aria-hidden="true"></i>Dashboard
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('talent.interviews.index') }}" class="nav-link">
                                <i class="fa fa-users"></i> &nbsp;Interviews
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('talent.candidates.index') }}" class="nav-link"><i
                                        class='fas fa-child'></i> Candidates</a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('talent.story_board') }}" class="nav-link">
                                <i class="fa fa-tty" aria-hidden="true"></i>
                                Hiring Storyboard
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('talent.positions.index') }}" class="nav-link">
                                <i class="fa fa-street-view" aria-hidden="true"></i>
                                Positions
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('talent.skills.index') }}" class="nav-link">
                                <i class="fa fa-music" aria-hidden="true"></i>
                                Skills
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('talent.users.index') }}" class="nav-link">
                                <i class="fa fa-user" aria-hidden="true"></i>
                                Users
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('talent.students_list') }}" class="nav-link">
                                <i class="fa fa-users" aria-hidden="true"></i>
                                Students
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('talent.students_list') }}" class="nav-link">
                                <i class="fa fa-note" aria-hidden="true"></i>
                                Exams
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('talent.exam_results') }}" class="nav-link">
                                <i class="fa fa-th-list" aria-hidden="true"></i>
                                Results
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    {{--for mobile view--}}
    <div class="m-show m-hire-details m-show">
        <ul>
            <li><a href="#">HRM</a></li>
            <li><a href="#">PMS</a></li>
            <li><a href="#">INTERVIEW</a></li>
            <li><a href="#">ADMIN</a></li>
        </ul>
    </div>
@endif

@if(Request::is('hrm/*'))
    <div class="collapse d-lg-flex p-0 dashboard-items" id="headerMenuCollapse">
        <div class="container">
            <div class="row align-items-center justify-content-center m-auto">
                <div class="col-lg-12 justify-content-center d-lg-flex">
                    <ul class="nav nav-tabs border-0 text-dark">
                        <li class="nav-item">
                            <a href="{{ route('hrm.dashboard') }}" class="nav-link">
                                <i class="fa fa-tachometer" aria-hidden="true"></i>Dashboard</a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('timelog.index') }}" class="nav-link">
                                <i class="fa fa-user" aria-hidden="true"></i> Time Log</a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('timesheet.index') }}" class="nav-link">
                                <i class="fa fa-clock" aria-hidden="true"></i> Timesheet</a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('leaves.index') }}" class="nav-link">
                                <i class="fa fa-user" aria-hidden="true"></i> Leave Management</a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('company_holidays.index') }}" class="nav-link">
                                <i class="fa fa-calendar" aria-hidden="true"></i>Company Holidays</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endif

@if(Request::is('pms/*'))
    <div class="header collapse d-lg-flex p-0 bg-teal-light" id="headerMenuCollapse">
        <div class="container">
            <div class="row align-items-center justify-content-center m-auto">
                <div class="col-lg-12 justify-content-center d-lg-flex">
                    <ul class="nav nav-tabs border-0 text-dark">
                        <li class="nav-item">
                            <a href="{{ route('users.index') }}" class="nav-link">
                                <i class="fa fa-user" aria-hidden="true"></i> Users</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endif

@if(Request::is('admin/*'))
    <div class="header collapse d-lg-flex p-0 bg-teal-light" id="headerMenuCollapse">
        <div class="container">
            <div class="row align-items-center justify-content-center m-auto">
                <div class="col-lg-12 justify-content-center d-lg-flex">
                    <ul class="nav nav-tabs border-0 text-dark">
                        <li class="nav-item">
                            <a href="{{ route('admin_timesheet.index') }}" class="nav-link">
                                <i class="fa fa-user" aria-hidden="true"></i> Timesheet</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endif


