<div class="clearfix before-gap"></div>
<footer class="footer bg-light-yellow fixed-bottom">{{--bg-azure-light--}}
    <div class="container">
        <div class="row align-items-center flex-row-reverse">
            <div class="col-12 col-lg-auto mt-3 mt-lg-0 text-center text-white">
                Copyright © 2019 Urvam Technologies
            </div>
        </div>
    </div>
</footer>
