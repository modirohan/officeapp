@component('mail::message')
# Hello <span>{{ $user_name }}</span>

@component('mail::button', ['url' => $url])
Activate
@endcomponent

@component('mail::Panel')
<p>Click this activation link and add your password...</p>
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent