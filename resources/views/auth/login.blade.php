<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">

    <title>Urvam-Signin</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="//stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <!-- Custom styles for this template -->
    <link href="{{ asset('css/login.css') }}" rel="stylesheet">
</head>

<body>

<div class="contianer">
    <div class="signin-box text-center">
        <h1 class="form-title">Urvam</h1>
        <p class="form-description">Please sign in</p>
        @include('layouts.partials.flash_message')
        <form method="POST" class="form form-signin" action="{{ route('user.authenticate') }}">
            {{ csrf_field() }}
            <div class="field">
                <input type="email" name="email" id="email" placeholder="Email address">
                <label for="email">Email address</label>
                {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
            </div>
            <div class="field">
                <input type="password" name="password" id="password" placeholder="Password">
                <label for="password">Password</label>
                {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
            </div>
            <div class="checkbox mb-3">
                <input type="checkbox" class="check-remember" name="remember-me"> Remember me
            </div>
            <button class="btn btn-lg btn-signin btn-block" type="submit">Sign in</button>
            <a href="{{ route('login.forgot-password') }}" class="forgot-pass">forgot password?</a>
        </form>
    </div>
</div>
</body>
</html>




