<div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
    <div class="div ">
        <p class="text-center"><i class="fa fa-cloud-upload" aria-hidden="true"></i></p>
        <p class="page">Upload Candidate CV/Resume Here</p>
    </div>
    <div class="mt-5"><i class="fa fa-user" aria-hidden="true"></i><b> Personal Information</b></div>
    {!! $errors->first('title', '
    <p class="help-block">:message</p>
    ') !!}
</div>
<div class=" row">
    <div class="col-sm-6">
        <div class="required-field r-partial pb-5">
            <label class="label">Name</label>
            <div class="control firstcontrol is-clearfix" aria-required="true" aria-invalid="true">
                <input type="text" autocomplete="on" maxlength="40" dir="auto" id="name" name="name" class="input w-100 pl-2 form-control"> <!----> <!----> <small class="help counter is-invisible">
                </small>
            </div>
        </div>
        <div class="required-field r-partial pb-5">
            <label class="label">Email</label>
            <div class="control firstcontrol is-clearfix" aria-required="true" aria-invalid="true">
                <input type="text" autocomplete="on" maxlength="40" dir="auto" id="email" name="email"  class="input w-100 pl-2 form-control"> <!----> <!----> <small class="help counter is-invisible">
                </small>
            </div>
        </div>
        <div class="required-field r-partial pb-5">
            <label class="label">Country</label>
            <div class="control firstcontrol is-clearfix" aria-required="true" aria-invalid="true">
                <input type="text" autocomplete="on" maxlength="40" dir="auto" id="country" name="country"  class="input w-100 pl-2 form-control"> <!----> <!----> <small class="help counter is-invisible">
                </small>
            </div>
        </div>
        <div class="required-field r-partial pb-5">
            <label class="label">State</label>
            <div class="control firstcontrol is-clearfix" aria-required="true" aria-invalid="true">
                <input type="text" autocomplete="on" maxlength="40" dir="auto" id="state" name="state"  class="input w-100 pl-2 form-control"> <!----> <!----> <small class="help counter is-invisible">
                </small>
            </div>
        </div>
        <div class="required-field r-partial pb-5">
            <label class="label">City</label>
            <div class="control firstcontrol is-clearfix" aria-required="true" aria-invalid="true">
                <input type="text" autocomplete="on" maxlength="40" dir="auto" id="city" name="city"  class="input w-100 pl-2 form-control"> <!----> <!----> <small class="help counter is-invisible">
                </small>
            </div>
        </div>
        <div class="required-field r-partial pb-5">
            <label class="label">Date of Birth</label>
            <div class="input-group date datepicker">
                <input type="text"  class="form-control">
                <div class="input-group-addon">
                    <span class="glyphicon glyphicon-th"></span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="required-field r-partial pb-5">
            <label class="label">Mobile</label>
            <div class="control firstcontrol is-clearfix" aria-required="true" aria-invalid="true">
                <input type="text" autocomplete="on" maxlength="40" dir="auto" id="sTest-candidateFirstName" name="firstname"  class="input w-100 pl-2 form-control"> <!----> <!----> <small class="help counter is-invisible">
                </small>
            </div>
        </div>
        <div class="required-field r-partial pb-5">
            <label class="label">Gender</label>
            <div class="row ml-0">
                <input type="radio" class="mt-2 mr-1" name="gender" value="male" checked> Male<br>
                <input type="radio" class="mt-2 mr-1 ml-1" name="gender" value="female"> Female<br>
                <input type="radio" class="mt-2 mr-1 ml-1" name="gender" value="other"> Other
                <div>
                </div>
            </div>
        </div>
        <div class="required-field r-partial pb-5">
            <label class="label">Current Address</label>
            <div class="control firstcontrol is-clearfix" aria-required="true" aria-invalid="true">
                <textarea name="address_line1" id="address_line1" class="form-control"></textarea>
                <small class="help counter is-invisible"></small>
            </div>
        </div>
        <div class="required-field r-partial pb-5">
            <label class="label">Permanent Address</label>
            <div class="control firstcontrol is-clearfix" aria-required="true" aria-invalid="true">
                <textarea name="address_line2" id="address_line2" class="form-control"></textarea>
                <small class="help counter is-invisible"></small>
            </div>
        </div>
        <div class="required-field r-partial pb-5">
            <label class="label">Zip code</label>
            <div class="control firstcontrol is-clearfix" aria-required="true" aria-invalid="true">
                <input type="text" autocomplete="on" maxlength="40" dir="auto" id="zip_code" name="zip_code" class="input w-100 pl-2 form-control"> <!----> <!----> <small class="help counter is-invisible">
                </small>
            </div>
        </div>
    </div>
</div>
<div class="mt-5"><i class="fa fa-university"></i><b> Academic Information</b></div>
<div class="row mt-3">
    <div class="col-sm-6">
        <div>
            <label class="label">Educational Qualification</label>
            <div class="autocomplete control is-icon-right">
                <div class="control has-icons-left is-clearfix">
                    <input type="text" autocomplete="off" id="degree" name="degree" class="input w-100 pl-2 form-control" > <!---->
                </div>
            </div>
        </div>
        <div>
            <label class="label">Colleges</label>
            <div class="autocomplete control is-icon-right">
                <div class="control has-icons-left is-clearfix">
                    <input type="text" autocomplete="off" id="college" name="college" class="input w-100 pl-2 form-control" > <!---->
                </div>
            </div>
        </div>
        <!---->
    </div>
    <div class="col-sm-6">
        <div>
            <label class="label">Educational Specialization</label>
            <div class="control is-clearfix">
                <input type="text" autocomplete="off" id="degree_information" name="degree_information"  class="input w-100 pl-2 form-control">
            </div>
        </div>
        <div>
            <label class="label">University</label>
            <div class="autocomplete control is-icon-right">
                <div class="control has-icons-left is-clearfix">
                    <input type="text" autocomplete="off" id="university" name="university" class="input w-100 pl-2 form-control" > <!---->
                </div>
            </div>
        </div>
    </div>
</div>
    <div>
        <label class="label">Academic Year</label>
        <div class="autocomplete control is-icon-right">
            <div class="control has-icons-left is-clearfix">
                <input type="text" autocomplete="off" id="academic_year" name="academic_year" class="input w-100 pl-2 form-control" > <!---->
            </div>
        </div>
    </div>
<div class="mt-5"><i class="fa fa-users" aria-hidden="true"></i><b> Employment Information</b></div>
<div class="row mt-3">
    <div class="col-sm-6">
        <div>
            <label class="label">Current / Last Organisation</label>
            <div class="autocomplete control is-icon-right">
                <div class="control has-icons-left is-clearfix">
                    <input type="text" autocomplete="off" id="organization" name="organization" class="input w-100 pl-2 form-control" > <!---->
                </div>
            </div>
        </div>
        <div class="mt-4">
            <label class="label">Total Experience (Years)
            </label>
            <div class="autocomplete control is-icon-right">
                <div class="control has-icons-left is-clearfix">
                    <input type="text" autocomplete="off" id="total_experience" name="total_experience" class="input w-100 pl-2 form-control" > <!---->
                </div>
            </div>
        </div>
        <div class="mt-4">
            <label class="label">Current Salary</label>
            <div class="autocomplete control is-icon-right">
                <div class="control has-icons-left is-clearfix">
                    <input type="text" autocomplete="off" id="current_salary" name="current_salary" class="input w-100 pl-2 form-control" > <!---->
                </div>
            </div>
        </div>
        <div class="mt-4">
            <label class="label">Current Employment Status</label>
            <div class="autocomplete control is-icon-right">
                <div class="control has-icons-left is-clearfix">
                    <input type="text" autocomplete="off" id="employment_status" name="employment_status" class="input w-100 pl-2 form-control" > <!---->
                </div>
            </div>
        </div>
        <div class="mt-4">
            <label class="label">Available from</label>
            <div class="autocomplete control is-icon-right">
                <div class="control has-icons-left is-clearfix">
                    <div class="input-group date datepicker">
                        <input type="text" autocomplete="off" class="input w-100 pl-2 form-control" id="immediate_joining" name="immediate_joining">
                        <div class="input-group-addon">
                            <span class="glyphicon glyphicon-th"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div>
            <label class="label"> Positions</label>
            <div class="control is-clearfix">
                {!! Form::select('position_id',$positions,null,['class'=>'form-control','id'=>'position_id']) !!}
            </div>
        </div>
        <div class="mt-4">
            <label class="label">Relevant Experience (Years)</label>
            <div class="autocomplete control is-icon-right">
                <div class="control has-icons-left is-clearfix">
                    <input type="text" autocomplete="off" id="relevant_experience" name="relevant_experience" class="input w-100 pl-2 form-control" > <!---->
                </div>
            </div>
        </div>
        <div class="mt-4">
            <label class="label">Currency Type</label>
            <div class="autocomplete control is-icon-right">
                <div class="control has-icons-left is-clearfix">
                    <input type="text" autocomplete="off" id="currency" name="currency" class="input w-100 pl-2 form-control" > <!---->
                </div>
            </div>
        </div>
        <div class="mt-4">
            <label class="label">Salary Expectation</label>
            <div class="autocomplete control is-icon-right">
                <div class="control has-icons-left is-clearfix">
                    <input type="text" autocomplete="off" id="expected_salary" name="expected_salary" class="input w-100 pl-2 form-control" > <!---->
                </div>
            </div>
        </div>
        <div class="mt-4">
            <label class="label">Notice Period (days)</label>
            <div class="autocomplete control is-icon-right">
                <div class="control has-icons-left is-clearfix">
                    <input type="text" autocomplete="off" id="notice_period" name="notice_period" class="input w-100 pl-2 form-control" > <!---->
                </div>
            </div>
        </div>
    </div>
</div>
<div class="mt-5">
    <i class="fa fa-bookmark"></i><b> Resume, Language and Technical Skills</b>
    <div class="mt-4">
        <label class="label">Skills</label>
        <div class="autocomplete control is-icon-right">
            <div class="control has-icons-left is-clearfix">
                <input type="text" autocomplete="off" id="sTest-candidateQualification" class="input w-100 pl-2 form-control" > <!---->
            </div>
        </div>
    </div>
</div>
<div class="row mt-3">
    <div class="col-sm-6">
        <div>
            <label class="label">Language</label>
            <div class="autocomplete control is-icon-right">
                <div class="control has-icons-left is-clearfix">
                    <input type="text" autocomplete="off" id="sTest-candidateQualification" class="input w-100 pl-2 form-control" > <!---->
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div>
            <label class="label">Proficiency Lavel</label>
            <div class="control is-clearfix">
                <input type="text" autocomplete="on" maxlength="20" id="sTest-candidateSpecialization"  class="input w-100 pl-2 form-control"> <!----> <!----> <small class="help counter is-invisible">
                </small>
            </div>
        </div>
    </div>
</div>
<div class="mt-5"><i class="fa fa-firefox"></i><b> RSocial Profile Links</b>
</div>
<div class="row mt-3">
    <div class="col-sm-6">
        <div>
            <label class="label">Facebook Profile URL</label>
            <div class="autocomplete control is-icon-right">
                <div class="control has-icons-left is-clearfix">
                    <input type="text" autocomplete="off" id="facebook_url" name="facebook_url" class="input w-100 pl-2 form-control" > <!---->
                </div>
            </div>
        </div>
        <div>
            <label class="label  mt-4">Linkedin Profile URL</label>
            <div class="autocomplete control is-icon-right">
                <div class="control has-icons-left is-clearfix">
                    <input type="text" autocomplete="off" id="linkedin_address" name="linkedin_address" class="input w-100 pl-2 form-control" > <!---->
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div>
            <label class="label">Skype address</label>
            <div class="control is-clearfix">
                <input type="text" autocomplete="on" maxlength="20" id="skype_address" name="skype_address"  class="input w-100 pl-2 form-control"> <!----> <!----> <small class="help counter is-invisible">
                </small>
            </div>
        </div>
        <div>
            <label class="label  mt-4">GitHub Profile URL</label>
            <div class="control is-clearfix">
                <input type="text" autocomplete="on" maxlength="20" id="sTest-candidateSpecialization"  class="input w-100 pl-2 form-control"> <!----> <!----> <small class="help counter is-invisible">
                </small>
            </div>
        </div>
    </div>
</div>
<div class="mt-4">
    <label class="label">Additional Information</label>
    <div class="autocomplete control is-icon-right">
        <div class="control has-icons-left is-clearfix">
            <textarea name="additional_info" id="additional_info" class="form-control"></textarea>
        </div>
    </div>
</div>
<div class="mt-4">
    <label class="label">Referance note</label>
    <div class="autocomplete control is-icon-right">
        <div class="control has-icons-left is-clearfix">
            <textarea name="reference_notes" id="reference_notes" class="form-control"></textarea>
        </div>
    </div>
</div>
<div class="mt-4">
    <label class="label">resume </label>
    <input type="file" id="input-file-now" name="resume" class="file-upload" />
</div>
<div class="form-group text-center mt-5">
    <input class="btn btn-primary btn-lg w-25" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Submit' }}">

</div>
<script>
    $('.datepicker').datepicker();
</script>
