<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Urvam Technologies | Candidate Tracking System</title>
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" id="bootstrap-css">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/responsive.css') }}" rel="stylesheet">

    <!------ Include the above in your HEAD tag ---------->
</head>
<body>
<!-- Tabs -->
<section id="tabs">
    <div class="container register">
        <div class="row register-form">
            <div class="col-lg-12 col-sm-12 col-xs-12 register-right">
                <div class="row register-form">
                    <div class="col-lg-12">
                        <div class="row">
                            <div class="center col-lg-12 form-group">
                                <img src="{{ url('images/logo.png') }}" class="img-fluid"/>
                            </div>
                            <div class="col-lg-12 center bottom-text">
                                <label for="Campus Placement Programme-2019-2020" class="theme-color-text">Campus
                                    Placement Programme-2019-2020</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 center">
                        <h1 class="section-title h1 text-color">Test</h1>
                    </div>
                    <div class="col-lg-12">
                        <nav>
                            <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                                <a class="nav-item nav-link active col-lg-6 tab-text-large" id="nav-home-tab"
                                   data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home"
                                   aria-selected="true">Aptitude Test</a>

                            </div>
                        </nav>
                        <div class="tab-content py-3 px-3 px-sm-0 bg-color" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="nav-home" role="tabpanel"
                                 aria-labelledby="nav-home-tab">
                                <h5 class="text-space">Total Questions : 15</h5>
                                <hr>
                                <h5 class="text-space">Time Of Complete : 25 Minutes</h5>
                                <hr>
                                <h5 class="text-space">Details : </h5>
                                <p> Each question has multiple options to choose as the correct answer. Please choose the correct radio option as the answer of your choice. The answer cannot be changed after submission.</p>
                                <hr>
                            </div>

                        <div class="center-button col-lg-12">
                            <a href="{{ route('talent.exams.question', 1) }}" class="btn btnRegister text-white">
                                Start Exam
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/jquery.min.js') }}"></script>
<!-- ./Tabs -->
<!--<script>
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        e.target // newly activated tab
        e.relatedTarget // previous active tab
    })
</script>-->
</body>
</html>