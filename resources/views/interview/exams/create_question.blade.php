<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Urvam Technologies | Candidate Tracking System</title>

    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" id="bootstrap-css">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/responsive.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/font-awesome.min.css') }}">
    <!------ timer ---------->
    <link href="{{ asset('css/jquerysctipttop.css') }}" rel="stylesheet" type="text/css">

    <!------ datepicker ---------->
    <link href="{{ asset('css/gijgo.min.css') }}" rel="stylesheet" type="text/css"/>
    <!------ Include the above in your HEAD tag ---------->
</head>
<body>
<section>
    <div class="container register">
        <div class="row register-form">
            <!--<div class="col-lg-3  col-sm-12 col-xs-12 register-left">
                <img src="https://image.ibb.co/n7oTvU/logo_white.png" alt=""/>
                <h3>Welcome</h3>
                <p></p>
            </div>-->
            <div class="col-lg-12 col-sm-12 col-xs-12 register-right">
                <!-- <div class="login-form-1">-->
                <div class="row register-form ">
                    <div class="col-lg-12">
                        <div class="row">
                            <div class="center col-lg-12 form-group">
                                <img src="{{ url('images/logo.png') }}" class="img-fluid"/>
                            </div>
                            <div class="col-lg-12 center bottom-text">
                                <label for="Campus Placement Programme-2019-2020" class="theme-color-text">Campus
                                    Placement Programme-2019-2020</label>
                            </div>
                        </div>
                        <div class="divider-25"></div>
                        <div class="divider-50"></div>
                    </div>
                    <div class="col-lg-6  col-xs-12 col-sm-12 text-center bottom-text">
                        <h1 class="text-xs-center float-lg-left question-title text-color">QUESTIONS</h1>
                    </div>
                    <hr>
                    <form method="POST" action="{{ route('talent.exams.store_answer') }}" accept-charset="UTF-8"
                          class="form-horizontal"
                          enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="col-lg-12">
                            <div class="col-lg-12">
                                <h1 class="text-medium font-weight-text1">{{ $question_detail['id'] }}
                                    ).&nbsp;{{ $question_detail['title'] }}</h1>
                            </div>
                            <input type="hidden" name="question_id" value="{{ $question_detail['id'] }}">
                            @if(count($question_detail['relatedAnswers']) > 0)
                                @foreach($question_detail['relatedAnswers'] as $answerDetail)
                                    <div class="col-lg-12">
                                        <span class="text-space-radio"> <input type="radio" name="answer_id"
                                                                               id="{{ $answerDetail['id'] }}"
                                                                               value="{{ $answerDetail['id'] }}"></span>
                                        <span class="text-space-radio font-size">{{ $answerDetail['title'] }}</span>
                                    </div>
                                @endforeach
                            @endif
                            <hr>
                            <div class="row">
                                <div class="col-lg-6 text-right col-xs-6 col-sm-6">
                                    <button type="submit" class="btn btn-outline-info col-lg-5">Next &raquo;</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/jquery-countdown-timer-control.js') }}"></script>
<script>
    $(function () {
        //default
        var timer0 = $('.timer0').startTimer({}).trigger('start');

        //with control
        var timer1 = $('.timer1').startTimer({
            onComplete: function (element) {
                console.log('Complete');
            }
            , onPause: $('.timerpause'),
            onReset: $('.timerreset'),
            onStart: $('.timerstart'),
            loop: true
        });

        //without control
        var timer2 = $('.timer2').startTimer({
            onComplete: function (element) {
                console.log('Complete');
            }
            //,onPause: $('.timerpause'), //optional, pause control
            //onReset: $('.timerreset'), //optional, reset control
            //onStart: $('.timerstart') //optional, start control
            //loop: true, //optional, enable loop
            //loopInterval: 2 //optional
        });
        timer2.trigger('start'); //optional, use to start without control button
        //timer2.trigger('pause'); //optional, use to pause without control button
        //timer2.trigger('resetime'); //optional, use to reset without control button
    })
</script>

<!-- ./Tabs -->
<!--<script>
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        e.target // newly activated tab
        e.relatedTarget // previous active tab
    })
</script>-->

</body>
</html>