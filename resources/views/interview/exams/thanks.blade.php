<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Urvam Technologies | Candidate Tracking System</title>
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" id="bootstrap-css">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/responsive.css') }}" rel="stylesheet">

    <!------ Include the above in your HEAD tag ---------->
</head>
<body>
<!-- Tabs -->
<section id="tabs">
    <div class="container register">
        <div class="row register-form">
            <div class="col-lg-12 col-sm-12 col-xs-12 register-right">
                <div class="row register-form">
                    <div class="col-lg-12">
                        <div class="row">
                            <div class="center col-lg-12 form-group">
                                <img src="{{ url('images/logo.png') }}" class="img-fluid"/>
                            </div>
                            <div class="col-lg-12 center bottom-text">
                                <label for="Campus Placement Programme-2019-2020" class="theme-color-text">Campus
                                    Placement Programme-2019-2020</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 center">
                        <h1 class="section-title h1 text-color">Thank You</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/jquery.min.js') }}"></script>
<!-- ./Tabs -->
<!--<script>
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        e.target // newly activated tab
        e.relatedTarget // previous active tab
    })
</script>-->
</body>
</html>