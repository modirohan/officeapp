@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row mt-5">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header d-inline">Skills
                        <span class="float-right">
                        <a href="{{ route('talent.skills.create') }}" class="btn btn-success btn-sm"
                           title="Add New Skill">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>
                        </span>
                    </div>

                    <div class="card-body">
                        @if(count($skills) > 0)
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>Position</th>
                                        <th>Title</th>
                                        <th>Is Active</th>
                                        <th>Added By</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($skills as $item)
                                        <tr>
                                            <td>{{ $item->positionInfo->title}}</td>
                                            <td>{{ $item->title }}</td>
                                            <td>{{ $item->is_active }}</td>
                                            <td>{{ $item->added_by }}</td>
                                            <td>
                                                <a href="{{ route('talent.skills.view', $item->id) }}"
                                                   title="View Skill">
                                                    <button class="btn btn-info btn-sm"><i class="fa fa-eye"
                                                                                           aria-hidden="true"></i> View
                                                    </button>
                                                </a>
                                                <a href="{{ route('talent.skills.edit', $item->id) }}"
                                                   title="Edit Skill">
                                                    <button class="btn btn-primary btn-sm"><i
                                                                class="fa fa-pencil-square-o"
                                                                aria-hidden="true"></i> Edit
                                                    </button>
                                                </a>

                                                <form method="POST"
                                                      action="{{ route('talent.skills.delete', $item->id) }}"
                                                      accept-charset="UTF-8" style="display:inline">
                                                    {{ csrf_field() }}
                                                    <button type="submit" class="btn btn-danger btn-sm"
                                                            title="Delete Skill"
                                                            onclick="return confirm(&quot;Confirm delete?&quot;)"><i
                                                                class="fa fa-trash-o" aria-hidden="true"></i> Delete
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                <div class="pagination-wrapper"> {!! $skills->appends(['search' => Request::get('search')])->render() !!} </div>
                            </div>
                        @else
                            <h1 class="text-center">Data Not Available</h1>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
