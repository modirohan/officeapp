@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row mt-5">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header d-inline">Skill
                        <span class="float-right">
                            <a href="{{ route('talent.skills.index') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                            <a href="{{ route('talent.skills.edit', $item->id) }}" title="Edit Skill"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                            <form method="POST" action="{{ route('talent.skills.delete', $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                {{ csrf_field() }}
                                <button type="submit" class="btn btn-danger btn-sm" title="Delete Skill" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                            </form>
                        </span>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr><th> Position  </th><td> {{ $skill->positionInfo->title }} </td></tr>
                                    <tr><th> Title </th><td> {{ $skill->title }} </td></tr>
                                    <tr><th> Is Active </th><td> {{ $skill->is_active }} </td></tr>
                                    <tr><th> Added By </th><td> {{ $skill->added_by }} </td></tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
