<div class="row">
    <div class="form-group col-sm-6{{ $errors->has('position_id') ? 'has-error' : ''}}">
        <label for="position" class="control-label">{{ 'Position' }}</label>
        {!! Form::select('position_id',$positions,null,['class'=>'form-control']) !!}
        {!! $errors->first('position_id', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="form-group col-sm-6{{ $errors->has('title') ? 'has-error' : ''}}">

        {!! Form::label('title','Title',['class'=>'control-label']) !!}
        <input class="form-control" name="title" type="text" id="title"
               value="{{ isset($skill->title) ? $skill->title : ''}}">
        {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="form-group col-sm-6">
        <div class="form-group">
            <div class="pretty p-icon p-round p-smooth">
                <input type="checkbox" id="is_active" name="is_active" {{ (isset($skill->is_active) && $skill->is_active == 1) ?  'checked' : '' }}/>
                <div class="state p-success">
                    <i class="icon mdi mdi-check"></i>
                    <label for="is_active">Is active</label>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
