<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Urvam Technologies | Candidate Tracking System</title>

    <link rel="stylesheet" href="{{ asset('theme/assets/css/dashboard.css') }}">
    <script src="{{ asset('theme/assets/plugins/maps-google/plugin.js') }}"></script>
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/responsive.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/font-awesome.min.css') }}">
    <link href="//cdnjs.cloudflare.com/ajax/libs/gijgo/1.9.11/combined/css/gijgo.css" rel="stylesheet" type="text/css"/>
    <!------ Include the above in your HEAD tag ---------->
</head>
<body>
<div class="container register">
    <div class="divider-50"></div>
    <div class="divider-10"></div>
    <div class="row">
        <div class="col-lg-3  col-sm-12 col-xs-12 register-left">
            <img src="https://image.ibb.co/n7oTvU/logo_white.png" alt=""/>
            <h3>Welcome</h3>
            <p></p>
        </div>
        <div class="col-lg-9 col-sm-12 col-xs-12 register-right">
            <form method="POST" action="{{ route('store_student') }}" accept-charset="UTF-8" class="form-horizontal"
                  enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="row register-form">
                    <div class="col-lg-12">
                        <div class="row">
                            <div class="center col-lg-12 form-group">
                                <img src="{{ url('images/logo.png') }}" class="img-fluid"/>
                            </div>
                            <div class="col-lg-12 center">
                                <label for="Campus Placement Programme-2019-2020" class="theme-color-text">Campus
                                    Placement
                                    Programme-2019-2020</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 center">
                        <p class="title-text">Student Registration</p>
                    </div>

                    <div class="col-lg-6 col-xs-12">
                        <div class="form-group">
                            <input type="text" name="name" class="form-control" placeholder="Student Name *" value=""
                                   required>
                        </div>
                        <div class="form-group">
                            <input type="text" name="enrollment_number" class="form-control"
                                   placeholder="Enrollment No. *" value="" required>
                        </div>
                        <div class="form-group">
                            <input id="datepicker" name="birth_date" class="form-control border p-2"
                                   placeholder="Date Of Birth" required>
                        </div>
                        <div class="form-group">
                            <input type="text" minlength="10" maxlength="10" name="phone_number" class="form-control"
                                   required placeholder="Your Phone *" value=""/>
                        </div>
                    </div>
                    <div class="col-lg-6 col-xs-12">
                        <div class="form-group">
                            <input type="email" name="email_address" class="form-control" placeholder="Your Email *"
                                   value="" required>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-12 maxl">
                                <label class="radio inline">
                                    <input type="radio" name="gender" value="male" checked>
                                    <span> Male </span>
                                </label>
                                <label class="radio inline">
                                    <input type="radio" name="gender" value="female">
                                    <span>Female </span>
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    {!! Form::select('college', \App\Models\Candidates::COLLEGE_LIST , '',['class'=>'form-control', 'placeholder' => 'Select College']) !!}
                                </div>
                                <div class="col-md-6">
                                    {!! Form::select('course', \App\Models\Candidates::COURSE_LIST , '',['class'=>'form-control', 'placeholder' => 'Select Course']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    {!! Form::select('interested_in', \App\Models\Candidates::INTERESTED_LIST , '',['class'=>'form-control', 'placeholder' => 'Select Area Of Interest']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <input type="text" name="address_line1" class="form-control"
                               placeholder="Enter Address Line 1 *" value="" required>
                    </div>
                    <div class="form-group col-md-12">
                        <input type="text" name="address_line2" class="form-control"
                               placeholder="Enter Address Line 2 (Optional)" value="">
                    </div>
                    <div class="center-button col-lg-12">
                        <input type="submit" class="btnRegister" value="Register"/>
                    </div>
                </div>
            </form>

        </div>
    </div>
</div>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/gijgo.min.js') }}" type="text/javascript"></script>
<script>
    $('#datepicker').datepicker();
</script>
</body>
</html>