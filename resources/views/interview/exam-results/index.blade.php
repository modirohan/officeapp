@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row mt-5">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header d-inline">
                        <div class="row align-items-center">
                            <span class="col-6 text-left">Examination Results</span>
                            <span class="col-6 text-right">
                        <a href="{{ route('talent.interviews.create') }}" class="btn btn-success btn-sm"
                           title="Add New Interview">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>
                        </span>
                        </div>
                    </div>
                    <div class="card-body">

                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Student Name</th>
                                    <th>Enrollment Number</th>
                                    <th>Answers</th>
                                    <th>Percentage</th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse($exam_results as $item)
                                    <tr>
                                        <td>{{ $item->candidateInfo->name }}</td>
                                        <td>{{ $item->candidateInfo->enrollment_number }}</td>
                                        <td>{{ $item->correct_answers ."/". $item->total_questions }}</td>
                                        <td>{{ (round(($item->correct_answers/$item->total_questions)*100)) ."%" }}</td>
                                    </tr>
                                @empty
                                    No record found
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
