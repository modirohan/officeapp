<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">

    <title>Signin Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="//stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <!-- Custom styles for this template -->
    <link href="{{ asset('css/login.css') }}" rel="stylesheet">
</head>

<body>
<form method="post" class="form-signin form-validate" action="{{ route('storeUsersDetails') }}">
    {{ csrf_field() }}
    <h2 class="text-center mb-5">
        <div class="row">
            <div class="text-center">{{ env('APP_NAME') }} User Form</div>
        </div>
    </h2>
    @include('layouts.partials.flash_message')
    <div class="form-group">
        {{ Form::label('name', 'Name') }}
        {{ Form::text('name', isset($check_valid_token) ? $check_valid_token->name : "" , ['class' => 'form-control','readonly'=>'readonly']) }}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="form-group">
        {{ Form::label('email', 'Email Address' , ['class' => 'control-label']) }}
        {{ Form::text('email', isset($check_valid_token) ? $check_valid_token->email : "" , ['class' => 'form-control','readonly'=>'readonly']) }}
        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="form-group">
        <label for="password">Password</label>
        <input type="password" class="form-control px-0" id="password"
               name="password">
        {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
    </div>
    <input type="submit" class="btn btn-primary resetPassword" value="Send"/>
</form>
</body>
</html>

