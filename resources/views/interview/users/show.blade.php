@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row mt-5">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header d-inline">User Detail
                        <span class="float-right">
                            <a href="{{ route('talent.users.index') }}" title="Back"><button class="btn btn-warning btn-sm"><i
                                            class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                            <a href="{{ route('talent.users.edit' ,$user->id) }}" title="Edit User">
                                <button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o"
                                                                      aria-hidden="true"></i> Edit</button></a>
                            <form method="POST" action="{{ route('talent.users.destroy',$user->id) }}" accept-charset="UTF-8"
                                  style="display:inline">
                                {{ csrf_field() }}
                                <button type="submit" class="btn btn-danger btn-sm" title="Delete User"
                                        onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                            </form>
                        </span>

                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                <tr>
                                    <th> Name</th>
                                    <td> {{ $user->name }} </td>
                                </tr>
                                <tr>
                                    <th> Email</th>
                                    <td> {{ $user->email }} </td>
                                </tr>
                                <tr>
                                    <th> Role</th>
                                    <td> {{ $user->roleInfo->title }} </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
