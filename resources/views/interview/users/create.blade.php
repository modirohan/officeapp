@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row mt-5">
            <div class="col-sm-12 col-md-12 my-2">
                <div class="card">
                    <div class="card-header d-inline">Create new User
                        <span class="float-right">
                            <a href="{{ route('talent.users.index') }}" title="Back">
                                <button class="btn btn-warning btn-sm">
                                    <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                                </button>
                            </a>
                        </span>
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('talent.users.store') }}" accept-charset="UTF-8"
                              class="form-horizontal" enctype="multipart/form-data" id="users_form">
                            {{ csrf_field() }}
                            @include ('interview.users.form', ['formMode' => 'create'])
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection