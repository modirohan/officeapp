<div class="row">
    <div class="form-group col-sm-6">
        {!! Form::label('position_id', 'Position') !!}
        {!! Form::select('position_id',$positionsList,null,['class'=>'form-control']) !!}
        {!! $errors->first('role_id', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="form-group col-sm-6 {{ $errors->has('name') ? 'has-error' : ''}}">
        {!! Form::label('name', 'Name') !!}
        {!! Form::text("name", isset($user->name) ? $user->name : old('email') ,['class'=> 'form-control']) !!}

        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="form-group col-sm-6 {{ $errors->has('email') ? 'has-error' : ''}}">
        {!! Form::label('email', 'Email') !!}
        {!! Form::text("email", isset($user->email) ? $user->email : old('email') ,['class'=> 'form-control']) !!}
        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
    </div>
    {{--@if($formMode != 'edit')
        <div class="form-group col-sm-6 {{ $errors->has('password') ? 'has-error' : ''}}">
            {!! Form::label('password', 'Password') !!}
            {!! Form::password("password",['class'=> 'form-control display-password']) !!}
            <i class="showPassword fa fa-eye-slash"></i>
            {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
        </div>
    @endif--}}
    <div class="form-group col-sm-6 {{ $errors->has('role_id') ? 'has-error' : ''}}">
        {!! Form::label('role_id', 'Roles') !!}
        {!! Form::select('role_id',$rolesList,null,['class'=>'form-control']) !!}
        {!! $errors->first('role_id', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="form-group col-sm-12">
        <input class="btn btn-primary send_mail_to_user" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
    </div>
</div>
@section('footer_scripts')
    <script src="{{ asset('modules/users/module.js') }}"></script>
@endsection
