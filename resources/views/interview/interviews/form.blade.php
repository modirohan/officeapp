
<div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
    <div class="div ">
        <p class="text-center"><i class="fa fa-cloud-upload" aria-hidden="true"></i></p>
        <p class="page">Upload Interview CV/Resume Here</p>
    </div>
    <div class="mt-5"><i class="fa fa-user" aria-hidden="true"></i><b> Personal Information</b></div>
    {!! $errors->first('title', '
    <p class="help-block">:message</p>
    ') !!}
</div>
<div class=" row">
    <div class="col-sm-6">
        <div class="required-field r-partial pb-5">
            <label class="label">First Name</label>
            <div class="control firstcontrol is-clearfix" aria-required="true" aria-invalid="true">
                <input type="text" autocomplete="on" maxlength="40" dir="auto" id="sTest-candidateFirstName" name="firstname" placeholder="jinkal" class="form-control input w-100 pl-2">   
            </div>
        </div>
        <div class="required-field r-partial pb-5">
            <label class="label">Email</label>
            <div class="control firstcontrol is-clearfix" aria-required="true" aria-invalid="true">
                <input type="text" autocomplete="on" maxlength="40" dir="auto" id="sTest-candidateFirstName" name="firstname" placeholder="jinkal@email.com" class="form-control input w-100 pl-2"> <!----> <!----> <small class="help counter is-invisible">
                </small>
            </div>
        </div>
        <div class="required-field r-partial pb-5">
            <label class="label">City</label>
            <div class="control firstcontrol is-clearfix" aria-required="true" aria-invalid="true">
                <input type="text" autocomplete="on" maxlength="40" dir="auto" id="sTest-candidateFirstName" name="firstname" placeholder="Ahmdabad" class="form-control input w-100 pl-2"> <!----> <!----> <small class="help counter is-invisible">
                </small>
            </div>
        </div>
        <div class="required-field r-partial pb-5">
            <label class="label">Date of Birth</label>
            <div>
                <form action="/action_page.php"><input type="date" name="bday"></form>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="required-field r-partial pb-5">
            <label class="label">Last Name</label>
            <div class="control firstcontrol is-clearfix" aria-required="true" aria-invalid="true">
                <input type="text" autocomplete="on" maxlength="40" dir="auto" id="sTest-candidateFirstName" name="Last Name" placeholder="Dadhania" class="form-control input w-100 pl-2"> <!----> <!----> <small class="help counter is-invisible">
                </small>
            </div>
        </div>
        <div class="required-field r-partial pb-5">
            <label class="label">Mobile</label>
            <div class="control firstcontrol is-clearfix" aria-required="true" aria-invalid="true">
                <input type="text" autocomplete="on" maxlength="40" dir="auto" id="sTest-candidateFirstName" name="firstname" placeholder="2568493527" class="form-control input w-100 pl-2"> <!----> <!----> <small class="help counter is-invisible">
                </small>
            </div>
        </div>
        <div class="required-field r-partial pb-5">
            <label class="label">Location</label>
            <div class="control firstcontrol is-clearfix" aria-required="true" aria-invalid="true">
                <input type="text" autocomplete="on" maxlength="40" dir="auto" id="sTest-candidateFirstName" name="firstname" placeholder="Gota" class="form-control input w-100 pl-2"> <!----> <!----> <small class="help counter is-invisible">
                </small>
            </div>
        </div>
        <div class="required-field r-partial pb-5">
            <label class="label">Gender</label>
            <div class="row ml-0">
                <input type="radio" class="form-control mt-2 mr-1" name="gender" value="male" checked> Male<br>
                <input type="radio" class="form-control mt-2 mr-1 ml-1" name="gender" value="female"> Female<br>
                <input type="radio" class="form-control mt-2 mr-1 ml-1" name="gender" value="other"> Other
                <div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="mt-5"><i class="fa fa-university"></i><b> Academic Information</b></div>
<div class="row mt-3">
    <div class="col-sm-6">
        <div>
            <label class="label">Educational Qualification</label>
            <div class="autocomplete control is-icon-right">
                <div class="control has-icons-left is-clearfix">
                    <input type="text" autocomplete="off" placeholder="Bachelor of Engineering"
                           id="sTest-candidateQualification" class="form-control  input w-100 pl-2" > <!---->
                </div>
            </div>
        </div>
        <!---->
    </div>
    <div class="col-sm-6">
        <div>
            <label class="label">Educational Specialization</label>
            <div class="control is-clearfix">
                <input type="text" autocomplete="on" maxlength="20" id="sTest-candidateSpecialization" placeholder="Computer Science" class="form-control input w-75 pl-2"> <!----> <!----> <small class="help counter is-invisible">
                    0 / 20
                </small>
            </div>
        </div>
    </div>
</div>
<div class="mt-5"><i class="fa fa-users" aria-hidden="true"></i><b> Employment Information</b></div>
<div class="row mt-3">
    <div class="col-sm-6">
        <div>
            <label class="label">Current / Last Organisation</label>
            <div class="autocomplete control is-icon-right">
                <div class="control has-icons-left is-clearfix">
                    <input type="text" autocomplete="off" placeholder="BMW"
                           id="sTest-candidateQualification" class="form-control input w-100 pl-2" > <!---->
                </div>
            </div>
        </div>
        <div class="mt-4">
            <label class="label">Total Experience (Years)
            </label>
            <div class="autocomplete control is-icon-right">
                <div class="control has-icons-left is-clearfix">
                    <input type="text" autocomplete="off" placeholder="0 Years"
                           id="sTest-candidateQualification" class="form-control input w-100 pl-2" > <!---->
                </div>
            </div>
        </div>
        <div class="mt-4">
            <label class="label">Salary Type</label>
            <div class="autocomplete control is-icon-right">
                <div class="control has-icons-left is-clearfix">
                    <input type="text" autocomplete="off" placeholder="Annual Salary"
                           id="sTest-candidateQualification" class="form-control input w-100 pl-2" > <!---->
                </div>
            </div>
        </div>
        <div class="mt-4">
            <label class="label">Current Salary</label>
            <div class="autocomplete control is-icon-right">
                <div class="control has-icons-left is-clearfix">
                    <input type="text" autocomplete="off" placeholder="50000"
                           id="sTest-candidateQualification" class="form-control input w-100 pl-2" > <!---->
                </div>
            </div>
        </div>
        <div class="mt-4">
            <label class="label">Current Employment Status</label>
            <div class="autocomplete control is-icon-right">
                <div class="control has-icons-left is-clearfix">
                    <input type="text" autocomplete="off" placeholder="Unemployeed"
                           id="sTest-candidateQualification" class="form-control input w-100 pl-2" > <!---->
                </div>
            </div>
        </div>
        <div class="mt-4">
            <label class="label">Available from</label>
            <div class="autocomplete control is-icon-right">
                <div class="control has-icons-left is-clearfix">
                    <input id="datepicker" width="276" class="form-control"/>
                    <script>
                        $('#datepicker').datepicker({
                            uiLibrary: 'bootstrap4'
                        });
                    </script> <!---->
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div>
            <label class="label"></label>
            <div class="control is-clearfix">
                <input type="text" autocomplete="on" maxlength="20" id="sTest-candidateSpecialization" placeholder="Designer Developer" class="form-control  input w-100 pl-2"> <!----> <!----> <small class="help counter is-invisible">
                </small>
            </div>
        </div>
        <div class="mt-4">
            <label class="label">Relevant Experience (Years)</label>
            <div class="autocomplete control is-icon-right">
                <div class="control has-icons-left is-clearfix">
                    <input type="text" autocomplete="off" placeholder="0 Years"
                           id="sTest-candidateQualification" class="form-control input w-100 pl-2" > <!---->
                </div>
            </div>
        </div>
        <div class="mt-4">
            <label class="label">Currency Type</label>
            <div class="autocomplete control is-icon-right">
                <div class="control has-icons-left is-clearfix">
                    <input type="text" autocomplete="off" placeholder="Dollars ($ - America)"
                           id="sTest-candidateQualification" class="form-control input w-100 pl-2" > <!---->
                </div>
            </div>
        </div>
        <div class="mt-4">
            <label class="label">Salary Expectation</label>
            <div class="autocomplete control is-icon-right">
                <div class="control has-icons-left is-clearfix">
                    <input type="text" autocomplete="off" placeholder="60000"
                           id="sTest-candidateQualification" class="form-control input w-100 pl-2" > <!---->
                </div>
            </div>
        </div>
        <div class="mt-4">
            <label class="label">Notice Period (days)</label>
            <div class="autocomplete control is-icon-right">
                <div class="control has-icons-left is-clearfix">
                    <input type="text" autocomplete="off" placeholder="60"
                           id="sTest-candidateQualification" class="form-control input w-100 pl-2" > <!---->
                </div>
            </div>
        </div>
    </div>
</div>
<div class="mt-5">
    <i class="fa fa-bookmark"></i><b> Resume, Language and Technical Skills</b>
    <div class="mt-4">
        <label class="label">Skills</label>
        <div class="autocomplete control is-icon-right">
            <div class="control has-icons-left is-clearfix">
                <input type="text" autocomplete="off" placeholder="Skills"
                       id="sTest-candidateQualification" class="form-control input w-100 pl-2" > <!---->
            </div>
        </div>
    </div>
</div>
<div class="row mt-3">
    <div class="col-sm-6">
        <div>
            <label class="label">Language</label>
            <div class="autocomplete control is-icon-right">
                <div class="control has-icons-left is-clearfix">
                    <input type="text" autocomplete="off" placeholder="English"
                           id="sTest-candidateQualification" class="form-control input w-100 pl-2" > <!---->
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div>
            <label class="label">Proficiency Lavel</label>
            <div class="control is-clearfix">
                <input type="text" autocomplete="on" maxlength="20" id="sTest-candidateSpecialization" placeholder="Native/Bilingual Proficiency" class="form-control input w-100 pl-2"> <!----> <!----> <small class="help counter is-invisible">
                </small>
            </div>
        </div>
    </div>
</div>
<div class="mt-5"><i class="fa fa-firefox"></i><b> RSocial Profile Links</b>
</div>
<div class="row mt-3">
    <div class="col-sm-6">
        <div>
            <label class="label">Facebook Profile URL</label>
            <div class="autocomplete control is-icon-right">
                <div class="control has-icons-left is-clearfix">
                    <input type="text" autocomplete="off" placeholder="https://www.facebook.com/ryancooper"
                           id="sTest-candidateQualification" class="form-control input w-100 pl-2" > <!---->
                </div>
            </div>
        </div>
        <div>
            <label class="label  mt-4">Linkedin Profile URL</label>
            <div class="autocomplete control is-icon-right">
                <div class="control has-icons-left is-clearfix">
                    <input type="text" autocomplete="off" placeholder="https://www.linkedin.com/ryancooper"
                           id="sTest-candidateQualification" class="form-control input w-100 pl-2" > <!---->
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div>
            <label class="label">Twitter Profile URL</label>
            <div class="control is-clearfix">
                <input type="text" autocomplete="on" maxlength="20" id="sTest-candidateSpecialization" placeholder="https://www.twitter.com/ryancooper" class="form-control input w-100 pl-2"> <!----> <!----> <small class="help counter is-invisible">
                </small>
            </div>
        </div>
        <div>
            <label class="label  mt-4">GitHub Profile URL</label>
            <div class="control is-clearfix">
                <input type="text" autocomplete="on" maxlength="20" id="sTest-candidateSpecialization" placeholder="https://www.github.com/ryancooper" class="form-control input w-100 pl-2"> <!----> <!----> <small class="help counter is-invisible">
                </small>
            </div>
        </div>
    </div>
</div>
<div class="mt-4">
    <label class="label">Source </label>
    <div class="autocomplete control is-icon-right">
        <div class="control has-icons-left is-clearfix">
            <input type="text" autocomplete="off" placeholder="Source"
                   id="sTest-candidateQualification" class="form-control input w-100 pl-2" > <!---->
        </div>
    </div>
</div>
<div class="form-group text-center mt-5">
    <input class="btn green-btn w-25" type="submit" value="{{ $formMode === 'edit' ? 'Update' : '+ Add field' }}">
</div>