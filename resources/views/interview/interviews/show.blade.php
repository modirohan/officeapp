@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header d-inline">interview #{{ $interview->id }}
                    <span class="float-right">
                            <a href="{{ route('talent.interviews.index') }}" title="Back"><button class="btn btn-warning btn-sm"><i
                                            class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                            <a href="{{ route('talent.interviews.edit' ,$interview->id) }}" title="Edit User">
                                <button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o"
                                                                          aria-hidden="true"></i> Edit</button></a>
                            <form method="POST" action="{{ route('talent.interviews.delete',$interview->id) }}" accept-charset="UTF-8"
                                  style="display:inline">
                                {{ method_field('DELETE') }}
                                {{ csrf_field() }}
                                <button type="submit" class="btn btn-danger btn-sm" title="Delete User"
                                        onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                            </form>
                        </span>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $interview->id }}</td>
                                    </tr>
                                    <tr>
                                        <th> Title </th>
                                        <td> {{ $interview->title }} </td>
                                    </tr>
                                    <tr>
                                        <th> Description </th>
                                        <td> {{ $interview->description }} </td>
                                    </tr>
                                    <tr>
                                        <th> Openings Count </th>
                                        <td> {{ $interview->openings_count }} </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
