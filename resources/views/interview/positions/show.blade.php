@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row mt-5">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header d-inline">Position Details
                        <span class="float-right">
                            <a href="{{ route('talent.positions.index') }}" title="Back"><button
                                        class="btn btn-warning btn-sm"><i
                                            class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                            <a href="{{ route('talent.positions.edit', $position->id) }}" title="Edit Position">
                                <button class="btn btn-primary btn-sm">
                                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit
                                </button>
                            </a>
                            <form method="POST" action="{{ route('talent.positions.delete', $position->id) }}"
                                  accept-charset="UTF-8" style="display:inline">
                                {{ csrf_field() }}
                                <button type="submit" class="btn btn-danger btn-sm"
                                        title="Delete Position"
                                        onclick="return confirm(&quot;Confirm delete?&quot;)"><i
                                            class="fa fa-trash-o" aria-hidden="true"></i> Delete
                                </button>
                            </form>
                        </span>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                <tr>
                                    <th> Title</th>
                                    <td> {{ $position->title }} </td>
                                </tr>
                                <tr>
                                    <th> Description</th>
                                    <td> {{ $position->description }} </td>
                                </tr>
                                <tr>
                                    <th> Openings Count</th>
                                    <td> {{ $position->openings_count }} </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
