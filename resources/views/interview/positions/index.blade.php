@extends('layouts.app')

@section('content')
    <div class="container mt-5">
        @include('layouts.partials.flash_message')
    </div>
    <div class="container">
        <div class="row mt-5">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header d-inline">Positions
                        <span class="float-right">
                        <a href="{{ route('talent.positions.create') }}" class="btn btn-success btn-sm"
                           title="Add New Position">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>
                        </span>
                    </div>
                    <div class="card-body">
                        @if(count($positions) > 0)
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>Title</th>
                                        <th>Description</th>
                                        <th>Openings Count</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($positions as $item)
                                        <tr>
                                            <td>{{ $item->title }}</td>
                                            <td>{{ $item->description }}</td>
                                            <td>{{ $item->openings_count }}</td>
                                            <td>
                                                <a href="{{ route('talent.positions.view', $item->id) }}"
                                                   title="View Position">
                                                    <button class="btn btn-info btn-sm"><i class="fa fa-eye"
                                                                                           aria-hidden="true"></i> View
                                                    </button>
                                                </a>
                                                <a href="{{ route('talent.positions.edit', $item->id) }}"
                                                   title="Edit Position">
                                                    <button class="btn btn-primary btn-sm"><i
                                                                class="fa fa-pencil-square-o"
                                                                aria-hidden="true"></i> Edit
                                                    </button>
                                                </a>
                                                <form method="POST"
                                                      action="{{ route('talent.positions.delete', $item->id) }}"
                                                      accept-charset="UTF-8" style="display:inline">
                                                    {{ csrf_field() }}
                                                    <button type="submit" class="btn btn-danger btn-sm"
                                                            title="Delete Position"
                                                            onclick="return confirm(&quot;Confirm delete?&quot;)"><i
                                                                class="fa fa-trash-o" aria-hidden="true"></i> Delete
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                <div class="pagination-wrapper"> {!! $positions->render() !!} </div>
                            </div>
                        @else
                            <h1 class="text-center">Data Not Available</h1>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
