<div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
    <label for="title" class="control-label">{{ 'Title' }}</label>
    <input class="form-control" name="title" type="text" id="title" value="{{ isset($position->title) ? $position->title : ''}}" >
    {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
    <label for="description" class="control-label">{{ 'Description' }}</label>
    <textarea class="form-control wysiwyg_editor" rows="5" name="description" type="textarea" id="description" >{{ isset($position->description) ? $position->description : ''}}</textarea>
    {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('openings_count') ? 'has-error' : ''}}">
    <label for="openings_count" class="control-label">{{ 'Openings Count' }}</label>
    <input class="form-control" name="openings_count" type="text" id="openings_count" value="{{ isset($position->openings_count) ? $position->openings_count : ''}}" >
    {!! $errors->first('openings_count', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>