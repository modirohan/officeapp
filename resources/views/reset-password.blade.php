<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">

    <title>Urvam-Reset Password</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="//stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <!-- Custom styles for this template -->
    <link href="{{ asset('css/login.css') }}" rel="stylesheet">
</head>

<body>

<div class="contianer">
    <div class="signin-box text-center">
        <h1 class="form-title">Urvam</h1>
        <p class="form-description">Please enter new password</p>
        @include('layouts.partials.flash_message')
        <form class="form form-signin" action="{{ route('reset-user-password') }}" method="post">
            {{ csrf_field() }}
            <div class="field">
                <input type="text" name="email" id="email" placeholder="Email address" readonly="readonly" value="{{ $result['email'] or old('email') }}">
                <label for="email">Email address</label>
            </div>
            <div class="field">
                <input type="password" name="password" id="password" placeholder="Password">
                <label for="password">Password</label>
                {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
            </div>
            <div class="field mb-5">
                <input type="password" name="password_confirmation" id="password_confirmation" placeholder="Confirm Password">
                <label for="password_confirmation">Confirm Password</label>
                {!! $errors->first('password_confirmation', '<p class="help-block">:message</p>') !!}
            </div>
            <button class="btn btn-lg btn-signin btn-block" type="submit">Reset Password</button>
        </form>
    </div>
</div>
</body>
</html>
