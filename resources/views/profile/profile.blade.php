@extends('layouts.app')

@section('content')
    <div class="container" id="profile">
        <div class="row my-5">
            <div class="col-md-3 d-inline-block">
                <div class="row">
                    <div class="col-sm-12 px-0 text-center">
                        @if(isset($user->image))
                            <img src="{{ showUserProfileImage($user->image) }}" alt="User's Profile Photo">
                        @else
                            <img src="{{asset('images/l60Hf.png')}}" alt="User's Profile Photo">
                        @endif
                        <h3 class="mt-3 text-center text-secondary">{{$user->name}}</h3>
                        <h6 class="mb-3 text-center text-secondary">{{$user->education}}</h6>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 px-0">
                        <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                            <a class="btn btn-primary mb-2" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">Profile</a>
                            <a class="btn btn-primary mb-2" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false">Salary History</a>
                            <a class="btn btn-primary mb-2" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false">Files</a>
                            <a class="btn btn-primary mb-2" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="false">Leave</a>
                            <a class="btn btn-primary mb-2" id="v-pills-Company-Policy-tab" data-toggle="pill" href="#v-pills-Company-Policy" role="tab" aria-controls="v-pills-Company Policy" aria-selected="false">Company Policy</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-9 d-inline-block">
                <div class="row mb-5">
                    <div class="col-sm-12 text-center">
                        <h1 class="text-info">Employee Profile</h1>
                        <h4 class="text-secondary ">Basic Detail</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="card ml-6">
                        <div class="card-body bg-light">
                        <div class="col-sm-12 text-left">
                            <div class="tab-content" id="v-pills-tabContent">
                                <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                                    <div class="row">
                                        <div class="col-md-6 text-gray-dark">
                                            @if($user->employee_id)<div><i class="fa fa-user text-info m-4"></i> {{$user->employee_id}}</div>@endif
                                            @if($user->phone_no)<div><i class="fa fa-phone text-info m-4"></i> {{$user->phone_no}}</div>@endif
                                            @if($user->current_salary)<div><i class="fas fa-rupee-sign text-info m-4"></i> {{$user->current_salary}}</div>@endif
                                            @if($user->joining_date)<div><i class="fas fa-calendar-alt text-info m-4"></i> {{$user->joining_date}}</div>@endif
                                            @if($user->gender)<div><i class="fas @if($user->gender == 'male') fa-mars @else fa-venus-mars @endif text-info m-4"></i> {{$user->gender}}</div>@endif
                                        </div>
                                        <div class="col-md-6 text-gray-dark">
                                            @if($user->email)<div><i class="fa fa-envelope text-info m-4"></i> {{$user->email}}</div>@endif
                                            @if($user->education)<div><i class="fa fa-book text-info m-4"></i> {{$user->education}}</div>@endif
                                            @if($user->experience)<div><i class="fas fa-users-cog text-info m-4"></i> {{$user->experience}}</div>@endif
                                            @if($user->date_of_birth)<div><i class="fas fa-calendar-alt text-info m-4"></i> {{$user->date_of_birth}}</div>@endif
                                            @if($user->time_shift)<div><i class="fas fa-clock text-info m-4"></i> {{$user->time_shift}}</div>@endif
                                        </div>
                                    </div>
                                    <a href="{{route('user.edit')}}" class="btn btn-info float-right mr-0 ml-auto-auto" role="button">Edit</a>
                                </div>
                                <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                                    <div class="row">
                                        <div class="col-md-4 text-gray-dark">
                                            <h5>Old Salary</h5>
                                            <p>15,000/-</p>
                                        </div>
                                        <div class="col-md-4 text-gray-dark">
                                            <h5>New Salary</h5>
                                            <p>17,000/-</p>
                                        </div>
                                        <div class="col-md-4 text-gray-dark">
                                            <h5>Applicable Date</h5>
                                            <p>28/03/2019</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">
                                    <div class="row">
                                        <div class="col-md-3 text-gray-dark">
                                            <h5>File Name</h5>
                                            <p>Photo ID</p>
                                            <p>Exp.letter</p>
                                        </div>
                                        <div class="col-md-3 text-gray-dark">
                                            <h5>Designation </h5>
                                            <p>jinkal's Photo</p>
                                            <p>Jinkal's Letter</p>
                                        </div>
                                        <div class="col-md-3 text-gray-dark">
                                            <h5>Download / View</h5>
                                            <p><b><a href="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRSrsiq52tRVgSB-3XXuwxBkVMcxB1--MYWLx1QG4bSUazLP6Ie" target="_blank">Download | view</a></b></p>
                                            <p><b><a href="https://i.pinimg.com/originals/d2/ce/9b/d2ce9b4a5948fe7e1c61d692e879d8e7.jpg" target="_blank">Download | view</a></b></p>
                                        </div>
                                        <div class="col-md-3 text-gray-dark">
                                            <h5>Private</h5>
                                            <p class="text-success"><b>Yes</b></p>
                                            <p class="text-danger"><b>No</b></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab">
                                    <div class="row">
                                        <div class="col-md-4 text-gray-dark">
                                            <h5>Leave Period</h5>
                                            <p>10/08/2019 To 12/08/2019</p>
                                        </div>
                                        <div class="col-md-4 text-gray-dark">
                                            <h5>Reason</h5>
                                            <p>marriage</p>
                                        </div>
                                        <div class="col-md-4 text-gray-dark">
                                            <h5>Leave Type</h5>
                                            <p>Paid Leave</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="v-pills-Company-Policy" role="tabpanel" aria-labelledby="v-pills-Company-Policy-tab">
                                    <div class="row">
                                        <div class="col-sm-12 o-auto">
                                        <div class="d-inline-flex">
                                        <div class="col-4 text-gray-dark">
                                            <h5>Title</h5>
                                            <p>Leave policy</p>
                                        </div>
                                        <div class="col-3 text-gray-dark">
                                            <h5>Details</h5>
                                            <p>Updated Leave Policy</p>
                                        </div>
                                        <div class="col-4 text-gray-dark">
                                            <h5>Last Revision</h5>
                                            <p>10/11/2019</p>
                                        </div>
                                        <div class="col-4 text-gray-dark">
                                            <h5>Applies To</h5>
                                            <p>All Member</p>
                                        </div>
                                        <div class="col-4 text-gray-dark">
                                            <h5>Status</h5>
                                            <p>Finalized</p>
                                        </div>
                                        <div class="col-4 text-gray-dark">
                                            <h5>Policy Document</h5>
                                            <p><b><a href="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRSrsiq52tRVgSB-3XXuwxBkVMcxB1--MYWLx1QG4bSUazLP6Ie" target="_blank">Download</a></b></p>
                                        </div>
                                    </div>
                                </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
        <div class="row">


            </div>
        </div>
    </div>
@endsection

@section('header_styles')
    <link rel="stylesheet" href="{{asset('global/css/custom/profile.css')}}">
@endsection

@section('footer_scripts')
    <script>

    </script>
@endsection