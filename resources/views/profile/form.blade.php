@extends('layouts.app')

@section('content')
<div class="container" id="form-profile">
    <h1>Profile</h1>
    <form action="{{ route('user.update') }}" method="POST" enctype="multipart/form-data">
    {{csrf_field()}}
    <div class="form-group row">
        <label for="name" class="col-sm-2 col-form-label">Name :</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" value="{{ ($user->name)?$user->name:old('name') }}" name="name">
        </div>
    </div>
        <div class="form-group row">
            <label for="employee_id" class="col-sm-2 col-form-label">Employee ID :</label>
            <div class="col-sm-4">
                <input type="text" class="form-control" name="employee_id" placeholder="Employee ID" value="{{ ($user->employee_id)?$user->employee_id:old('employee_id') }}" @if($user->employee_id) disabled @endif>
            </div>
            <label for="designation" class="col-sm-2 col-form-label">Designation :</label>
            <div class="col-sm-4">
                <input type="text" class="form-control" name="designation" placeholder="Designation" value="{{ ($user->designation)?$user->designation:old('designation') }}" @if($user->designation) disabled @endif>
            </div>
        </div>
        <div class="form-group row">
            <label for="email" class="col-sm-2 col-form-label">Email :</label>
            <div class="col-sm-4">
                <input type="email" class="form-control" placeholder="Company Email Address" name="email" value="{{ ($user->email)?$user->email:old('email') }}" readonly>
            </div>
            <label for="phone_no" class="col-sm-2 col-form-label">Contact :</label>
            <div class="col-sm-4">
                <input type="text" class="form-control" id="inputcontact" placeholder="Mobile Number" name="phone_no" value="{{ ($user->phone_no)?$user->phone_no:old('phone_no') }}" maxlength="12" minlength="10">
            </div>
        </div>
        <div class="form-group row">
            <label for="inputAddress" class="col-sm-2 col-form-label">Address :</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="inputAddress" placeholder="Location" name="location" value="{{($user->location)?$user->location:old('location')}}">
            </div>
            <div class="col-sm-2">
            </div>
            <div class="col-sm-4 mt-2">
                <input type="text" class="form-control" id="inputCity" placeholder="City" name="city" value="{{($user->city)?$user->city:old('city')}}">
            </div>
            <div class="col-sm-3 mt-2">
                <input type="text" class="form-control" id="inputState" placeholder="State" name="state" value="{{($user->state)?$user->state:old('state')}}">
            </div>
            <div class="col-sm-3 mt-2">
                <input type="text" class="form-control" id="inputPincode" placeholder="Pincode" name="pincode" value="{{($user->pincode)?$user->pincode:old('pincode')}}">
            </div>
        </div>
        <div class="form-group row">
            <label for="permanent_location" class="col-sm-2 col-form-label">Permanent Address :</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="inputLocation" placeholder="Location" name="permanent_location" value="{{($user->permanent_location)?$user->permanent_location:old('permanent_location')}}">
            </div>
            <div class="col-sm-2">
            </div>
            <div class="col-sm-4 mt-2">
                <input type="text" class="form-control" id="inputCity" placeholder="City" name="permanent_city" value="{{($user->permanent_city)?$user->permanent_city:old('permanent_city')}}">
            </div>
            <div class="col-sm-3 mt-2">
                <input type="text" class="form-control" id="inputState" placeholder="State" name="permanent_state" value="{{($user->permanent_state)?$user->permanent_state:old('permanent_state')}}">
            </div>
            <div class="col-sm-3 mt-2">
                <input type="text" class="form-control" id="inputPincode" placeholder="Pincode" name="permanent_pincode" value="{{($user->permanent_pincode)?$user->permanent_pincode:old('permanent_pincode')}}">
            </div>
        </div>
        <div class="form-group row">
            <label for="inputEducation" class="col-sm-2 col-form-label">Education :</label>
            <div class="col-sm-4">
                <input type="text" class="form-control" id="inputAddress" placeholder="Last Degree" value="{{($user->education)?$user->education:old('education')}}" name="education">
            </div>
            <label for="inputcurrent salary" class="col-sm-2 col-form-label">Current Salary :</label>
            <div class="col-sm-4">
                <input type="text" class="form-control" id="inputcontact" placeholder="Current Salary" name="current_salary" value="{{($user->current_salary)?$user->current_salary:old('current_salary')}}" @if($user->current_salary) disabled @endif>
            </div>
        </div>
        <div class="form-group row">
            <label for="inputExperience" class="col-sm-2 col-form-label">Experience :</label>
            <div class="col-sm-4">
                <input type="text" class="form-control" id="inputAddress" placeholder="Years" name="year_of_experience" value="{{ ($user->year_of_experience)?$user->year_of_experience:old('year_of_experience') }}" @if($user->year_of_experience) disabled @endif>
            </div>
            <label for="inputJoining Date" class="col-sm-2 col-form-label">Joining Date :</label>
            <div class="col-sm-4">
                <input type="date" class="form-control" id="inputJoining Date" name="joining_date" value="{{($user->joining_date)?$user->joining_date:old('joining_date') }}" @if($user->joining_date) disabled @endif>
            </div>
        </div>
    <div class="form-group row">
        <label for="inputDate of birth" class="col-sm-2 col-form-label">Date of birth :</label>
        <div class="col-sm-4">
            <input type="date" class="form-control" id="inputDate of birth" name="date_of_birth" value="{{($user->date_of_birth)?$user->date_of_birth:old('date_of_birth') }}">
        </div>
        <label for="inputAddress" class="col-sm-2 col-form-label">Gender :</label>
        <div class="col-sm-4">
            <div class="row mt-2">
                <input type="radio" name="gender" value="male" @if($user->gender == 'male') checked @endif> Male<br>
                <input type="radio" name="gender" value="female" @if($user->gender == 'female') checked @endif> Female<br>
            </div>
        </div>
    </div>
        <div class="form-group row">
            <label for="time_shift" class="col-sm-2 col-form-label">Time Shift :</label>
            <div class="col-sm-10">
                <div class="row mt-2">
                    @foreach(\App\Models\User::TIME_SHIFTS as $key => $time_shift)
                        <input type="radio" name="time_shift" value="{{ $key }}" @if($key == $user->getOriginal('time_shift')) checked @endif> {{ $time_shift }}
                    @endforeach
                </div>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-2">
                <div class="row mt-2">
                    <input class="form-checkbox" name="change_password" type="checkbox" id="change_password">
                    <label for="change_password">Change Password</label>
                </div>
            </div>
            <div class="form-group {{ $errors->has('password') ? 'has-error' : ''}} change_password_field" style="display: none;">
                <label for="password" class="col-md-5 m-2 control-label">{{ 'New Password' }}</label>
                <div class="col-sm-10">
                    <input type="password" id="password" class="form-control"
                           name="password">
                </div>
            </div>
        </div>
        <div class="form-group col-sm-6">
            <input type="file" name="image" class="form-control">
        </div>

        <div class="text-center">
            <input type="submit" class="button" value="Edit Profile">
        </div>
    </form>
</div>
@endsection

@section('header_styles')
    <link rel="stylesheet" href="{{asset('global/css/custom/profile_form.css')}}">
@endsection

@section('footer_scripts')
    <script>
        $(document).on('change', '#change_password', function () {
            var is_checked = $(this).prop('checked');
            if(is_checked){
                $(".change_password_field").show();
            }else{
                $(".change_password_field").hide();
            }
        });
    </script>
@endsection