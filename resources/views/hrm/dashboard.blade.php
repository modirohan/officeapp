@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>HRM Dashboard</h1>
            </div>
            @include('hrm.partials.day_timelogs')
            @include('hrm.partials.month_holidays')
            @include('hrm.partials.month_birthdays')
            @include('hrm.partials.qr_code')
        </div>
    </div>
@endsection