@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row mt-5">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        Create Leave Application
                        <a href="{{route('leaves.index')}}" class="btn btn-primary ml-auto">Back</a>
                    </div>
                    <div class="card-body">
                        <form action="{{route('leaves.store')}}" method="POST">
                            <div class="row">
                                <div class="form-group col-sm-3">
                                    <label for="start_date">Start Date</label>
                                    {{csrf_field()}}
                                    <input name="start_date" type="date" class="form-control" data-validation="required" data-validation-error-msg="Start Date is Required">
                                </div>
                                <div class="form-group col-sm-3">
                                    <input name="is_start_date_half_day" type="radio" class="form-control" value="1">Is Half Day?
                                </div>
                                <div class="form-group col-sm-3">
                                    <input name="start_date_half_day_session" type="radio" class="form-control" value="1">First Half
                                </div>
                                <div class="form-group col-sm-3">
                                    <input name="start_date_half_day_session" type="radio" class="form-control" value="2">Second Half
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-sm-3">
                                    <label for="start_date">End Date</label>
                                    {{csrf_field()}}
                                    <input name="end_date" type="date" class="form-control">
                                </div>
                                <div class="form-group col-sm-3">
                                    <input name="is_end_date_half_day" type="radio" class="form-control" value="1">Is Half Day?
                                </div>
                                <div class="form-group col-sm-3">
                                    <input name="end_date_half_day_session" type="radio" class="form-control" value="1">First Half
                                </div>
                                <div class="form-group col-sm-3">
                                    <input name="end_date_half_day_session" type="radio" class="form-control" value="2">Second Half
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-sm-6">
                                    <label for="reason">Reason</label>
                                    <select name="reason" class="form-control">
                                        @foreach(\App\Models\Hrm\LeaveApplications::REASON as $key => $value)
                                            <option value="{{$key}}">{{$value}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-sm-12">
                                    <label for="detailed_reason">Detailed Reason</label>
                                    <textarea name="detailed_reason" class="form-control"></textarea>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-sm-6">
                                    <input type="radio" name="is_available_on_phone" value="1">Is Available on phone
                                </div>
                                <div class="form-group col-sm-6">
                                    <label for="detailed_reason">Emergency Phone Number</label>
                                    <input type="text" name="emergency_contact_no" class="form-control">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-sm-3">
                                    <input type="submit" value="Create Leave Application" class="btn btn-primary">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection