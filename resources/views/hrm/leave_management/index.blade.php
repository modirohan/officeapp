@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row mt-5">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        Leaves
                        <a href="{{route('leaves.create')}}" class="btn btn-primary ml-auto">Create Leave Application</a>
                    </div>
                    <div class="card-body">
                        @if(!empty($leaves->toArray()))
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Leave Period</th>
                                    <th>Reason</th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($leaves as $leave)
                                    <tr >
                                        <td>
                                            {{ $leave->start_date }}
                                            @if(isset($leave->end_date))
                                                - {{ $leave->end_date }}
                                                @endif
                                        </td>
                                        <td>{{ $leave->reason }}</td>
                                        <td>
                                            <span class="badge @if($leave->status == 'Pending') badge-danger @else badge-success @endif">{{ $leave->status }}</span>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                            @else
                                <h3>No Leaves Application</h3>
                            @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection