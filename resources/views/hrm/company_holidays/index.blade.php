@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row mt-5">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header d-inline">Company Holidays</div>
                    <div class="card-body">
                        <div class="table-responsive">
                            @if(!$company_holidays->isEmpty())
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Company Holidays</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($company_holidays as $item)
                                    <tr @if(!getDateStatus($item->date)) style="background-color:#9e9e9e59;" @endif>
                                        <td>{{ $item->title }}</td>
                                        <td>{{ $item->date }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                                @else
                                <div class="container">
                                    <h6 class="text-center">No Holidays this Month</h6>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection