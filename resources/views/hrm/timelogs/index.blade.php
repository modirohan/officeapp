@extends('layouts.app')

@section('content')
    <div class="container mt-5">
    </div>
    <div class="container">
        <div class="row mt-5">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header d-inline">Log Time Entry</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6 col-sm-12 d-inline-block border-right">
                                <form action="{{route('timelog.store')}}" method="POST">
                                    <div class="row">
                                        <div class="form-group col-sm-12">
                                            <label for="entry_type">Entry Type</label>
                                            {{csrf_field()}}
                                            <select name="entry_type" class="form-control">
                                                @foreach($data['entry_type'] as $key => $value)
                                                    <option value="{{$key}}">{{$value}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-sm-12">
                                            <label for="entry_mode">Entry Mode</label>
                                            <select name="entry_mode" class="form-control">
                                                @foreach($data['entry_mode'] as $key => $value)
                                                    <option value="{{$key}}">{{$value}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <input type="submit" name="submit" value="Save Timelog"
                                                   class="btn green-btn">
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-6 col-sm-12 d-inline-block">
                                @if(count($timelogs) > 0)
                                    <div class="table-responsive">
                                        <table class="table hours-table">
                                            <thead>
                                            <tr>
                                                <th>Entry Type</th>
                                                <th>Entry Mode</th>
                                                <th>Timing</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($timelogs as $item)
                                                <tr>
                                                    <td>{{ $item->entry_type }}</td>
                                                    <td>{{ $item->entry_mode }}</td>
                                                    <td>{{ \Carbon\Carbon::parse($item->entry_time)->format("h:i A") }}</td>
                                                </tr>
                                            @endforeach
                                            <tr class="twh-tr">
                                                <td colspan="2">
                                                    <label class="text-white float-right">Total Working Hours: </label>
                                                </td>
                                                <td class="text-white">
                                                    {{ $timeLogDetails['Total Working Hours'] }}
                                                </td>
                                            </tr>
                                            <tr class="tbh-tr">
                                                <td colspan="2">
                                                    <label class="text-white float-right">Total Break Hours: </label>
                                                </td>
                                                <td class="text-white">
                                                    {{ $timeLogDetails['Total Break Hours'] }}
                                                </td>
                                            </tr>
                                            <tr class="tlh-tr">
                                                <td colspan="2">
                                                    <label class="text-white float-right">Total Lunch Hours: </label>
                                                </td>
                                                <td class="text-white">
                                                    {{ $timeLogDetails['Total Lunch Hours'] }}
                                                </td>
                                            </tr>
                                            <tr class="awh-tr">
                                                <td colspan="2">
                                                    <label class="text-white float-right">Actual Working Hours: </label>
                                                </td>
                                                <td class="text-white">
                                                    {{ $timeLogDetails['Actual Working Hours'] }}
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                @else
                                    <h1 class="text-center">Data Not Available</h1>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('header_styles')
    <link rel="stylesheet" href="{{ asset('css/hrm.css') }}">
@endsection
