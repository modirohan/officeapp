<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            Your Today's Timelog
            <a href="{{route('timelog.index')}}" class="btn btn-primary ml-auto">Go to Timelog</a>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table">
                    <tbody>
                    <tr class="bg-info">
                        <td colspan="2">
                            <label class="text-white float-right">Total Working Hours: </label>
                        </td>
                        <td class="text-white">
                            {{ $timeLogDetails['Total Working Hours'] }}
                        </td>
                    </tr>
                    <tr class="bg-info">
                        <td colspan="2">
                            <label class="text-white float-right">Total Break Hours: </label>
                        </td>
                        <td class="text-white">
                            {{ $timeLogDetails['Total Break Hours'] }}
                        </td>
                    </tr>
                    <tr class="bg-info">
                        <td colspan="2">
                            <label class="text-white float-right">Total Lunch Hours: </label>
                        </td>
                        <td class="text-white">
                            {{ $timeLogDetails['Total Lunch Hours'] }}
                        </td>
                    </tr>
                    <tr class="bg-success">
                        <td colspan="2">
                            <label class="text-white float-right">Actual Working Hours: </label>
                        </td>
                        <td class="text-white">
                            {{ $timeLogDetails['Actual Working Hours'] }}
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>