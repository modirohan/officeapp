<div class="col-md-6">
    <div class="card">
        <div class="card-header">
            Birthdays This Month
        </div>
        <div class="container">
            <div class="table-responsive">
                @if(!$this_month_birthdays->isEmpty())
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Birth Date</th>
                            <th>Age</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($this_month_birthdays as $user)
                            @if(isset($user->date_of_birth))
                                <tr>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->date_of_birth }}</td>
                                    <td>{{ getUserAge($user->date_of_birth) }} </td>
                                </tr>
                            @endif
                        @endforeach
                        </tbody>
                    </table>
                @else
                    <div class="container">
                        <h6 class="text-center">No Birthdays this month</h6>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>