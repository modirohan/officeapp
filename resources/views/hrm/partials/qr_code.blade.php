<div class="col-md-12">
    <div class="card">
        <div class="card-header">
            Your QR Code
        </div>
        <div class="container ml-auto">
            <img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')
                        ->size(450)->errorCorrection('H')
                        ->generate(auth()->user()->token)) !!} ">
        </div>
    </div>
</div>