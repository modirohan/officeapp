<div class="col-md-6">
    <div class="card">
        <div class="card-header">
            Current Month Holidays
        </div>
        <div class="container">
            <div class="table-responsive">
                @if(!$this_month_holidays->isEmpty())
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Title</th>
                            <th>Holiday Date</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($this_month_holidays as $item)
                            <tr @if(!getDateStatus($item->date)) style="background-color:#9e9e9e59;" @endif>
                                <td>{{ $item->title }}</td>
                                <td>{{ $item->date }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @else
                    <div class="container">
                        <h6 class="text-center">No Holidays this month</h6>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>