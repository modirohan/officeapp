@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-1" style="background-color: #10bfb8; font-size: 28px; margin-bottom: 20px;">
                <div class="col-sm-1 text-white m-4 pr-9"><i class="fa fa-signal" aria-hidden="true"></i></div>
                <div class="col-sm-1 text-white m-4 pr-9"><i class="fa fa-users" aria-hidden="true"></i></div>
                <div class="col-sm-1 text-white m-4 pr-9"><i class="fa fa-window-restore" aria-hidden="true"></i></div>
                <div class="col-sm-1 text-white m-4 pr-9"><i class="fa fa-address-book" aria-hidden="true"></i></div>
                <div class="col-sm-1 text-white m-4 pr-9"><i class="fa fa-address-card" aria-hidden="true"></i></div>
                <div class="col-sm-1 text-white m-4 pr-9"><i class="fa fa-envelope" aria-hidden="true"></i></div>
                <div class="col-sm-1 text-white m-4 pr-9"><i class="fa fa-clock-o" style="margin-bottom: 577px;"></i></div>
                <div class="col-sm-1 text-white m-4 pr-9"><i class="fa fa-bar-chart"></i></div>
                <div class="col-sm-1 text-white m-4 pr-9"><i class="fa fa-file-text"></i></div>
                <div class="col-sm-1 text-white m-4 pr-9"><i class="fa fa-cog" aria-hidden="true"></i></div>
            </div>

            <div class="col-sm-11 bg-white" style="border-top:8px solid #10bfb8; font-size: 19px;">
                <div class="topnav">
                    <div class="search-container">
                        <form action="/action_page.php">
                            <i class="fa fa-heart text-secondary" aria-hidden="true"></i>
                            <input type="text" class="p-2 border-0 w-75" class="form-control" placeholder="Search.." name="search">
                            <button type="submit" class="float-left p-2 border-0 bg-white text-secondary"><i class="fa fa-search"></i></button>
                            <button class="p-2 bg-white border-0 color"><i class="fa fa-plus" aria-hidden="true"></i></button>
                            <button class="bg-white border-0 color"><i class="fa fa-envelope" aria-hidden="true"></i></button>
                            <button class="bg-white border-0 color"><i class="fas fa-bell"></i></button>
                            <button class="pl-7 p-2 bg-white border-0 pl-5 text-secondary"><i class="fa fa-user" aria-hidden="true"></i></button>
                        </form>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-3 text-center text-white div bg uL">
                        <h2 class="alert-heading">Assigned Jobs</h2>
                        <div class="card">
                            <img src="https://cdn3.iconfinder.com/data/icons/business-round-flat-vol-1-1/36/user_account_profile_avatar_person_student_male-512.png" alt="Avatar" style="width:130px; margin-left: 56px">
                            <p class="title">CEO & Founder, Example</p>
                            <div style="margin: 24px 0;">
                                <a href="#"><i class="fab fa-dribbble icon"></i></a>
                                <a href="#"><i class="fab fa-twitter icon"></i></a>
                                <a href="#"><i class="fab fa-linkedin-in icon"></i></a>
                                <a href="#"><i class="fab fa-facebook-square icon"></i></a>
                            </div>
                            <p><button class="button">Contact</button></p>
                        </div>
                        <div style="margin: 24px; padding: 24px; width: 100%; margin-left: 0px" class="bg-white">
                            <a href="#"><i class="fa fa-phone icon Pt-5 Pb-5"></i></a>
                            <a href="#"><i class="fas fa-address-book icon"></i></a>
                            <a href="#"><i class='fas fa-pen icon'></i></a>
                            <a href="#"><i class="fa fa-info icon" style="width: 34px"></i></a>
                        </div>
                        <div class="border-bottom-1 text-secondary text-left p-2">Email:-</div>
                        <div class="border-bottom-1 text-secondary text-left p-2">Phone:-</div>
                        <div class="border-bottom-1 text-secondary text-left p-2">Skills:-</div>
                        <div class="border-bottom-1 text-secondary text-left p-2">Resume:-</div>
                        <div class="border-bottom-1 text-secondary text-left p-2">Edu Qualification:-</div>
                        <div class="border-bottom-1 text-secondary text-left p-2">Work Experience:-</div>
                        <div class="border-bottom-1 text-secondary text-left p-2">Notice Period (days):-</div>
                        <div class="border-bottom-1 text-secondary text-left p-2">Last Organization:-</div>
                        <div class="border-bottom-1 text-secondary text-left p-2">Current Salary:-</div>
                        <div class="border-bottom-1 text-secondary text-left p-2">Expected Salary:-</div>
                        <div class="border-bottom-1 text-secondary text-left p-2">Owner:-</div>
                        <div class="border-bottom-1 text-secondary text-left p-2">Source:-</div>
                        <div class="border-bottom-1 text-secondary text-left p-2">Relevant Experience:-</div>
                    </div>
                    <div class="col-sm-9 uL">
                        <ul class="nav nav-tabs bordeR" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Notes</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Files</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Candidates Questions</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="History-tab" data-toggle="tab" href="#History" role="tab" aria-controls="contact" aria-selected="false">Candidates History</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="Feedback-tab" data-toggle="tab" href="#Feedback" role="tab" aria-controls="contact" aria-selected="false">Interview Feedback</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                <i class="fas fa-sticky-note icoN mt-7">  Notes</i>
                                <div class="row mt-5">
                                    <div class="col-sm-1 h-10 ml-3 coloR"></div>
                                <div class="col-sm-10">
                                    <div class="row bg-white pr-3 ml-2">
                                        <p class="paragrapH color">John Doe</p><p class="paragraph ">June21</p></div>
                                    <p class="paragrapH ml-1">bhd gyu rt fsb byu87fieru fhoirf yfiuibb vhbvbuvuiuivh vfdhgfh gjnghju yuyikuik  hyhtjh  tjytjyt j hytj hjhytjuty <i class="fa fa-edit iCON"></i>  <i class="fa fa-close iCON"></i></p>
                                </div>
                            </div>
                            </div>
                            <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                <i class="fa fa-file-text icoN mt-7">  File</i><br>
                                <div class="file btn btn-lg buttoN">Upload<input class="inpuT" type="file" name="file"/>
                                </div>
                                <div class="row mt-3">
                                    <div class="col-sm-1">
                                        <label class="containeR">
                                            <input type="checkbox" checked="checked">
                                            <span class="checkmark"></span>
                                        </label></div>
                                    <div class="col-sm-1"> <i class="fas fa-folder icON"></i></div>
                                    <div class="col-sm-5 paragrapH">Final Work</div>
                                    <div class="col-sm-4 paragrapH">5/sep/2019</div>
                                    <div class="col-sm-1"><i class="fa fa-trash iCON" aria-hidden="true"></i></div>
                                </div>
                                <div class="row mt-3">
                                    <div class="col-sm-1">
                                        <label class="containeR">
                                            <input type="checkbox" checked="checked">
                                            <span class="checkmark"></span>
                                        </label></div>
                                    <div class="col-sm-1"> <i class="fas fa-folder icON"></i></div>
                                    <div class="col-sm-5 paragrapH">Final Work</div>
                                    <div class="col-sm-4 paragrapH">5/sep/2019</div>
                                    <div class="col-sm-1"><i class="fa fa-trash iCON" aria-hidden="true"></i></div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                                <h4 class=" icoN1 mt-7">?  Candidates Questions</h4>
                                <div class="row">
                                <h3 class="text-secondary mt-5 ml-2">Q1.</h3>
                                <h3 class="icoN1 font mt-5">Lorem Ipsum is simply dummy text of the printing and typesetting?</h3>
                                </div>
                                <div class="row">
                                    <h5 class="text-secondary mt-5 ml-9 ">Your Answer</h5>
                                    <h5 class="text-secondary mt-5 mL float-right">Man Answer</h5>
                                </div>
                                <div class="row">
                                    <h5 class="text-secondary ml-9 ">Your Answer</h5>
                                    <h5 class="text-success mL float-right">   <input type="radio" name="gender" value="male">  Man Answer</h5>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="History" role="tabpanel" aria-labelledby="History-tab">
                                <i class="fa fa-history icoN1 mt-7 mb-0 CandidateS" aria-hidden="true">  Candidates History</i>
                                <div class="page">
                                    <div class="page__demo">
                                        <div class="main-container page__container">
                                            <div class="timeline">
                                                <div class="timeline__group">
                                                    <div class="timeline__box">
                                                        <div class="timeline__date">
                                                            <span class="timeline__day"><i class="fa fa-calendar paddinG" aria-hidden="true"></i></span>
                                                        </div>
                                                        <div class="timeline__post">
                                                            <div class="timeline__content">
                                                               <h2 class="color">Interview schedule</h2>
                                                                <div class="row">
                                                                <h5 class="text-secondary ml-4">Name:</h5><h5 class="color">  John Doe</h5>
                                                                    <h5 class="text-secondary ml-4">Assign by:</h5><h5 class="color mr-9">  Abc</h5><h5 class="color ml-9">  Today</h5>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="timeline__box">
                                                        <div class="timeline__date">
                                                            <span class="timeline__day"><i class="fa fa-user paddinG" aria-hidden="true"></i></span>
                                                        </div>
                                                        <div class="timeline__post">
                                                            <div class="timeline__content">
                                                                <h2 class="color">Profile Update</h2>
                                                                <div class="row">
                                                                    <h5 class="text-secondary ml-4">Name:</h5><h5 class="color">  John Doe</h5>
                                                                    <h5 class="text-secondary ml-4">Birth Date:</h5><h5 class="color">  John Doe</h5>
                                                                    <h5 class="text-secondary ml-4">Status:</h5><h5 class="color mr-7">  gcfh</h5>
                                                                    <h5 class="color  ml-2">  Today</h5>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="timeline__box">
                                                        <div class="timeline__date">
                                                            <span class="timeline__day"><i class="fa fa-group paddinG"></i></span>
                                                        </div>
                                                        <div class="timeline__post">
                                                            <div class="timeline__content">
                                                                <h2 class="color">Interview Feedback</h2>
                                                                <div class="row">
                                                                    <h5 class="text-secondary ml-3">Poor</h5>
                                                                    <h5 class="color ml-5">  Fair</h5>
                                                                   <h5 class="color ml-5"> Excellent</h5>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="tab-pane fade" id="Feedback" role="tabpanel" aria-labelledby="Feedback-tab">
                                <i class="fa fa-group icoNo mt-7">  Interview Feedback</i>
                                <nav class="navbar navbar-expand-sm mt-7">
                                    <ul class="navbar-nav navbaR">
                                        <li class="nav-item">
                                            <a class="nav-link text-white" href="#">Poor</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link text-white" href="#">Fair</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link text-white" href="#">Excellent</a>
                                        </li>
                                    </ul>
                                </nav>
                                <div class="progress mt-7 w-50 ml-4 bordeR">
                                    <div class="progress-bar progress-bar-striped progress-bar-animated" style="width:60%">60%</div>
                                </div>
                                <div class="progress mt-5 w-50 ml-4 bordeR">
                                    <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" style="width:40%">40%</div>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>









            </div>
        </div>
    </div>
    </div>
@endsection

@section('header_styles')
    <<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="{{ asset('css/candidate.css') }}" rel="stylesheet">
    <<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
@endsection

@section('footer_scripts')

@endsection