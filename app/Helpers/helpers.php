<?php

use Carbon\Carbon;

if (!function_exists('getDateStatus')) {
    function getDateStatus($date)
    {
        if ($date < Carbon::now()->toDateString()) {
            return '0';
        } else {
            return '1';
        }
    }
}

function getCurrentCandidate()
{
    if (auth()->guard('candidates')->check()) {
        $user = auth()->guard('candidates')->user();
        return $user;
    }
}

if (!function_exists('getEntryType')) {
    function getEntryType($entry_type)
    {
        return \App\Models\Hrm\Timelogs::ENTRY_TYPE[$entry_type];
    }
}

if (!function_exists('getEntryMode')) {
    function getEntryMode($entry_mode)
    {
        return \App\Models\Hrm\Timelogs::ENTRY_MODE[$entry_mode];
    }
}
if (!function_exists('getTimeDifference')) {
    function getTimeDiff($dtime, $atime)
    {
        $nextDay = 0;
        $dep = explode(':', $dtime);
        $arr = explode(':', $atime);
        $diff = abs(mktime($dep[0], $dep[1], 0, date('n'), date('j'), date('y')) - mktime($arr[0], $arr[1], 0, date('n'), date('j') + $nextDay, date('y')));
        return $diff;
    }
}

function calculateSecondsToMinutesAndHours($seconds, $f = 'h ', $m = 'm')
{
    return sprintf("%02d%s%02d%s", floor($seconds / 3600), $f, ($seconds / 60) % 60, $m);
}


function CalculateTime($times)
{
    $i = 0;
    foreach ($times as $time) {
        $i += $time;
    }
    return $i;
}

function convertToHoursMins($time, $format = '%02d:%02d')
{
    if ($time < 1) {
        return;
    }
    $hours = floor($time / 60);
    $minutes = ($time % 60);
    return sprintf($format, $hours, $minutes);
}

if (!function_exists('calculateTimeByEntryType')) {
    function calculateTimeByEntryType($data)
    {
        // Check if the array_chunk has no remainder
        $chunked_timesheet = array_chunk($data, 2);
        $calculated_time = [];
        foreach ($chunked_timesheet as $timesheet_record) {
            if (count($timesheet_record) % 2 != 0) {
                $consider_current_time = Carbon::now()->toTimeString();
                foreach ($timesheet_record as $timesheet_data) {
                    $calculated_time[$timesheet_data['entry_type']][] = getTimeDiff($consider_current_time, $timesheet_data['entry_time']);
                }
            } else {
                $count = 0;
                foreach ($timesheet_record as $timesheet_data) {
                    $first_time_value = ($count == 0) ? $timesheet_data['entry_time'] : $first_time_value;
                    if ($count != 0) {
                        $calculated_time[$timesheet_data['entry_type']][] = getTimeDiff($timesheet_data['entry_time'], $first_time_value);
                    }
                    $count = ($count == 0) ? 1 : 0;
                }
            }
        }
        $time = [];
        foreach ($calculated_time as $key => $data) {
            foreach ($data as $value) {
                array_push($time, $value);
            }
        }
        return CalculateTime($time);
    }

    if (!function_exists('getUserAge')) {
        function getUserAge($birth_date)
        {
            if ($birth_date != "") {
                return \Carbon\Carbon::parse($birth_date)->age . " Years";
            } else {
                return null;
            }
        }
    }

    if (!function_exists('getLeaveReason')) {
        function getLeaveReason($reason)
        {
            return \App\Models\Hrm\LeaveApplications::REASON[$reason];
        }
    }

    if (!function_exists('getTimeShift')) {
        function getTimeShift($time_shifts)
        {
            if (isset($time_shifts)) {
                return \App\Models\User::TIME_SHIFTS[$time_shifts];
            }

        }
    }

    if (!function_exists('getCarbonDate')) {
        function getCarbonDate($date)
        {
            return \Carbon\Carbon::parse($date);
        }
    }

    if (!function_exists('getDateDiff')) {
        function getDateDiff($start_date, $end_date)
        {
            return $start_date->diffInDays($end_date) + 1;
        }
    }

    if (!function_exists('getHalfaySession')) {
        function getHalfaySession($session)
        {
            return \App\Models\Hrm\LeaveApplications::HALF_DAY_SESSION[$session];
        }
    }

    if (!function_exists('calculateDeductableValueAndPaidLeaveStatusForHalfSession')) {
        function calculateDeductableValueAndPaidLeaveStatusForHalfSession($leaves_balance)
        {
            if ($leaves_balance >= 0.5) {
                $deductable_value = 0;
                $is_paid_leave = 1;
            } else {
                $deductable_value = 0.5;
                $is_paid_leave = 0;
            }
            $half_day = 1;
            return ['deductable_value' => $deductable_value, 'is_paid_leave' => $is_paid_leave, 'half_day' => $half_day];
        }
    }

    if (!function_exists('calculateDeductableValueAndPaidLeaveStatusForFullSession')) {
        function calculateDeductableValueAndPaidLeaveStatusForFullSession($leaves_balance)
        {
            if ($leaves_balance >= 1) {
                $deductable_value = 0;
                $is_paid_leave = 1;
            } else {
                $deductable_value = 1;
                $is_paid_leave = 0;
            }
            $half_day = 0;
            return ['deductable_value' => $deductable_value, 'is_paid_leave' => $is_paid_leave, 'half_day' => $half_day];
        }
    }
    if (!function_exists('getLeaveStatus')) {
        function getLeaveStatus($status)
        {
            if ($status == 1) {
                return 'Approved';
            } else {
                return 'Pending';
            }
        }
    }
    if (!function_exists('showUserProfileImage')) {
        function showUserProfileImage($image_name = "")
        {
            return url(\App\Models\User::IMG_URL . $image_name);
        }
    }

}
