<?php

namespace App\Interfaces;


use Illuminate\Http\Request;

interface UserRepositoryInterface
{
    public function updateProfile($data);

    public function getProfile($user_id);

}