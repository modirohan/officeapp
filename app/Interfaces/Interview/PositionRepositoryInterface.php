<?php

namespace App\Interfaces\Interview;

use Illuminate\Http\Request;

interface PositionRepositoryInterface
{
    public function getPositionList($perPage);
    public function storePosition(Request $request);
    public function getPositionById($id);
    public function updatePosition(Request $request, $id);
    public function deletePosition($id);
    public function showPosition($id);
    public function getPositionTitles();
}