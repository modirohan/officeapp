<?php

namespace App\Interfaces\Interview;

use Illuminate\Http\Request;

interface InterviewRepositoryInterface
{
    public function getAllInterviews(Request $request);
    public function storeInterview(Request $request);
    public function getInterviewById($id);
    public function updateInterview(Request $request, $id);
    public function deleteInterview($id);

}