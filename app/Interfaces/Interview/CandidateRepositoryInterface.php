<?php

namespace App\Interfaces\Interview;

use Illuminate\Http\Request;

interface CandidateRepositoryInterface
{
    public function getAllCandidate(Request $request);
    public function storeCandidate(Request $request);
    public function getCandidateById($id);
    public function updateCandidate(Request $request, $id);
    public function deleteCandidate($id);

}