<?php

namespace App\Interfaces\Interview;

use Illuminate\Http\Request;

interface SkillRepositoryInterface
{
    public function getSkillList($perPage,Request $request);
    public function storeSkill(Request $request);
    public function getSkillById($id);
    public function updateSkill(Request $request, $id);
    public function deleteSkill($id);

}