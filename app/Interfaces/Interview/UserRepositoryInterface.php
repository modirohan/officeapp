<?php

namespace App\Interfaces\Interview;

use Illuminate\Http\Request;

interface UserRepositoryInterface
{
    public function getUserList($perPage,Request $request);
    public function storeUser(Request $request);
    public function getUserById($id);
    public function updateUser(Request $request, $id);
    public function deleteUser($id);
    public function showUser($id);
    public function validToken($token);
    public function updateUserDetails(Request $request);

}