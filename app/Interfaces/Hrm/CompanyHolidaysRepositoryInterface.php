<?php

namespace App\Interfaces\Hrm;

interface CompanyHolidaysRepositoryInterface
{
    public function getAllHolidays();
    public function getHolidaysByDate($start_date, $end_date);
    public function getCurrentYearHolidays();
    public function getCurrentMonthHolidays();
}