<?php

namespace App\Interfaces\Hrm;

interface LeavesAccountRepositoryInterface
{
    public function storeLeave($data);
}