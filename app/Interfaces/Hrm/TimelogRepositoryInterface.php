<?php

namespace App\Interfaces\Hrm;

use Illuminate\Http\Request;

interface TimelogRepositoryInterface
{
    public function storeTimelog(Request $request);
    public function getUserTimeLog($user_id = [], $start_date = null, $end_date = null);
    public function getTodaysTimeLog();
    public function getTodaysTimeLogDetails();
    public function getSuitableEntryTypeAndEntryMode();
    public function getTimeLogDetailsByDateRange($user_id = [], $start_date = null, $end_date = null);
}