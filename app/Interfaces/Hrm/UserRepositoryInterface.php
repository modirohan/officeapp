<?php

namespace App\Interfaces\Hrm;

interface UserRepositoryInterface
{
    public function getUsersByCurrentMonthBirthday();
    public function getUsersHavingBirthdaysByDate($start_date, $end_date);
    public function getAllUsers();
}