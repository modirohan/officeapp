<?php

namespace App\Interfaces\Hrm;

interface LeaveApplicationHistoryRepositoryInterface
{
    public function storeLeaveApplicationHistory($data);
}