<?php

namespace App\Interfaces\Hrm;

interface LeaveApplicationRepositoryInterface
{
    public function storeLeaveApplication($data);

    public function getLeaveApplication();

    public function getLeaveApplicationByDateRange($start_date, $end_date);
}