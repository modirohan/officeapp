<?php

namespace App\Interfaces;

interface ForgotPasswordRepositoryInterface
{
    public function resetPasswordLink($token = '');
    public function sendResetLinkToMail($reset_data);
}