<?php

namespace App\Providers;

// Admin Interfaces
use App\Http\Controllers\Api\Repositories\Hrm\TimesheetRepository;
use App\Http\Controllers\Api\Interfaces\Hrm\TimesheetRepositoryInterface;
use App\Interfaces\ForgotPasswordRepositoryInterface;
use App\Interfaces\Hrm\CompanyHolidaysRepositoryInterface;
use App\Interfaces\Hrm\LeaveApplicationHistoryRepositoryInterface;
use App\Interfaces\Hrm\LeaveApplicationRepositoryInterface;
use App\Interfaces\Hrm\LeavesAccountRepositoryInterface;
use App\Interfaces\Hrm\TimelogRepositoryInterface;
use App\Interfaces\Interview\CandidateRepositoryInterface;
use App\Interfaces\Interview\InterviewRepositoryInterface;
use App\Interfaces\Interview\PositionRepositoryInterface;
use App\Interfaces\Interview\SkillRepositoryInterface;
use App\Interfaces\Interview\UserRepositoryInterface;
use App\Interfaces\RoleRepositoryInterface;
use App\Repositories\ForgotPasswordRepository;
use App\Repositories\Hrm\CompanyHolidaysRepository;
use App\Repositories\Hrm\LeaveAccountRepository;
use App\Repositories\Hrm\LeaveApplicationHistoryRepository;
use App\Repositories\Hrm\LeaveApplicationRepository;
use App\Repositories\Hrm\TimelogRepository;
use App\Repositories\Interview\CandidateRepository;
use App\Repositories\Interview\InterviewRepository;
use App\Repositories\Interview\PositionRepository;
use App\Repositories\Interview\SkillRepository;
use App\Repositories\Interview\UserRepository;
use App\Repositories\RoleRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {

         $this->app->bind(UserRepositoryInterface::class, function ($app) {
            return $app->make(UserRepository::class);
        });

        $this->app->bind(PositionRepositoryInterface::class, function ($app) {
            return $app->make(PositionRepository::class);
        });
        $this->app->bind(SkillRepositoryInterface::class, function ($app) {
            return $app->make(SkillRepository::class);
        });
        $this->app->bind(CandidateRepositoryInterface::class, function ($app) {
            return $app->make(CandidateRepository::class);
        });
        $this->app->bind(InterviewRepositoryInterface::class, function ($app) {
            return $app->make(InterviewRepository::class);
        });
        $this->app->bind(RoleRepositoryInterface::class, function ($app) {
            return $app->make(RoleRepository::class);
        });
        $this->app->bind(ForgotPasswordRepositoryInterface::class, function ($app) {
            return $app->make(ForgotPasswordRepository::class);
        });

        //HRM Repositories
        $this->app->bind(TimelogRepositoryInterface::class, function ($app) {
            return $app->make(TimelogRepository::class);
        });
        $this->app->bind(CompanyHolidaysRepositoryInterface::class, function ($app) {
            return $app->make(CompanyHolidaysRepository::class);
        });
        $this->app->bind(\App\Interfaces\Hrm\UserRepositoryInterface::class, function ($app) {
            return $app->make(\App\Repositories\Hrm\UserRepository::class);
        });

        $this->app->bind(LeaveApplicationRepositoryInterface::class, function ($app) {
            return $app->make(LeaveApplicationRepository::class);
        });
        $this->app->bind(LeavesAccountRepositoryInterface::class, function ($app) {
            return $app->make(LeaveAccountRepository::class);
        });
        $this->app->bind(LeaveApplicationHistoryRepositoryInterface::class, function ($app) {
            return $app->make(LeaveApplicationHistoryRepository::class);
        });


        /*API Repositories*/
            /*HRM*/
        $this->app->bind(TimesheetRepositoryInterface::class, function ($app) {
            return $app->make(TimesheetRepository::class);
        });


        //Global Repositories
        $this->app->bind(\App\Interfaces\UserRepositoryInterface::class, function ($app) {
            return $app->make(\App\Repositories\UserRepository::class);
        });

    }
}