<?php

namespace App\Http\Controllers\Api\Repositories\Hrm;

use App\Http\Controllers\Api\Interfaces\Hrm\TimesheetRepositoryInterface;
use App\Models\Hrm\Timelogs;
use Carbon\Carbon;

class TimesheetRepository implements TimesheetRepositoryInterface
{

    public function getUserTimelogByDate($data)
    {
        $date = getCarbonDate($data['date']);
        $user_id = $data['user_id'];
        return Timelogs::where('user_id', $user_id)->where('entry_date', $date)->get()->toArray();
    }

    public function getTimeLogDetailsByDate($requestData)
    {
        $user_id = $requestData['user_id'];
        $date = getCarbonDate($requestData['date']);
        $timesheet_data = Timelogs::where('entry_date', $date)->select("entry_time", "entry_type", "entry_mode")->where('user_id', $user_id)->orderBy("entry_time", "ASC")->get()->groupBy('entry_type')->toArray();
        $data = [];
        $timesheet_info = [];
        $timesheet_info['actual_working_hours'] = 0;
        foreach ($timesheet_data as $timesheet) {
            $data[$timesheet[0]['entry_type']] = calculateTimeByEntryType($timesheet);
        }
        $data['Normal'] = (isset($data['Normal'])) ? ($data['Normal']) : 0;
        $data['Break'] = (isset($data['Break'])) ? ($data['Break']) : 0;
        $data['Lunch'] = (isset($data['Lunch'])) ? ($data['Lunch']) : 0;
        $timesheet_info['actual_working_hours'] = calculateSecondsToMinutesAndHours($data['Normal'] - $data['Lunch'] - $data['Break']);
        $timesheet_info['total_working_hours'] = calculateSecondsToMinutesAndHours($data['Normal']);
        $timesheet_info['total_break_hours'] = calculateSecondsToMinutesAndHours($data['Break']);
        $timesheet_info['total_lunch_hours'] = calculateSecondsToMinutesAndHours($data['Lunch']);
        return $timesheet_info;
    }

    public function storeTimeLog($requestData)
    {
        $requestData['entry_time'] = Carbon::now()->toTimeString();
        $requestData['entry_date'] = Carbon::now()->toDateString();
        Timelogs::create($requestData);
        return true;
    }
}
