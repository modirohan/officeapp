<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{
    public function index(Request $request)
    {
        $token = $request->get('token');
        if($token != null){
            $user = User::where('token', $token)->get()->first();
            if(!isset($user)){
                return response()->json([
                    'status' => 100,
                    'message' => "User Not Found With such token",
                    'data' => (object)[]]);
            }
            $access_token = str_random(16);
            $user->access_token = $access_token;
            $user->save();
            if(isset($user->image)){
                $user->image = showUserProfileImage($user->image);
            }else{
                $user->image = asset('images/l60Hf.png');
            }
            return response()->json([
                'status' => 200,
                'message' => "Successful Login Through token",
                'data' => $user]);
        }else{
            $login_rules = array(
                'email_address' => 'required|email',
                'password' => "required"
            );
            // run the validation rules on the inputs from the form
            $validator = Validator::make(Input::all(), $login_rules);
            // if the validator fails, redirect back to the form
            if ($validator->fails()) {
                $errors = $validator->errors();
                return response()->json([
                    'status' => 400,
                    'message' => implode(",", $errors->all()),
                    'data' => (object)[],
                ]);
            }

            $credentials = ['email' => $request->get('email_address'), 'password' => $request->get('password')];
            if (Auth::attempt($credentials)) {
                $user = Auth::user();
                $access_token = str_random(16);
                $user->access_token = $access_token;
                $user->save();
                if(isset($user->image)){
                    $user->image = showUserProfileImage($user->image);
                }else{
                    $user->image = asset('images/l60Hf.png');
                }
                return response()->json([
                    'status' => 200,
                    'message' => "Successful Login through credentials",
                    'data' => $user ]);
            }else{
                return response()->json([
                    'status' => 100,
                    'message' => "Invalid Email or Password",
                    'data' => (object)[]]);
            }
        }
    }
}
