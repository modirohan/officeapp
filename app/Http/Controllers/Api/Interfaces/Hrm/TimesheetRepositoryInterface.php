<?php

namespace App\Http\Controllers\Api\Interfaces\Hrm;

interface TimesheetRepositoryInterface
{
    public function getUserTimelogByDate($data);

    public function storeTimeLog($data);

    public function getTimeLogDetailsByDate($data);
}