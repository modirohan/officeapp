<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\Interfaces\Hrm\TimesheetRepositoryInterface;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class TimeSheetController extends Controller
{
    protected $timesheetRepository;

    public function __construct(TimesheetRepositoryInterface $timesheetRepository)
    {
        $this->timesheetRepository = $timesheetRepository;
    }
    public function store(Request $request){
        $timelog_rules = array(
            'access_token' => "required",
            'entry_type' => "required|in:1,2,3",
            'entry_mode' => "required|in:1,2"
        );
        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $timelog_rules);
        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
                'status' => 400,
                'message' => implode(",", $errors->all()),
                'data' => [],
            ]);
        }
        $user = User::where('access_token', $request->get('access_token'))->get()->first();
        if(!isset($user)){
            return response()->json([
                'status' => 100,
                'message' => "Invalid Access Token",
                'data' => []]);
        }
        $requestData['entry_type'] = $request->get('entry_type');
        $requestData['entry_mode'] = $request->get('entry_mode');
        $requestData['user_id'] = $user->id;
        $this->timesheetRepository->storeTimeLog($requestData);
        return response()->json([
            'status' => 200,
            'message' => "Timelog Entry Logged in",
            'data' => []
            ]);

    }
    public function getTimeLogByDate(Request $request){
        $timelog_rules = array(
            'date' => 'required|date',
            'access_token' => "required"
        );
        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $timelog_rules);
        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
                'status' => 400,
                'message' => implode(",", $errors->all()),
                'data' => [],
            ]);
        }

        $user = User::where('access_token', $request->get('access_token'))->get()->first();
        if(!isset($user)){
            return response()->json([
                'status' => 100,
                'message' => "Invalid Access Token",
                'data' => []]);
        }
        $data['user_id'] = $user->id;
        $data['date'] = $request->get('date');

        $timelogs = $this->timesheetRepository->getUserTimelogByDate($data);
        $timelogsDetails = $this->timesheetRepository->getTimeLogDetailsByDate($data);
        if(empty($timelogs)){
            return response()->json([
                'status' => 100,
                'message' => "No Timelogs Available",
                'data' => []]);
        }

        return response()->json([
            'status' => 200,
            'message' => "Available Timelogs",
            'data' => [
                'timelogs' => $timelogs,
                'timelog_summary' => $timelogsDetails
            ]]);
    }
}
