<?php

namespace App\Http\Controllers\Hrm;

use App\Http\Controllers\Controller;
use App\Interfaces\Hrm\CompanyHolidaysRepositoryInterface;
use App\Interfaces\Hrm\LeaveApplicationRepositoryInterface;
use App\Interfaces\Hrm\TimelogRepositoryInterface;
use App\Interfaces\Hrm\UserRepositoryInterface;
use App\Services\CalenderService;

class TimesheetController extends Controller
{
    protected $timelogRepository;
    protected $calender;
    protected $companyHolidaysRepository;
    protected $userRepository;
    protected $leaveApplicationRepository;

    public function __construct(LeaveApplicationRepositoryInterface $leaveApplicationRepository, UserRepositoryInterface $userRepository, CalenderService $calender, TimelogRepositoryInterface $timelogRepository, CompanyHolidaysRepositoryInterface $companyHolidaysRepository)
    {
        $this->timelogRepository = $timelogRepository;
        $this->calender = $calender;
        $this->companyHolidaysRepository = $companyHolidaysRepository;
        $this->userRepository = $userRepository;
        $this->leaveApplicationRepository = $leaveApplicationRepository;
    }
    public function index()
    {
        $calendar = $this->calender->createCalender();
        return view('hrm.timesheet.index', compact('calendar'));
    }
    public function getMonthlyCalender($start_date, $end_date){
        $timelogs = $this->timelogRepository->getUserTimeLog([auth()->user()->id], $start_date, $end_date);
        $company_leaves = $this->companyHolidaysRepository->getHolidaysByDate($start_date, $end_date);
        $timelog_details = $this->timelogRepository->getTimeLogDetailsByDateRange([auth()->user()->id], $start_date, $end_date);
        $users_birthdays = $this->userRepository->getUsersHavingBirthdaysByDate($start_date, $end_date);
        $leaves_periods = $this->leaveApplicationRepository->getLeaveApplicationByDateRange($start_date, $end_date);
        $data = array_merge($timelogs, $company_leaves, $timelog_details, $users_birthdays, $leaves_periods);
        return json_encode([
            'status' => true,
            'data' => $data
        ]);
    }
}
