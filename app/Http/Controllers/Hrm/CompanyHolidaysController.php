<?php

namespace App\Http\Controllers\Hrm;

use App\Http\Controllers\Controller;
use App\Interfaces\Hrm\CompanyHolidaysRepositoryInterface;

class CompanyHolidaysController extends Controller
{
    protected $companyHolidayRepository;

    public function __construct(CompanyHolidaysRepositoryInterface $companyHolidayRepository)
    {
        $this->companyHolidayRepository =$companyHolidayRepository;
    }

    public function index(){
        $company_holidays = $this->companyHolidayRepository->getCurrentYearHolidays();
        return view('hrm.company_holidays.index', compact('company_holidays'));
    }
}
