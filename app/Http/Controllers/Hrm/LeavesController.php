<?php

namespace App\Http\Controllers\Hrm;

use App\Http\Controllers\Controller;
use App\Interfaces\Hrm\LeaveApplicationHistoryRepositoryInterface;
use App\Interfaces\Hrm\LeaveApplicationRepositoryInterface;
use App\Interfaces\Hrm\LeavesAccountRepositoryInterface;
use Yajra\DataTables\Utilities\Request;

class LeavesController extends Controller
{
    protected $leavesApplicationRepository;
    protected $leaveAccountRepository;
    protected $leavesApplicationHistoryRepository;

    public function __construct(LeaveApplicationRepositoryInterface $leavesApplicationRepository, LeavesAccountRepositoryInterface $leavesAccountRepository, LeaveApplicationHistoryRepositoryInterface $leaveApplicationHistoryRepository){
        $this->leavesApplicationRepository = $leavesApplicationRepository;
        $this->leaveAccountRepository = $leavesAccountRepository;
        $this->leavesApplicationHistoryRepository = $leaveApplicationHistoryRepository;
    }

    public function index()
    {
        $leaves = $this->leavesApplicationRepository->getLeaveApplication();
        return view('hrm.leave_management.index', compact('leaves'));
    }

    public function create()
    {
        return view('hrm.leave_management.create');
    }

    public function store(Request $request)
    {
        $requestData = $request->except('_token');
        $leave_application = $this->leavesApplicationRepository->storeLeaveApplication($requestData);
        if(!$leave_application){
            return redirect()->route('leaves.index')->with('error', 'You can not apply for leave. You have pending leave in your Account');
        }
        $requestData['leave_application_id'] = $leave_application->id;
        $this->leaveAccountRepository->storeLeave($requestData);
        $this->leavesApplicationHistoryRepository->storeLeaveApplicationHistory($requestData);
        return redirect()->route('leaves.index')->with('success', 'Leave application has been registered successfully');
    }
}
