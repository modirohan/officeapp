<?php

namespace App\Http\Controllers\Hrm;

use App\Http\Controllers\Controller;
use App\Interfaces\Hrm\TimelogRepositoryInterface;
use Illuminate\Http\Request;

class TimelogsController extends Controller
{
    protected $timelogRepository;

    public function __construct(TimelogRepositoryInterface $timelogRepository){
        $this->timelogRepository = $timelogRepository;
    }
    public function index(){
        $timelogs = $this->timelogRepository->getTodaysTimeLog();
        $timeLogDetails = $this->timelogRepository->getTodaysTimeLogDetails();
        $data = $this->timelogRepository->getSuitableEntryTypeAndEntryMode();
        return view('hrm.timelogs.index', compact('timelogs','timeLogDetails', 'data'));
    }

    public function store(Request $request){
        $this->timelogRepository->storeTimelog($request);
        return redirect()->back()->with('success', 'Entry Created successfully');
    }

}
