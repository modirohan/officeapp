<?php

namespace App\Http\Controllers;


use App\Interfaces\Hrm\CompanyHolidaysRepositoryInterface;
use App\Interfaces\Hrm\TimelogRepositoryInterface;
use App\Interfaces\Hrm\UserRepositoryInterface;

class DashboardController extends Controller
{
    protected $timelogRepository;
    protected $companyHolidaysRepository;
    protected $userRepository;

    public function __construct(UserRepositoryInterface $userRepository, TimelogRepositoryInterface $timelogRepository,
                                CompanyHolidaysRepositoryInterface $companyHolidaysRepository)
    {
        $this->timelogRepository = $timelogRepository;
        $this->companyHolidaysRepository = $companyHolidaysRepository;
        $this->userRepository = $userRepository;
    }

    public function index()
    {
        if (auth()->user()->is_pms_enabled == 1) {
            return view('pms.dashboard');
        }
        if (auth()->user()->is_interview_enabled == 1) {
            return redirect()->route('talent.dashboard');
        }
        return view('dashboard');
    }

    public function hrm()
    {
        if (auth()->user()->is_timetracking_enabled == 1) {
            $timeLogDetails = $this->timelogRepository->getTodaysTimeLogDetails();
            $this_month_holidays = $this->companyHolidaysRepository->getCurrentMonthHolidays();
            $this_month_birthdays = $this->userRepository->getUsersByCurrentMonthBirthday();
            return view('hrm.dashboard', compact('timeLogDetails', 'this_month_holidays', 'this_month_birthdays'));
        }
    }

    public function admin()
    {
        if (auth()->user()->is_admin == 1) {
            return view('admin.dashboard');
        }
    }

    public function dashboard()
    {
        return view('new_dashboard');
    }
}