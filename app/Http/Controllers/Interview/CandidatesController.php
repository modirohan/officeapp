<?php

namespace App\Http\Controllers\Interview;

use App\Http\Controllers\Controller;
use App\Interfaces\Interview\CandidateRepositoryInterface;
use App\Interfaces\Interview\PositionRepositoryInterface;
use Illuminate\Http\Request;

class CandidatesController extends Controller
{
    protected $candidateRepository;
    protected $positionRepository;
    public function __construct(CandidateRepositoryInterface $candidateRepository, PositionRepositoryInterface $positionRepository)
    {
        $this->candidateRepository = $candidateRepository;
        $this->positionRepository = $positionRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $candidates = $this->candidateRepository->getAllCandidate($request);
        return view('interview.candidates.index', compact('candidates'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $positions = $this->positionRepository->getPositionTitles();
        return view('interview.candidates.create',compact('positions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
       //dd($request->all());
        $this->candidateRepository->storeCandidate($request);
        return redirect()->route('talent.candidates.index')->with('flash_message', 'Position added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $candidate = $this->candidateRepository->getCandidateById($id);
        return view('interview.candidates.show', compact('candidate'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $candidate = $this->candidateRepository->getCandidateById($id);
        return view('interview.candidates.edit', compact('candidate'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {

        $this->candidateRepository->updateCandidate($request, $id);
        return redirect()->route('talent.candidates.index')->with('flash_message', 'Candidate  updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $this->candidateRepository->deleteCandidate($id);
        return redirect()->route('talent.candidates.index')->with('flash_message', 'Candidate  deleted!');
    }
}
