<?php

namespace App\Http\Controllers\Interview;

use App\Http\Controllers\Controller;
use App\Interfaces\Interview\InterviewRepositoryInterface;
use Illuminate\Http\Request;

class InterviewController extends Controller
{
    protected $interviewRepository;
    public function __construct(InterviewRepositoryInterface $interviewRepository)
    {
        $this->interviewRepository = $interviewRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $interviews = $this->interviewRepository->getAllInterviews($request);
        return view('interview.interviews.index', compact('interviews'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('interview.interviews.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $this->interviewRepository->storeInterview($request);
        return redirect()->route('talent.interviews.index')->with('flash_message', 'Position added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $interview = $this->interviewRepository->getInterviewById($id);
        return view('interview.interviews.show', compact('interview'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $interview = $this->interviewRepository->getInterviewById($id);
        return view('interview.interviews.edit', compact('interview'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->interviewRepository->updateInterview($request, $id);
        return redirect()->route('talent.interviews.index')->with('flash_message', 'Position updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $this->interviewRepository->deleteInterview($id);
        return redirect()->route('talent.interviews.index')->with('flash_message', 'Position deleted!');
    }
}
