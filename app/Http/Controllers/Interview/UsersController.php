<?php

namespace App\Http\Controllers\Interview;

use App\Http\Controllers\Controller;
use App\Interfaces\Interview\PositionRepositoryInterface;
use App\Interfaces\Interview\UserRepositoryInterface;
use App\Interfaces\RoleRepositoryInterface;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    protected $userRepository;
    protected $roleRepository;
    protected $positionRepository;
    public function __construct(UserRepositoryInterface $userRepository, RoleRepositoryInterface $roleRepository, PositionRepositoryInterface $positionRepository)
    {
        $this->userRepository = $userRepository;
        $this->roleRepository = $roleRepository;
        $this->positionRepository = $positionRepository;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */

    public function index(Request $request)
    {
        $perPage = 25;
        $users = $this->userRepository->getUserList($perPage,$request);
        return view('interview.users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $positionsList = $this->positionRepository->getPositionTitles();
        $rolesList = $this->roleRepository->getRolesTitles();
        return view('interview.users.create',compact('positionsList','rolesList'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|unique:users|email',
        ]);
        $this->userRepository->storeUser($request);
        return redirect()->route('talent.users.index')->with('flash_message', 'successfully created...!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $user =$this->userRepository->showUser($id);
        return view('interview.users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $user = $this->userRepository->getUserById($id);
        $positionsList = $this->positionRepository->getPositionTitles();
        $rolesList = $this->roleRepository->getRolesTitles();
        return view('interview.users.edit', compact('user','positionsList','rolesList'));
    }

    public function update(Request $request, $id)
    {
        $this->userRepository->updateUser($request, $id);
        return redirect()->route('talent.users.index')->with('flash_message', 'Data successfully updated...!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $this->userRepository->deleteUser($id);
        return redirect()->route('talent.users.index')->with('flash_message', 'successfully deleted...!');
    }

    public function activateLink($token = '')
    {
        $check_valid_token = $this->userRepository->validToken($token);
        $result = array("success" => true);
        if (count($check_valid_token) > 0) {
            $result['name'] = $check_valid_token->name;
            $result['email'] = $check_valid_token->email;
            $result['success'] = true;
        } else {
            $result['success'] = false;
        }
        return view('interview.users.user_activation_form', compact('result','check_valid_token'));
    }
    public function storeUserDetails(Request $request)
    {
        $this->validate($request, [
            'password' => 'required',
        ]);
        $this->userRepository->updateUserDetails($request);
        return redirect()->route("talent.users.index")->with('flash_message', 'your data is successfully updated');
    }
}
