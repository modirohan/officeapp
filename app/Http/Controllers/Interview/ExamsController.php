<?php

namespace App\Http\Controllers\Interview;

use App\Http\Controllers\Controller;
use App\Interfaces\Interview\CandidateRepositoryInterface;
use App\Interfaces\Interview\PositionRepositoryInterface;
use App\Models\Answers;
use App\Models\ExamResult;
use App\Models\Questions;
use App\Models\StudentExaminations;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ExamsController extends Controller
{
    protected $candidateRepository;
    protected $positionRepository;

    public function __construct(CandidateRepositoryInterface $candidateRepository, PositionRepositoryInterface $positionRepository)
    {
        $this->candidateRepository = $candidateRepository;
        $this->positionRepository = $positionRepository;
    }

    public function index()
    {
        return view('interview.exams.index');
    }

    public function createExamQuestion($question_id)
    {
        $question_detail = Questions::with('relatedAnswers')->where('id', $question_id)->first();
        if ($question_id == 1) {
            $candidate = getCurrentCandidate();
            $isExistData = StudentExaminations::where('student_id', $candidate->id)->first();
            if ($isExistData){
                $isExistData->update(['exam_id' => 1, 'correct_answers' => 0, 'total_questions' => 15, 'start_time' => Carbon::now(), 'end_time' => Carbon::now()->addMinutes(25)]);
            }else{
                $data = ['student_id' => $candidate->id, 'exam_id' => 1, 'correct_answers' => 0, 'total_questions' => 15, 'start_time' => Carbon::now(), 'end_time' => Carbon::now()->addMinutes(25)];
                StudentExaminations::create($data);
            }
        }
        return view('interview.exams.create_question', compact('question_detail'));
    }

    public function storeAnswer(Request $request)
    {
        $candidate = getCurrentCandidate();
        $requestData = $request->all();
        $nextQueID = $requestData['question_id'] + 1;
        if ($nextQueID == 16) {
            return redirect()->route('talent.exams.thanks');
        } else {
            if (isset($requestData['answer_id'])){
                $answerDetail = Answers::where('id', $requestData['answer_id'])->where('question_id', $requestData['question_id'])->first();
                $requestData['is_correct_answer'] = ($answerDetail['is_correct_answer'] == 1) ? 1 : 0;
                $requestData['student_id'] = $candidate->id;
                $requestData['exam_id'] = 1;
                $requestData['is_correct_answer'] = ($answerDetail['is_correct_answer'] == 1) ? 1 : 0;
                ExamResult::create($requestData);
                if ($requestData['is_correct_answer'] == 1){
                    StudentExaminations::where('student_id', $candidate->id)->increment('correct_answers');
                }
            }
            return redirect()->route('talent.exams.question', $nextQueID);
        }
    }

    public function thanks()
    {
        $candidate = getCurrentCandidate();
        StudentExaminations::where('student_id', $candidate->id)->update(['actual_end_time' => Carbon::now()]);
        return view('interview.exams.thanks');
    }

}
