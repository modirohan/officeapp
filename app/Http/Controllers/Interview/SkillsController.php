<?php

namespace App\Http\Controllers\Interview;

use App\Http\Controllers\Controller;
use App\Interfaces\Interview\PositionRepositoryInterface;
use App\Interfaces\Interview\SkillRepositoryInterface;
use Illuminate\Http\Request;

class SkillsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    protected $skillRepository;
    protected $positionRepository;
    public function __construct(SkillRepositoryInterface $skillRepository, PositionRepositoryInterface $positionRepository)
    {
        $this->skillRepository = $skillRepository;
        $this->positionRepository = $positionRepository;
    }
    public function index(Request $request)
    {
        $perPage = 25;
        $skills = $this->skillRepository->getSkillList($perPage,$request);
        return view('interview.skills.index', compact('skills'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $positions = $this->positionRepository->getPositionTitles();
        return view('interview.skills.create', compact('positions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->skillRepository->storeSkill($request);
        return redirect()->route('talent.skills.index')->with('flash_message', 'Data successfully stored...!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $skill=$this->skillRepository->getSkillById($id);
        return view('interview.skills.show', compact('skill'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $positions = $this->positionRepository->getPositionTitles();
        $skill = $this->skillRepository->getSkillById($id);
        return view('interview.skills.edit', compact('skill','positions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->skillRepository->updateSkill($request, $id);
        return redirect()->route('talent.skills.index')->with('flash_message', 'Skill updated...!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function delete($id)
    {
        $this->skillRepository->deleteSkill($id);
        return redirect()->route('talent.skills.index')->with('flash_message', 'Skill deleted...!');
    }
}
