<?php

namespace App\Http\Controllers\Interview;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class QuestionsController extends Controller
{
    public function __construct()
    {
    }

    public function index(Request $request)
    {
        return view('interview.questions.index');
    }

    public function create()
    {
        return view('interview.questions.create');
    }

    public function store(Request $request)
    {

        return redirect()->route('talent.exam_questions')->with('flash_message', 'Question added successfully!');
    }


}
