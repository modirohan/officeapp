<?php

namespace App\Http\Controllers\Interview;

use App\Http\Controllers\Controller;
use App\Models\StudentExaminations;

class ExamResultsController extends Controller
{
    public function __construct()
    {
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $exam_results = StudentExaminations::with('candidateInfo')->where('exam_id',1)->get();
        return view('interview.exam-results.index',compact('exam_results'));
    }


}
