<?php

namespace App\Http\Controllers\Interview;

use App\Http\Controllers\Controller;
use App\Interfaces\Interview\PositionRepositoryInterface;
use Illuminate\Http\Request;

class PositionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    protected $positionRepository;
    public function __construct(PositionRepositoryInterface $positionRepository)
    {
        $this->positionRepository = $positionRepository;
    }
    public function index()
    {
        $perPage = 25;
        $positions = $this->positionRepository->getPositionList($perPage);
        return view('interview.positions.index', compact('positions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('interview.positions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->positionRepository->storePosition($request);
        return redirect()->route('talent.positions.index')->with('flash_message', 'Data successfully stored...!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $position =$this->positionRepository->showPosition($id);
        return view('interview.positions.show', compact('position'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $position = $this->positionRepository->getPositionById($id);
        return view('interview.positions.edit', compact('position'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->positionRepository->updatePosition($request, $id);
        return redirect()->route('talent.positions.index')->with('flash_message', 'Position updated...!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $this->positionRepository->deletePosition($id);
        return redirect()->route('talent.positions.index')->with('flash_message', 'Data successfully deleted...!');
    }
}
