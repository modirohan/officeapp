<?php

namespace App\Http\Controllers\Interview;

use App\Http\Controllers\Controller;

class StoryBoardController extends Controller
{
    public function index()
    {
        return view('interview.story_board.index');
    }
}