<?php

namespace App\Http\Controllers\Interview;

use App\Http\Controllers\Controller;
use App\Interfaces\Interview\CandidateRepositoryInterface;
use Illuminate\Http\Request;

class StudentsController extends Controller
{
    protected $candidateRepository;
    public function __construct(CandidateRepositoryInterface $candidateRepository)
    {
        $this->candidateRepository = $candidateRepository;
    }

    public function index()
    {
        return view('interview.students.create');
    }

    public function create()
    {
        return view('interview.questions.create');
    }

    public function store(Request $request)
    {
        $this->candidateRepository->storeCandidate($request);
        return redirect()->route('talent.exams')->with('flash_message', 'Successfully Stored.....');
    }


}
