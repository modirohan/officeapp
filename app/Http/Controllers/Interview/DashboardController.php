<?php

namespace App\Http\Controllers\Interview;

use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function index()
    {
        if(auth()->user()->is_interview_enabled == 1){
            return view('interview.dashboard');
        }
        return view('dashboard');
    }
}