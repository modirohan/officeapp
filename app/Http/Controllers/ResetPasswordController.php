<?php

namespace App\Http\Controllers;


use App\Interfaces\ForgotPasswordRepositoryInterface;
use App\Models\PasswordResets;
use App\Models\User;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ResetPasswordController extends Controller
{
    /*
        |--------------------------------------------------------------------------
        | Password Reset Controller
        |--------------------------------------------------------------------------
        |
        | This controller is responsible for handling password reset requests
        | and uses a simple trait to include this behavior. You're free to
        | explore this trait and override any methods you wish to tweak.
        |
        */
    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/home';
    protected $forgotPasswordRepository;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ForgotPasswordRepositoryInterface $forgotPasswordRepository)
    {
        $this->middleware('guest');
        $this->forgotPasswordRepository = $forgotPasswordRepository;
    }

    public function resetPasswordLink($token = '')
    {
        $result = $this->forgotPasswordRepository->resetPasswordLink($token);
        return view('reset-password', compact('result'));
    }

    public function resetPassword(Request $request)
    {
        $this->validate($request, [
            'password' => 'confirmed|min:6'
        ]);

        $requestData = $request->all();
        $email = $requestData['email'];
        $reset_data = array('password' => Hash::make($requestData['password']));
        $user = User::where('email', $email)->first();
        if ($user) {
            $user->update($reset_data);
            PasswordResets::where('email', $email)->delete();
        }
        return redirect()->route("home")->with('flash_message', 'Your password reset successfully. Now you can login.');

    }
}
