<?php

namespace App\Http\Controllers\Admin;

use App\Interfaces\Hrm\TimelogRepositoryInterface;
use App\Interfaces\Hrm\UserRepositoryInterface;
use App\Services\Admin\CalenderService;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TimesheetController extends Controller
{
    protected $calender;
    protected $userRepository;
    protected $timelogRepository;

    public function __construct(CalenderService $calenderService, UserRepositoryInterface $userRepository, TimelogRepositoryInterface $timelogRepository)
    {
        $this->calender = $calenderService;
        $this->userRepository = $userRepository;
        $this->timelogRepository = $timelogRepository;
    }
    public function index(Request $request)
    {
        $calendar = $this->calender->createCalender($request->user_id);
        $users = $this->userRepository->getAllUsers();
        $user_id = $request->user_id;
        return view('admin.timesheets.index', compact('calendar', 'users', 'user_id'));
    }

    public function getMonthlyCalender($start_date, $end_date, $user_id){
        $timelogs = $this->timelogRepository->getUserTimeLog([$user_id], $start_date, $end_date);
        $timelog_details = $this->timelogRepository->getTimeLogDetailsByDateRange([$user_id], $start_date, $end_date);
        $data = array_merge($timelogs,$timelog_details);
        return json_encode([
            'status' => true,
            'data' => $data
        ]);
    }
}
