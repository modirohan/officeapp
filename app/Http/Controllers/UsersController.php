<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserProfileRequest;
use App\Interfaces\UserRepositoryInterface;

class UsersController extends Controller
{
    protected $userRepository;

    public function __construct(UserRepositoryInterface $userRepository){
        $this->userRepository = $userRepository;
    }
    public function index(){
        $user = $this->userRepository->getProfile(auth()->user()->id);
        return view('profile.profile', compact('user'));
    }

    public function showList(){
        return view('employees_list');
    }

    public function edit(){
        $user = $this->userRepository->getProfile(auth()->user()->id);
        return view('profile.form', compact('user'));
    }

    public function update(UserProfileRequest $request){
        $requestData = $request->except('_token');
        if ($request->file('image')) {
            $file = $request->file('image');
            $filename_with_extension = $request->file('image')->getClientOriginalName();
            $filename = pathinfo($filename_with_extension, PATHINFO_FILENAME);
            $ext = $request->file('image')->getClientOriginalExtension();
            $filename_to_store = $filename . time() . "." . $ext;
            $file->move(storage_path('/app/public/profile_image'), $filename_to_store);
            $requestData['image'] = $filename_to_store;
        }
        $this->userRepository->updateProfile($requestData);
        return redirect()->route('user.profile')->with('success', 'Profile Updated Successfully');
    }
}
