<?php

namespace App\Http\Controllers;

use App\Interfaces\ForgotPasswordRepositoryInterface;
use App\Models\PasswordResets;
use App\Models\User;
use Illuminate\Http\Request;

class ForgotPasswordController extends Controller
{
    protected $forgotPasswordRepository;
    public function __construct(ForgotPasswordRepositoryInterface $forgotPasswordRepository)
    {
        $this->forgotPasswordRepository = $forgotPasswordRepository;
    }
    public function index()
    {
        return view('forgot-password');
    }
    public function sendResetLink(Request $request)
    {
        $requestData = $request->all();
        // VALIDATION FOR FORGOT PASSWORD
        $this->validate($request, [
            'email' => 'required|email',
        ]);

        $email = $requestData['email'];
        $token = $random = str_random(15);
        $isuserxist = 0;

        $user = User::where('email', $email)->first();

        if (isset($user) && count($user) > 0) {
            $isuserxist = 1;
        }
        if ($isuserxist == 1) {
            $reset_data = array('email' => $email, 'token' => $token, 'created_at' => date('Y-m-d'));
            $isemailexist = PasswordResets::where('email', '=', $email)->first();

            if (count($isemailexist) > 0) {
                PasswordResets::where('email', '=', $email)->update($reset_data);
            } else {
                PasswordResets::create($reset_data);
            }
            $reset_data['reset_link'] = route('resetPasswordLink', $token);
            //send mail
            $this->forgotPasswordRepository->sendResetLinkToMail($reset_data);
            return redirect()->back()->with('success','Please check your email');
        } else {
            return redirect()->back()->with('error','User does not exist.');
        }
    }
}