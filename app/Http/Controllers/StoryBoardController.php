<?php
/**
 * Created by PhpStorm.
 * User: Malay Mehta
 * Date: 08-01-2019
 * Time: 12:51 AM
 */

namespace App\Http\Controllers;


class StoryBoardController extends Controller
{
    public function index()
    {
        return view('story_board.index');
    }
    public function getCandidatePage()
    {
        return view('story_board.candidate');
    }
}