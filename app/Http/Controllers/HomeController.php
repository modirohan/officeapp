<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function login()
    {
        if (Auth::check()) {
            
            return redirect()->route('dashboard');
        }

        return view('auth.login');
    }

    public function authenticate(Request $request)
    {
       
       $remember_me = $request->has('remember-me') ? true : false; 
    
        if (Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password')],$remember_me)) {
            
            if (auth()->user()->is_admin == 1) {
                return redirect()->route('admin.dashboard');
        }
            if (auth()->user()->is_timetracking_enabled == 1) {
                return redirect()->route('hrm.dashboard');
            }
            return redirect()->route('dashboard');
        }else {
            return redirect()->back()->with('error','Invalid Credentials');
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('home');
    }
    public function test()
    {
        return view('emails.email_reset_password');
    }
}