<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Candidates extends Authenticatable
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    const COLLEGE_LIST  = ['K. S. College' => 'K. S. College', 'DDIT - Nadiad' => 'DDIT - Nadiad'];
    const COURSE_LIST  = ['BE IT' => 'BE IT', 'MSc IT'=>'MSc IT'];
    const INTERESTED_LIST  = ['web_development' => 'Web Development', 'mobile_app_development'=>'Mobile App Development', 'designing'=>'Designing'];
    protected $table = 'candidates';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'name', 'email_address', 'interested_in', 'password', 'address_line1', 'address_line2', 'city', 'state', 'country', 'enrollment_number', 'college', 'course', 'gender',
        'phone_number', 'resume', 'facebook_url', 'skype_address', 'linkedin_address', 'birth_date', 'additional_info',
        'reference_notes', 'current_salary', 'expected_salary', 'notice_period', 'immediate_joining', 'position_id', 'status_id',
        'status_title', 'is_active', 'added_by', 'created_at', 'updated_at', 'deleted_at'];

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    protected $hidden = [
        'password'
    ];

    /* MUTATOR */
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }

    public function setBirthDateAttribute($value)
    {
        $this->attributes['birth_date'] = Carbon::parse($value)->format('Y-m-d');
    }

    // Relationships
}
