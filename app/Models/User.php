<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    use SoftDeletes;
    protected $fillable = ['name', 'email', 'education', 'password', 'role_id', 'image', 'activation_code', 'activation_date', 'status', 'is_timetracking_enabled', 'is_interview_enabled', 'is_pms_enabled', 'is_admin', 'token', 'access_token', 'employee_id', 'designation', 'phone_no', 'location', 'city', 'state', 'pincode', 'current_salary', 'year_of_experience', 'joining_date', 'date_of_birth', 'gender', 'permanent_location', 'permanent_city', 'permanent_pincode', 'permanent_state'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    /*Model Constants*/
    const TIME_SHIFTS = [
        '1' => '09.00 to 06.00',
        '2' => '09.30 to 06.30'
    ];
    const IMG_URL = "storage/profile_image/";

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /* Scopes */


    /* Relationships */
    public function roleInfo()
    {
        return $this->belongsTo(Roles::class, 'role_id', 'id')->select('id', 'title');
    }
    /*Accessors*/
     public function getTimeShiftAttribute($value){
         return getTimeShift($value);
     }
}
