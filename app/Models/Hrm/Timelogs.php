<?php

namespace App\Models\Hrm;

use Illuminate\Database\Eloquent\Model;

class Timelogs extends Model
{
    protected $table = 'timelogs';

    protected $fillable = ['user_id', 'timelog_string', 'entry_date', 'entry_time', 'entry_type', 'entry_mode', 'created_at', 'updated_at', 'deleted_at'];

    const ENTRY_TYPE = [
        '1' => 'Normal',
        '2' => 'Lunch',
        '3' => 'Break'
    ];

    const ENTRY_MODE = [
        '1' => "IN",
        '2' => 'OUT'
    ];
    /*Accessors*/
    public function getEntryTypeAttribute($value){
        return getEntryType($value);
    }
    public function getEntryModeAttribute($value){
        return getEntryMode($value);
    }
}
