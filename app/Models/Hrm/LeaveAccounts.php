<?php

namespace App\Models\Hrm;

use Illuminate\Database\Eloquent\Model;

class LeaveAccounts extends Model
{
    protected $fillable = ['leave_application_id', 'user_id', 'leave_date', 'is_half_day', 'half_day_session', 'leave_type', 'is_paid_leave', 'deductable_leave_value', 'notes', 'created_at', 'deleted_at', 'updated_at'];

    /*Relations*/
    public function leave_application(){
        return $this->belongsTo('App\Models\Hrm\LeaveApplications', 'leave_application_id');
    }

    /*Accessors*/
    public function getStartDateHalfDaySessionAttribute($value){
        return getHalfaySession($value);
    }
    public function getEndDateHalfDaySessionAttribute($value){
        return getHalfaySession($value);
    }

}
