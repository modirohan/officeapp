<?php

namespace App\Models\Hrm;

use Illuminate\Database\Eloquent\Model;

class LeaveApplicationsHistory extends Model
{
    protected $fillable = ['leave_application_id', 'is_approved', 'approved_by', 'created_at', 'updated_at', 'deleted_at', 'remarks'];

    protected $table = 'leave_application_history';

    /*Relations*/

    public function leave_application()
    {
        return $this->belongsTo('App\Models\Hrm\LeaveApplications', 'leave_application_id');
    }
}
