<?php

namespace App\Models\Hrm;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LeaveApplications extends Model
{
    use SoftDeletes;

    protected $fillable = ['start_date', 'user_id', 'end_date', 'is_start_date_half_day', 'start_date_half_day_session', 'is_end_date_half_day', 'end_date_half_day_session', 'reason', 'detailed_reason', 'is_available_on_phone', 'emergency_contact_no'];

    const HALF_DAY_SESSION = [
        '1' => 'First Half',
        '2' => 'Second Half'
    ];
    const REASON = [
        '1' => 'Medical Emergency',
        '2' => 'Social Emergency',
        '3' => 'Casual'
    ];
    /*Relations*/
    public function leave_accounts(){
        return $this->hasMany('App\Models\Hrm\LeaveAccounts', 'leave_application_id');
    }

    public function leave_application_history()
    {
        return $this->hasOne('App\Models\Hrm\LeaveApplicationsHistory', 'leave_application_id');
    }

    public function user(){
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    /*Accessors*/
    public function getReasonAttribute($value){
        return getLeaveReason($value);
    }

    public function getStatusAttribute($value){
        return getLeaveStatus($value);
    }

}
