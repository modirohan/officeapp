<?php

namespace App\Models\Hrm;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CompanyHolidays extends Model
{
    use SoftDeletes;

    protected $table = 'company_leaves';

    protected $fillable = ['title', 'date', 'created_at', 'updated_at', 'deleted_at'];
}
