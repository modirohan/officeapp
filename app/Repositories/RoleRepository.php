<?php

namespace App\Repositories;

use App\Interfaces\RoleRepositoryInterface;
use App\Models\Roles;


class RoleRepository implements RoleRepositoryInterface
{
    public function getRolesTitles(){
        return Roles::pluck('title', 'id')->toArray();
    }
}
