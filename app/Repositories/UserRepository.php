<?php

namespace App\Repositories;

use App\Interfaces\UserRepositoryInterface;
use App\Models\User;

class UserRepository implements UserRepositoryInterface
{
    public function updateProfile($data)
    {
        if (!isset($data['change_password'])) {
            unset($data['password']);
        }else{
            unset($data['change_password']);
            $data['password'] = bcrypt($data['password']);
        }
        return User::where('id', auth()->user()->id)->update($data);
    }

    public function getProfile($user_id)
    {
        return User::find($user_id);
    }
}
