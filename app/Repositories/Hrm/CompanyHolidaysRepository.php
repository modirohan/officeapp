<?php

namespace App\Repositories\Hrm;

use App\Interfaces\Hrm\CompanyHolidaysRepositoryInterface;
use App\Models\Hrm\CompanyHolidays;
use Carbon\Carbon;


class CompanyHolidaysRepository implements CompanyHolidaysRepositoryInterface
{

    public function getAllHolidays()
    {
        return CompanyHolidays::all();
    }

    public function getCurrentYearHolidays()
    {
        return CompanyHolidays::whereYear('date', date("Y"))->get();
    }

    public function getCurrentMonthHolidays()
    {
        return CompanyHolidays::whereMonth('date', date("m"))->whereYear('date', date("Y"))->get();
    }

    public function getHolidaysByDate($start_date, $end_date)
    {
        $start_date = Carbon::parse($start_date);
        $end_date = Carbon::parse($end_date);
        $company_leaves = CompanyHolidays::where('date', '>=', $start_date)->where('date', '<=', $end_date)->get()->toArray();
        $leaves_data = [];
        foreach ($company_leaves as $leave){
            $leaves_data[] = ['title' => $leave['title'],'start' => $leave['date'], 'color' => 'green', 'textColor'=> 'white'];
        }
        return $leaves_data;
    }
}
