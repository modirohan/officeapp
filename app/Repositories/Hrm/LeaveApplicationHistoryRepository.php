<?php

namespace App\Repositories\Hrm;



use App\Interfaces\Hrm\LeaveApplicationHistoryRepositoryInterface;
use App\Models\Hrm\LeaveApplicationsHistory;

class LeaveApplicationHistoryRepository implements LeaveApplicationHistoryRepositoryInterface
{


    public function storeLeaveApplicationHistory($data)
    {
        LeaveApplicationsHistory::create([
            'leave_application_id' => $data['leave_application_id'],
        ]);
    }
}
