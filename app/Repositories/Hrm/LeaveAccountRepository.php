<?php

namespace App\Repositories\Hrm;

use App\Interfaces\Hrm\CompanyHolidaysRepositoryInterface;
use App\Interfaces\Hrm\LeavesAccountRepositoryInterface;
use App\Models\Hrm\LeaveAccounts;
use Carbon\CarbonPeriod;

class LeaveAccountRepository implements LeavesAccountRepositoryInterface
{
    protected $companyHolidaysRepository;

    public function __construct(CompanyHolidaysRepositoryInterface $companyHolidaysRepository){
        $this->companyHolidaysRepository = $companyHolidaysRepository;
    }
    public function storeLeave($data)
    {
        if(isset($data['end_date'])){
            $period = CarbonPeriod::create($data['start_date'], $data['end_date'])->toArray();
            $company_holidays = $this->companyHolidaysRepository->getHolidaysByDate($data['start_date'], $data['end_date']);
            $company_holidays_date = [];
            foreach($company_holidays as $holiday){
                array_push($company_holidays_date, getCarbonDate($holiday['start']));
            }
            $period = array_values(array_diff($period, $company_holidays_date));
        }else{
            $period = [getCarbonDate($data['start_date'])];
        }
        $leaves_data = [];
        $leaves_balance = (float)auth()->user()->leaves_balance;
        if(count($period) == 1 && !$period[0]->isWeekend()){
            if(isset($data['start_date_half_day_session']) && $data['is_start_date_half_day'] == 1 ){
                $deductable_data = calculateDeductableValueAndPaidLeaveStatusForHalfSession($leaves_balance);
                $leaves_balance-=0.5;
            }else{
                $deductable_data = calculateDeductableValueAndPaidLeaveStatusForFullSession($leaves_balance);
                $leaves_balance-=1;
            }
            $leaves_data[] = [
                'leave_application_id' => $data['leave_application_id'],
                'user_id' => auth()->user()->id,
                'leave_date' => $period[0]->toDateString(),
                'is_half_day' => $deductable_data['half_day'],
                'half_day_session' =>(isset($data['start_date_half_day_session']))?$data['start_date_half_day_session']:null,
                'leave_type' => 'debit',
                'deductable_leave_value' => $deductable_data['deductable_value'],
                'is_paid_leave' => $deductable_data['is_paid_leave']
            ];
        }else{
            foreach($period as $key => $date){
                if($date->isWeekend()) continue;
                //Check for the first iteration
                if(reset($period) == $date){
                    if(isset($data['start_date_half_day_session']) && isset($data['is_start_date_half_day']) && $data['is_start_date_half_day'] == 1 && $data['start_date_half_day_session'] == 2){
                        $deductable_data = calculateDeductableValueAndPaidLeaveStatusForHalfSession($leaves_balance);
                        $leaves_balance-=0.5;
                    }else{
                        $deductable_data = calculateDeductableValueAndPaidLeaveStatusForFullSession($leaves_balance);
                        $leaves_balance-=1;
                    }
                    $leaves_data[] = [
                        'leave_application_id' => $data['leave_application_id'],
                        'user_id' => auth()->user()->id,
                        'leave_date' => $date->toDateString(),
                        'is_half_day' => $deductable_data['half_day'],
                        'half_day_session' =>(isset($data['start_date_half_day_session']) && $data['start_date_half_day_session'] == 2)?$data['start_date_half_day_session']:null,
                        'leave_type' => 'debit',
                        'deductable_leave_value' => $deductable_data['deductable_value'],
                        'is_paid_leave' => $deductable_data['is_paid_leave']
                    ];
                    continue;
                }
                //Check for the last iteration
                if(count($period)>1 && ($key == count($period)-1) ){
                    if(isset($data['end_date_half_day_session']) && isset($data['is_end_date_half_day']) && $data['is_end_date_half_day'] == 1 && $data['end_date_half_day_session'] == 1){
                        $deductable_data = calculateDeductableValueAndPaidLeaveStatusForHalfSession($leaves_balance);
                        $leaves_balance-=0.5;
                    }else{
                        $deductable_data = calculateDeductableValueAndPaidLeaveStatusForFullSession($leaves_balance);
                        $leaves_balance-=1;
                    }
                    $leaves_data[] = [
                        'leave_application_id' => $data['leave_application_id'],
                        'user_id' => auth()->user()->id,
                        'leave_date' => $date->toDateString(),
                        'is_half_day' => $deductable_data['half_day'],
                        'half_day_session' =>(isset($data['end_date_half_day_session']) && $data['end_date_half_day_session'] == 1)?$data['end_date_half_day_session']:null,
                        'leave_type' => 'debit',
                        'deductable_leave_value' => $deductable_data['deductable_value'],
                        'is_paid_leave' => $deductable_data['is_paid_leave']
                    ];
                    continue;
                }
                $leaves_data[] = [
                    'leave_application_id' => $data['leave_application_id'],
                    'user_id' => auth()->user()->id,
                    'leave_date' => $date->toDateString(),
                    'is_half_day' => 0,
                    'half_day_session' => null,
                    'leave_type' => 'debit',
                    'deductable_leave_value' => ($leaves_balance >= 1) ? 0 : 1,
                    'is_paid_leave' => ($leaves_balance >= 1) ? 1 : 0
                ];
                $leaves_balance-=1;
            }
        }
        return LeaveAccounts::insert($leaves_data);
    }
}
