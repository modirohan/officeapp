<?php

namespace App\Repositories\Hrm;

use App\Interfaces\Hrm\UserRepositoryInterface;
use App\Models\User;
use Illuminate\Support\Carbon;

class UserRepository implements UserRepositoryInterface
{


    public function getAllUsers()
    {
        return User::where('is_timetracking_enabled', 1)->orderBy('name')->get();
    }

    public function getUsersByCurrentMonthBirthday()
    {
        return User::whereMonth('date_of_birth', date("m"))->get();
    }

    public function getUsersHavingBirthdaysByDate($start_date, $end_date){
        $current_month = Carbon::parse($start_date)->format('m');
        $users = User::whereMonth('date_of_birth', $current_month)->get()->toArray();
        $birthdays_data = [];
        foreach($users as $user){
            $birthdays_data[] = [
                    'title' => $user['name'] . "'s Birthday ",
                    'start' => date('Y').getCarbonDate($user['date_of_birth'])->format('-m-d'),
                    'color' => '#2097c8',
                    'textColor' => '#fff'
                ];
        }
        return $birthdays_data;
    }
}
