<?php

namespace App\Repositories\Hrm;



use App\Interfaces\Hrm\LeaveApplicationRepositoryInterface;
use App\Models\Hrm\LeaveApplications;

class LeaveApplicationRepository implements LeaveApplicationRepositoryInterface
{

    public function storeLeaveApplication($data)
    {
        $data['user_id'] = auth()->user()->id;
        $unapproved_leaves = LeaveApplications::where('status', 0)->where('user_id', $data['user_id'])->first();
        if($unapproved_leaves){
            return false;
        }
        return LeaveApplications::create($data);
    }

    public function getLeaveApplication()
    {
        return LeaveApplications::whereUserId(auth()->user()->id)->get();
    }

    public function getLeaveApplicationByDateRange($start_date, $end_date)
    {
        $leave_applications = LeaveApplications::where('start_date', '>=', $start_date)->where('start_date', '<=', $end_date)->get();
        $leaves_data = [];
        foreach($leave_applications as $leave_application){
            $end_date = ($leave_application->end_date) ? getCarbonDate($leave_application->end_date)->addDays(1)->toDateString() : $leave_application->start_date;
            $leaves_data[] = [
                'title' => $leave_application->user->name . " is on leave ",
                'start' => $leave_application->start_date,
                'end' => $end_date,
                'color' => '#ac3b61d9',
                'textColor' => '#fff'
            ];
        }
        return $leaves_data;
    }
}
