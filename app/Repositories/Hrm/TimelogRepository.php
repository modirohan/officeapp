<?php

namespace App\Repositories\Hrm;

use App\Interfaces\Hrm\TimelogRepositoryInterface;
use App\Models\Hrm\Timelogs;
use Carbon\CarbonPeriod;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class TimelogRepository implements TimelogRepositoryInterface
{

    public function storeTimelog(Request $request)
    {
        $requestData = $request->all();
        $requestData['user_id'] = auth()->user()->id;
        $requestData['entry_time'] = Carbon::now()->toTimeString();
        $requestData['entry_date'] = Carbon::now()->toDateString();
        $requestData['timelog_string'] = Carbon::now()->format('g:i A'). ' ' . getEntryType($requestData['entry_type']) . ' ' . getEntryMode($requestData['entry_mode']);
        Timelogs::create($requestData);
        return true;
    }
    public function getUserTimeLog($user_ids = [], $start_date = null, $end_date = null)
    {
        $start_date = Carbon::parse($start_date);
        $end_date = Carbon::parse($end_date);
        $timelogs = Timelogs::whereIn('user_id', $user_ids)->where('entry_date', '>=', $start_date)->where('entry_date', '<=', $end_date)->get()->toArray();
        $timelogs_data = [];
        foreach($timelogs as $timelog){
            $timelogs_data[] = [
                'title' => $timelog['timelog_string'],
                'start' => $timelog['entry_date'],
                'color' => 'yellow',
                'textColor' => 'black'
            ];
        }
        return $timelogs_data;
    }
    public function getTodaysTimeLog()
    {
        return Timelogs::whereDay('entry_date', date("d"))->where('user_id', auth()->user()->id)->get();
    }

    public function getTodaysTimeLogDetails()
    {
        $current_user_id = auth()->user()->id;
        $timesheet_data = Timelogs::whereDay('entry_date', date("d"))->select("entry_time", "entry_type", "entry_mode")->where('user_id', $current_user_id)->orderBy("entry_time", "ASC")->get()->groupBy('entry_type')->toArray();
        $data = [];
        $timesheet_info = [];
        $timesheet_info['Actual Working Hours'] = 0;
        foreach ($timesheet_data as $timesheet) {
            $data[$timesheet[0]['entry_type']] = calculateTimeByEntryType($timesheet);
        }
        $data['Normal'] = (isset($data['Normal'])) ? ($data['Normal']) : 0;
        $data['Break'] = (isset($data['Break'])) ? ($data['Break']) : 0;
        $data['Lunch'] = (isset($data['Lunch'])) ? ($data['Lunch']) : 0;
        $timesheet_info['Actual Working Hours'] = calculateSecondsToMinutesAndHours($data['Normal'] - $data['Lunch'] - $data['Break']);
        $timesheet_info['Total Working Hours'] = calculateSecondsToMinutesAndHours($data['Normal']);
        $timesheet_info['Total Break Hours'] = calculateSecondsToMinutesAndHours($data['Break']);
        $timesheet_info['Total Lunch Hours'] = calculateSecondsToMinutesAndHours($data['Lunch']);
        return $timesheet_info;
    }

    public function getSuitableEntryTypeAndEntryMode(){
        $timelog = Timelogs::where('user_id', auth()->user()->id)->whereDay('entry_date', date("d"))->orderBy('entry_time', 'desc')->first();
        if(isset($timelog)){
            if($timelog->getOriginal('entry_mode') == 2){
                return ['entry_mode' => ['1' => 'IN'], 'entry_type' => [$timelog->getOriginal('entry_type') => $timelog->entry_type]];
            }elseif(($timelog->getOriginal('entry_mode') == 1 && $timelog->getOriginal('entry_type') == 1 )){
                return ['entry_mode' => ['2' => 'OUT'], 'entry_type' => ['2' => 'Lunch', '1' => 'Normal']];
            }
            else{
                return ['entry_mode' => ['2' => 'OUT'], 'entry_type' => ['1' => 'Normal', '3' => 'Break']];
            }
        }else{
            return ['entry_mode' => ['1' => 'IN'], 'entry_type' => ['1' => 'Normal']];
        }
    }

    public function getTimeLogDetailsByDateRange($user_id = [], $start_date = null, $end_date = null)
    {
        $user_id = $user_id[0];
        $period = CarbonPeriod::create($start_date, $end_date)->toArray();
        $timelog_details = [];
        foreach($period as $date){
            $carbon_date = getCarbonDate($date);
            $timesheet_data = Timelogs::where('entry_date', $carbon_date)->select("entry_time", "entry_type", "entry_mode")->where('user_id', $user_id)->orderBy("entry_time", "ASC")->get()->groupBy('entry_type')->toArray();
            $data = [];
            $timesheet_info = [];
            $timesheet_info['actual_working_hours'] = 0;
            foreach ($timesheet_data as $timesheet) {
                $data[$timesheet[0]['entry_type']] = calculateTimeByEntryType($timesheet);
            }
            $data['Normal'] = (isset($data['Normal'])) ? ($data['Normal']) : 0;
            $data['Break'] = (isset($data['Break'])) ? ($data['Break']) : 0;
            $data['Lunch'] = (isset($data['Lunch'])) ? ($data['Lunch']) : 0;
            if(empty(array_filter($data))) continue;
            $timesheet_info['actual_working_hours'] = calculateSecondsToMinutesAndHours($data['Normal'] - $data['Lunch'] - $data['Break']);
            $timesheet_info['total_working_hours'] = calculateSecondsToMinutesAndHours($data['Normal']);
            $timesheet_info['total_break_hours'] = calculateSecondsToMinutesAndHours($data['Break']);
            $timesheet_info['total_lunch_hours'] = calculateSecondsToMinutesAndHours($data['Lunch']);
            $timelog_details[] = [
                'title' => "Actual Working Hours: " . $timesheet_info['actual_working_hours'] . "\nTotal Working Hours: " . $timesheet_info['total_working_hours'] . "\nTotal Break Hours: " . $timesheet_info['total_break_hours'] . "\nTotal Lunch Hours: " . $timesheet_info['total_lunch_hours'],
                'start' => $carbon_date->toDateString(),
                'color' => 'lightgreen',
                'textColor' => 'black'
           ];
        }
        return $timelog_details;
    }
}
