<?php

namespace App\Repositories;

use App\Interfaces\ForgotPasswordRepositoryInterface;
use App\Mail\ForgotPasswordResetLinkMail;
use App\Models\PasswordResets;
use Illuminate\Support\Facades\Mail;


class ForgotPasswordRepository implements ForgotPasswordRepositoryInterface
{
    public function resetPasswordLink($token = ''){
        $check_valid_token = PasswordResets::where('token', $token)->first();
        $result = array("success" => true);
        if (count($check_valid_token) > 0) {
            $result['email'] = $check_valid_token->email;
            $result['success'] = true;
        } else {
            $result['success'] = false;
        }
        return $result;
    }
    public function sendResetLinkToMail($reset_data){
        Mail::to($reset_data['email'])->send(new ForgotPasswordResetLinkMail($reset_data));
        return true;
    }
}
