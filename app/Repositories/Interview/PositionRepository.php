<?php

namespace App\Repositories\Interview;

use App\Interfaces\Interview\PositionRepositoryInterface;
use App\Models\Positions;
use Illuminate\Http\Request;

class PositionRepository implements PositionRepositoryInterface
{
    public function getPositionList($perPage)
    {
        $positions = Positions::latest()->paginate($perPage);
        return $positions;
    }
    public function storePosition(Request $request)
    {
        $requestData = $request->except('_token');
        $positions= Positions::create($requestData);
        return $positions;
    }
    public function getPositionById($id)
    {
        return $positions = Positions::findOrFail($id);
    }
    public function updatePosition(Request $request, $id)
    {
        $requestData = $request->all();
        $positions = Positions::findOrFail($id);
        $positions->update($requestData);
        return $positions;
    }
    public function deletePosition($id)
    {
        $positions = Positions::findOrFail($id);
        $positions->delete();
        return $positions;
    }
    public function showPosition($id)
    {
        return $user = Positions::findOrFail($id);
    }
    public function getPositionTitles()
    {
        return Positions::pluck('title', 'id')->toArray();
    }
/*

*/
}
