<?php

namespace App\Repositories\Interview;

use App\Interfaces\Interview\InterviewRepositoryInterface;
use App\Models\Candidates;
use App\Models\Interviews;
use App\Models\Skills;
use Illuminate\Http\Request;


class InterviewRepository implements InterviewRepositoryInterface
{
    public function getAllInterviews(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;
        if (!empty($keyword)) {
            $interviews = Interviews::where('title', 'LIKE', "%$keyword%")
                ->orWhere('description', 'LIKE', "%$keyword%")
                ->orWhere('openings_count', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $interviews = Interviews::latest()->paginate($perPage);
        }
        return $interviews;
    }
    public function storeInterview(Request $request)
    {
        $requestData = $request->all();
        $interview = Interviews::create($requestData);
        return $interview;
    }
    public function getInterviewById($id)
    {
        return $interview = Interviews::findOrFail($id);
    }
    public function updateInterview(Request $request, $id)
    {
        $requestData = $request->all();
        $interview = Interviews::findOrFail($id);
        $interview->update($requestData);
        return $interview;
    }
    public function deleteInterview($id)
    {
        $interview = Interviews::findOrFail($id);
        $interview->delete();
        return $interview;
    }
}
