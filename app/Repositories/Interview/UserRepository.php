<?php

namespace App\Repositories\Interview;

use App\Interfaces\Interview\UserRepositoryInterface;
use App\Mail\NewUserActivation;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

//use Your Model

/**
 * Class StudentRepository.
 */
class UserRepository implements UserRepositoryInterface
{
    /**
     * @return string
     *  Return the model
     */
    public function getUserList($perPage,Request $request)
    {

        $keyword = $request->get('search');

        if (!empty($keyword)) {
            $user_data = User::where('name', 'LIKE', "%$keyword%")
                ->orWhere('email', 'LIKE', "%$keyword%")
                ->orWhere('password', 'LIKE', "%$keyword%")
                ->orWhere('role_id', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $user_data = User::with('roleInfo')->latest()->paginate($perPage);
        }
        return $user_data;
    }
    public function storeUser(Request $request)
    {
        $requestData = $request->all();
        $requestData['activation_token'] = $random = str_random(15);
        $requestData['activation_code'] = $requestData['activation_token'];
        $user_data = User::create($requestData);
        Mail::to($requestData['email'])->send(new NewUserActivation($user_data));
        return $user_data;
    }
    public function getUserById($id)
    {
        return $user = User::findOrFail($id);
    }
    public function updateUser(Request $request, $id)
    {
        $requestData = $request->all();
        $user = User::findOrFail($id);
        $user->update($requestData);
        return $user;
    }
    public function deleteUser($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        return $user;
    }
    public function showUser($id)
    {
        return $user = User::with('roleInfo')->findOrFail($id);
    }
    public function validToken($token)
    {
        return User::where('activation_code', $token)->first();
    }
    public function updateUserDetails(Request $request)
    {
        $requestData = $request->all();
        $email = $requestData['email'];
        $user = User::where('email', $email)->first();
        if ($user) {
            $user->update([
                'status' => 1,
                'activation_code' => null,
                'activation_date' => Carbon::now(),
                'password' => Hash::make($requestData['password'])
            ]);
        }
    }
}
