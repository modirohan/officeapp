<?php

namespace App\Repositories\Interview;

use App\Interfaces\Interview\CandidateRepositoryInterface;
use App\Models\Candidates;
use App\Models\Skills;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CandidateRepository implements CandidateRepositoryInterface
{
    public function getAllCandidate(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;
        if (!empty($keyword)) {
            $candidates = Candidates::where('title', 'LIKE', "%$keyword%")
                ->orWhere('description', 'LIKE', "%$keyword%")
                ->orWhere('openings_count', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $candidates = Candidates::latest()->paginate($perPage);
        }
        return $candidates;
    }
    public function storeCandidate(Request $request)
    {
        $requestData = $request->all();
        $requestData['password'] = '123456';
        $candidate = Candidates::create($requestData);
        Auth::guard('candidates')->loginUsingId($candidate->id);
        return $candidate;
    }
    public function getCandidateById($id)
    {
        return $user = Candidates::findOrFail($id);
    }
    public function updateCandidate(Request $request, $id)
    {
        $requestData = $request->all();
        $candidate = Candidates::findOrFail($id);
        $candidate->update($requestData);
        return $candidate;
    }
    public function deleteCandidate($id)
    {
        $candidate = Candidates::findOrFail($id);
        $candidate->delete();
        return $candidate;
    }
}
