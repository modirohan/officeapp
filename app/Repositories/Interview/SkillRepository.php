<?php

namespace App\Repositories\Interview;

use App\Interfaces\Interview\SkillRepositoryInterface;
use App\Models\Skills;
use Illuminate\Http\Request;

class SkillRepository implements SkillRepositoryInterface
{
    public function getSkillList($perPage, Request $request)
    {
        $keyword = $request->get('search');
        if (!empty($keyword)) {
            $skill_data = Skills::where('position_id', 'LIKE', "%$keyword%")
                ->orWhere('title', 'LIKE', "%$keyword%")
                ->orWhere('is_active', 'LIKE', "%$keyword%")
                ->orWhere('added_by', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $skill_data = Skills::with('positionInfo')->latest()->paginate($perPage);
        }
        return $skill_data;
    }
    public function storeSkill(Request $request)
    {
        $requestData = $request->except('_token');
        $requestData['is_active'] = isset($requestData['is_active']) ? 1 : 0;
        $skill_data = Skills::create($requestData);
        return $skill_data;
    }
    public function getSkillById($id)
    {
        return $user = Skills::findOrFail($id);
    }
    public function updateSkill(Request $request, $id)
    {
        $skill = Skills::findOrFail($id);
        $requestData = $request->all();
        $requestData['is_active'] = isset($requestData['is_active']) ? 1 : 0;
        $skill->update($requestData);
        return $skill;
    }
    public function deleteSkill($id)
    {
        $skill = Skills::findOrFail($id);
        $skill->delete();
        return $skill;
    }
}
