<?php

namespace App\Services\Admin;

use Calendar;

class CalenderService
{
    public function createCalender($user_id){
        if(isset($user_id)){
            return Calendar::addEvents([])->setOptions(['firstDay' => 1])->setOptions([ //set fullcalendar options
                'firstDay' => 1
            ])->setCallbacks([ //set fullcalendar callback options (will not be JSON encoded)
                'viewRender' => 'function(event,element) {       
                let user_id = '.$user_id.'
                let $calender = $(".fc").fullCalendar("getDate");         
                let numeric_year = moment($calender).format("YYYY");
                let month = moment($calender).format("MM");
                let start_date = `01-${month}-${numeric_year}`;
                let last_date = new Date(numeric_year, month, 0).getDate();
                let end_date = `${last_date}-${month}-${numeric_year}`;               
                $.ajax({
                    url:`/admin/timesheet/${start_date}/${end_date}/${user_id}`,  
                    method:"get"
                })
                .done(function(msg){
                let data = JSON.parse(msg);
                for(let prop in data.data){
                    $(".fc").fullCalendar("renderEvent", { title: `${data.data[prop].title}`, start: data.data[prop].start, end: data.data[prop].end, color:data.data[prop].color, textColor:data.data[prop].textColor }); 
                }               
                });
            }',
                'header' => '{left: "prev, next", center:"title", right:"month"}',
                'eventLimit' => true,
            ]);
        }
        return Calendar::addEvents([])->setOptions(['firstDay' => 1]);
    }
}