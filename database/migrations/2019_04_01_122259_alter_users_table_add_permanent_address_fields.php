<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUsersTableAddPermanentAddressFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->text('permanent_location')->nullable()->after('pincode');
            $table->string('permanent_state')->nullable()->after('permanent_location');
            $table->string('permanent_pincode')->nullable()->after('permanent_state');
            $table->string('permanent_city')->nullable()->after('permanent_pincode');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('permanent_location');
            $table->dropColumn('permanent_state');
            $table->dropColumn('permanent_city');
            $table->dropColumn('permanent_pincode');
        });
    }
}
