<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeaveApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leave_applications', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id');
            $table->date('start_date');
            $table->date('end_date')->nullable();
            $table->boolean('is_start_date_half_day')->default(0);
            $table->string('start_date_half_day_session')->nullable();
            $table->boolean('is_end_date_half_day')->default(0);
            $table->string('end_date_half_day_session')->nullable();
            $table->string('reason')->nullable();
            $table->text('detailed_reason')->nullable();
            $table->boolean('is_available_on_phone')->default(0);
            $table->string('emergency_contact_no')->nullable();
            $table->string('status')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leave_applications');
    }
}
