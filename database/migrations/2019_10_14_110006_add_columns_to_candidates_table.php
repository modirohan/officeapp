<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToCandidatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('candidates', function (Blueprint $table) {
            $table->string('enrollment_number')->nullable()->after('phone_number');
            $table->string('college')->nullable()->after('enrollment_number');
            $table->string('course')->nullable()->after('college');
            $table->string('gender')->nullable()->after('course');
            $table->string('interested_in')->nullable()->after('gender');
            $table->string('password')->nullable()->after('email_address');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('candidates', function (Blueprint $table) {
            $table->dropColumn('enrollment_number');
            $table->dropColumn('college');
            $table->dropColumn('course');
            $table->dropColumn('gender');
            $table->dropColumn('interested_in');
            $table->dropColumn('password');
        });
    }
}
