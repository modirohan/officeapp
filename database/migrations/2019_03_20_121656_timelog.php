<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Timelog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('timelogs');
        Schema::create('timelogs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id');
            $table->date('entry_date');
            $table->time('entry_time');
            $table->string('entry_mode');
            $table->string('entry_type');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('timelogs');
    }
}
