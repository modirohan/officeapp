<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCandidateEmploymentInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('candidate_employment_information', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('candidate_id');
            $table->string('organization')->nullable();
            $table->string('position')->nullable();
            $table->string('total_experience')->nullable();
            $table->string('relevant_experience')->nullable();
            $table->string('currency')->nullable();
            $table->string('current_salary')->nullable();
            $table->string('expected_salary')->nullable();
            $table->string('notice_period')->nullable();
            $table->string('employment_status')->nullable();
            $table->string('work_period')->nullable();
            $table->integer('added_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('candidates_emplyment_info');
    }
}
