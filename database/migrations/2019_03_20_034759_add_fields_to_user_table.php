<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->boolean('is_timetracking_enabled')->default(0)->after('activation_date');
            $table->boolean('is_interview_enabled')->default(0)->after('is_timetracking_enabled');
            $table->boolean('is_pms_enabled')->default(0)->after('is_interview_enabled');
            $table->boolean('is_admin')->default(0)->after('is_pms_enabled');
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('is_timetracking_enabled');
            $table->dropColumn('is_interview_enabled');
            $table->dropColumn('is_pms_enabled');
            $table->dropColumn('is_admin');
        });
    }
}
