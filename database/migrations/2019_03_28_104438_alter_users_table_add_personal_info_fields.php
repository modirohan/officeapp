<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUsersTableAddPersonalInfoFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('employee_id')->nullable()->after('date_of_birth');
            $table->string('designation')->nullable()->after('employee_id');
            $table->string('phone_no')->nullable()->after('designation');
            $table->text('location')->nullable()->after('phone_no');
            $table->string('city')->nullable()->after('location');
            $table->string('state')->nullable()->after('city');
            $table->string('pincode')->nullable()->after('state');
            $table->string('current_salary')->nullable()->after('pincode');
            $table->string('year_of_experience')->nullable()->after('current_salary');
            $table->string('joining_date')->nullable()->after('year_of_experience');
            $table->string('gender')->nullable()->after('joining_date');
            $table->string('time_shift')->nullable()->after('gender');
            $table->string('education')->nullable()->after('time_shift');
            $table->string('image')->nullable()->after('education');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('employee_id');
            $table->dropColumn('designation');
            $table->dropColumn('phone_no');
            $table->dropColumn('location');
            $table->dropColumn('city');
            $table->dropColumn('state');
            $table->dropColumn('pincode');
            $table->dropColumn('current_salary');
            $table->dropColumn('year_of_experience');
            $table->dropColumn('joining_date');
            $table->dropColumn('gender');
            $table->dropColumn('time_shift');
            $table->dropColumn('education');
            $table->dropColumn('image');
        });
    }
}
