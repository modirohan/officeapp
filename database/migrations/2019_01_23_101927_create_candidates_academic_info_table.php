<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCandidatesAcademicInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('candidate_education_information', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('candidate_id');
            $table->string('degree')->nullable();
            $table->string('degree_information')->nullable();
            $table->string('university')->nullable();
            $table->string('college')->nullable();
            $table->string('academic_year')->nullable();
            $table->integer('added_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('candidates_academic_info');
    }
}
