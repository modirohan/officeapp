<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeaveAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leave_accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('leave_application_id')->nullable();
            $table->string('user_id');
            $table->string('leave_date');
            $table->boolean('is_half_day');
            $table->string('half_day_session')->nullable();
            $table->enum('leave_type', ['credit', 'debit']);
            $table->boolean('is_paid_leave')->default(0);
            $table->string('deductable_leave_value');
            $table->text('notes')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leave_accounts');
    }
}
