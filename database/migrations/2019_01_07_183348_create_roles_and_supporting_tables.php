<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRolesAndSupportingTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->timestamps();
            $table->softDeletes();
        });

        // Creating the candidates table
        Schema::create('candidates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email_address')->nullable();
            $table->string('address_line1')->nullable();
            $table->string('address_line2')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('country')->nullable();
            $table->string('phone_number')->nullable();
            $table->string('resume')->nullable();
            $table->string('facebook_url')->nullable();
            $table->string('skype_address')->nullable();
            $table->string('linkedin_address')->nullable();
            $table->date('birth_date')->nullable();
            $table->text('additional_info')->nullable();
            $table->text('reference_notes')->nullable();
            $table->string('current_salary')->nullable();
            $table->string('expected_salary')->nullable();
            $table->string('notice_period')->nullable();
            $table->boolean('immediate_joining')->nullable();
            $table->integer('position_id')->nullable();
            $table->integer('status_id')->nullable();
            $table->string('status_title')->nullable();
            $table->boolean('is_active')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        // Interviews
        Schema::create('interviews', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('candidate_id');
            $table->integer('interviewer_id');
            $table->dateTime('scheduled_at')->nullable();
            $table->text('notes')->nullable();
            $table->integer('status')->nullable();
            $table->string('mode')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        // Candidate Positions
        Schema::create('positions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->text('description')->nullable();
            $table->integer('openings_count')->nullable();
            $table->boolean('is_active')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        // Candidate Notes
        Schema::create('candidate_notes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('candidate_id');
            $table->text('notes')->nullable();
            $table->integer('status')->nullable();
            $table->integer('added_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        // Master Skills Table
        Schema::create('skills', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('position_id');
            $table->string('title')->nullable();
            $table->boolean('is_active')->nullable();
            $table->integer('added_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        // Candidate Skills
        Schema::create('candidate_skills', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('candidate_id');
            $table->integer('skill_id');
            $table->integer('added_by');
            $table->timestamps();
            $table->softDeletes();
        });

        // Interview Feedback
        Schema::create('interview_feedback', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('candidate_id');
            $table->integer('interview_id');
            $table->text('details')->nullable();
            $table->integer('interview_status')->nullable();
            $table->integer('candidate_status')->nullable();
            $table->integer('added_by');
            $table->timestamps();
            $table->softDeletes();
        });

        // Interview Skill Ratings
        Schema::create('interview_skill_ratings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('candidate_id');
            $table->integer('interview_id');
            $table->integer('skill_id');
            $table->float('ratings')->nullable();
            $table->text('details')->nullable();
            $table->integer('added_by');
            $table->timestamps();
            $table->softDeletes();
        });

        // Candidate Status
        Schema::create('candidate_statuses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('title');
            $table->integer('added_by');
            $table->boolean('is_active');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('interview_statuses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('title');
            $table->integer('added_by');
            $table->boolean('is_active');
            $table->timestamps();
            $table->softDeletes();
        });

        // Candidate Interview Mappings
        Schema::create('mappings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('candidate_status_id');
            $table->integer('interview_status_id');
            $table->integer('added_by');
            $table->timestamps();
            $table->softDeletes();
        });

        // Email Templates
        Schema::create('emails', function (Blueprint $table) {
            $table->increments('id');
            $table->text('title');
            $table->text('subject');
            $table->longText('message');
            $table->string('action')->nullable();
            $table->integer('added_by');
            $table->timestamps();
            $table->softDeletes();
        });

    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
