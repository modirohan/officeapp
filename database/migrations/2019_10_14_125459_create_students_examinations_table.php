<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsExaminationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students_examinations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('student_id');
            $table->integer('exam_id')->nullable();
            $table->integer('correct_answers')->nullable();
            $table->integer('total_questions')->nullable();
            $table->dateTime('start_time')->nullable();
            $table->dateTime('end_time')->nullable();
            $table->dateTime('actual_end_time')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('examinations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('college_id');
            $table->text('college_name')->nullable();
            $table->dateTime('exam_date');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students_examinations');
    }
}
