<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(DefaultDataSeeder::class);
        $this->call(CompanyLeavesTableSeeder::class);
        $this->call(InsertSampleTimeLogSeeder::class);
    }
}
