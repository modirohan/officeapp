<?php

use Illuminate\Database\Seeder;

class CompanyLeavesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    protected $leaves = [
        ['title' => 'Mahashivratri', 'date' => '2019-03-04'],
        ['title' => 'Holi', 'date' => '2019-03-21'],
        ['title' => 'Rath Yatra', 'date' => '2019-07-04'],
        ['title' => 'Raksha Bandhan', 'date' => '2019-07-15'],
        ['title' => 'Janmashtami', 'date' => '2019-08-24'],
        ['title' => 'Dussehra', 'date' => '2021-10-15'],
        ['title' => 'Diwali', 'date' => '2019-10-27'],
        ['title' => 'Hindu New Year Day', 'date' => '2019-10-28'],
        ['title' => 'BhaiDuj', 'date' => '2019-10-29'],
        ['title' => 'Christmas', 'date' => '2019-12-25'],
    ];

    public function run()
    {
        DB::table('company_leaves')->truncate();
        DB::table('company_leaves')->insert($this->leaves);
    }
}
