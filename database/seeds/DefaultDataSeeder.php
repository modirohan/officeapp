<?php

use Illuminate\Database\Seeder;

class DefaultDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Roles Table
        DB::table('roles')->truncate();
        DB::table('roles')->insert([
            'title' => 'Admin',
        ]);

        DB::table('roles')->insert([
            'title' => 'Interviewer',
        ]);

        // Users Table
        DB::table('users')->truncate();
        DB::table('users')->insert([
            'name' => 'Malay Mehta',
            'email' => 'meetmalay@gmail.com',
            'password' => bcrypt('urvam87826'),
            'token' => '123456',
            'role_id' => '1',
            'is_admin' => '1'
        ]);

        DB::table('users')->insert([
            'name' => 'Ujjwal Goyal',
            'email' => 'ujjwal.urvam@gmail.com',
            'password' => bcrypt('123456'),
            'token' => str_random(16),
            'role_id' => '1',
            'is_timetracking_enabled' => 1
        ]);
        DB::table('users')->insert([
            'name' => 'Jinkal',
            'email' => 'jinkal.urvam@gmail.com',
            'password' => bcrypt('123456'),
            'token' => str_random(16),
            'role_id' => '1',
            'is_timetracking_enabled' => 1
        ]);
        DB::table('users')->insert([
            'name' => 'Alpesh Parmar',
            'email' => 'alpesh.urvam@gmail.com',
            'password' => bcrypt('123456'),
            'token' => str_random(16),
            'role_id' => '1',
            'is_timetracking_enabled' => 1
        ]);
        DB::table('users')->insert([
            'name' => 'Kishan Ambaliya',
            'email' => 'kishan.urvam@gmail.com',
            'password' => bcrypt('123456'),
            'token' => str_random(16),
            'role_id' => '1',
            'is_timetracking_enabled' => 1
        ]);
        DB::table('users')->insert([
            'name' => 'Madhavi',
            'email' => 'madhavi.urvam@gmail.com',
            'password' => bcrypt('123456'),
            'token' => str_random(16),
            'role_id' => '1',
            'is_timetracking_enabled' => 1
        ]);
        DB::table('users')->insert([
            'name' => 'Manisha Patel',
            'email' => 'manisha.urvam@gmail.com',
            'password' => bcrypt('123456'),
            'token' => str_random(16),
            'role_id' => '1',
            'is_timetracking_enabled' => 1
        ]);
        DB::table('users')->insert([
            'name' => 'Neha Dwivedi',
            'email' => 'neha.urvam@gmail.com',
            'password' => bcrypt('123456'),
            'token' => str_random(16),
            'role_id' => '1',
            'is_timetracking_enabled' => 1
        ]);
        DB::table('users')->insert([
            'name' => 'Riya Modi',
            'email' => 'riya.urvam@gmail.com',
            'password' => bcrypt('123456'),
            'token' => str_random(16),
            'role_id' => '1',
            'is_timetracking_enabled' => 1
        ]);
        DB::table('users')->insert([
            'name' => 'Shraddha Chavda',
            'email' => 'shraddha.urvam@gmail.com',
            'password' => bcrypt('123456'),
            'token' => str_random(16),
            'role_id' => '1',
            'is_timetracking_enabled' => 1
        ]);
        DB::table('users')->insert([
            'name' => 'Prashant Dadhania',
            'email' => 'prashant.urvam@gmail.com',
            'password' => bcrypt('123456'),
            'token' => str_random(16),
            'role_id' => '1',
            'is_timetracking_enabled' => 1
        ]);
        DB::table('users')->insert([
            'name' => 'rohan modi',
            'email' => 'rohan.urvam@gmail.com',
            'password' => bcrypt('123456'),
            'token' => str_random(16),
            'role_id' => '0',
            'is_timetracking_enabled' => 0
        ]);


    }
}
