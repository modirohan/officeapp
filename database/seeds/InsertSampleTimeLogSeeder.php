<?php

use Illuminate\Database\Seeder;

class InsertSampleTimeLogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('timelogs')->truncate();
        \Illuminate\Support\Facades\DB::table('timelogs')->insert([
            [
                'user_id' => 3,
                'entry_date' => "2019-03-27",
                'entry_time' => "09:15:00",
                'entry_mode' => "2",
                'entry_type' => "1"
            ],
//            [
//                'user_id' => 3,
//                'entry_date' => "2019-03-29",
//                'entry_time' => "09:30:00",
//                'entry_mode' => "1",
//                'entry_type' => "1"
//            ],
            [
                'user_id' => 3,
                'entry_date' => "2019-03-29",
                'entry_time' => "10:30:00",
                'entry_mode' => "2",
                'entry_type' => "3"
            ],
//            [
//                'user_id' => 3,
//                'entry_date' => "2019-03-29",
//                'entry_time' => "10:55:00",
//                'entry_mode' => "1",
//                'entry_type' => "3"
//            ],
            [
                'user_id' => 3,
                'entry_date' => "2019-03-29",
                'entry_time' => "13:35:00",
                'entry_mode' => "2",
                'entry_type' => "2"
            ],
            ]);
    }
}
