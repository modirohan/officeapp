$(document).ready(function () {
    $('#user_form').bootstrapValidator({
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            name: {
                validators: {
                    notEmpty: {
                        message: 'Please enter the name'
                    }
                }
            },
            email: {
                validators: {
                    notEmpty: {
                        message: 'Please enter the email'
                    }
                }
            }
        }
    }).on('error.field.bv', function (e, data) {
        $("div.form-group").each(function () {
            if ($(this).hasClass("has-error")) {
                $(this).find("small.help-block").css("color", "red").show();
            }
        });
    });
});
/* PASSWORD SHOW HIDE ON HOVER OF EYE ICON */
$(".showPassword").hover(
    function functionName() {
        //Change the attribute to text
        $(this).parent().find(".display-password").attr("type", "text");
        $(this)
            .removeClass("fa-eye-slash")
            .addClass("fa-eye");
    },
    function() {
        //Change the attribute back to password
        $(this).parent().find(".display-password").attr("type", "password");
        $(this)
            .removeClass("fa-eye")
            .addClass("fa-eye-slash");
    }
);